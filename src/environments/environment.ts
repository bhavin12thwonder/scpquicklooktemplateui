// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // baseApiUrl: '//amhraovscpd2:8090/api/',//http://localhost:47117/api/,
  // scpUrl: '//safetycenterportaldev/',
  // scpAdminUrl: '//safetycenterportaldev/Admin/index',
  // asamApiUrl: '//amhraovscpd2:8092/api/'
  baseApiUrl: '//localhost:52689/api/',//http://localhost:47117/api/,
  scpUrl: '//safetycenterportaldev/',
  scpAdminUrl: '//safetycenterportaldev/Admin/index',
  asamApiUrl: '//localhost:60043/api/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
