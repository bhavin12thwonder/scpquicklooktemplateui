import { Component, Input, OnInit } from '@angular/core';
import { InputTextModule } from 'primeng/inputtext';
import { AddTemplateModule } from '../add-template/add-template.module';
import { ViewChild } from '@angular/core';
import { Button } from 'protractor';
import { MessageService, ConfirmationService } from 'primeng/api';
//import { NotificationService } from "../../notification-system/notification.service";
//import { ConfirmationDialogService } from '../../confirmation/confirmation.service';
import { IDynamicTableRows, IDynamicTableRowColumns, DynamicTableRows, DynamicTableRowColumns, ITemplateModel } from '../../models/template.model';
import { TemplateService } from '../../../services/template.service';
import { TemplateTabService } from '../../../services/templateTab.service';
import { TemplateTabConfigurationService } from '../../../services/templateTabConfiguration.service';
import { TestTypeService } from '../../../services/testType.service';
import { TestModeService } from '../../../services/testMode.service';
import { ITemplateTabModel } from 'src/app/models/templateTab.model';
import { ITemplateTabConfigurationModel, ITemplateTabConfigurationRowModel, ITemplateTabConfigurationColumnsModel } from 'src/app/models/templateTabConfiguration.model';
import { BreadcrumbsService } from '../../breadcrumb/breadcrumbs.service';
import { environment } from '../../../environments/environment';
import { AddTabTableConfigurationModalComponent } from '../../modal-popup/tabTableConfiguration/add/add-tabTableConfiguration-modal.component'
import { ITabConfigurationTableRows, ITabConfigurationTableRowsColumns, TabConfigurationTableRows, TabConfigurationTableRowsColumns } from '../../models/tabConfigurationTable.model';
import { ASAMService } from 'src/services/asam.service';
import { IUniqueIdsViewModel } from 'src/app/models/chartChannel.model';
import { Dropdown } from 'primeng';

@Component({
  selector: 'app-add-template',
  templateUrl: './add-template.component.html',
  styleUrls: ['./add-template.component.css'],
  styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }
    `]
})
export class AddTemplateComponent implements OnInit {
  displayFormatSelection: boolean = false;
  templateIndexId = 0;
  templateTabs: any;
  dynamicTable: any;
  numberOfRows: string = "";
  noOfColumn: number;
  rowData: IDynamicTableRows;
  columnsData: IDynamicTableRowColumns;
  columnsDataList: Array<IDynamicTableRowColumns> = [];
  editRowData: ITemplateTabConfigurationRowModel = <ITemplateTabConfigurationRowModel>{};
  editColumnsData: ITemplateTabConfigurationColumnsModel = <ITemplateTabConfigurationColumnsModel>{};
  editColumnsDataList: Array<IDynamicTableRowColumns> = [];
  existingRows: [];
  lastRowData: any;
  testTypeDropdownData: any;
  testModeDropdownData: any;
  testDropdownData: any;
  templateId: number;
  testId: number;
  testName: number;
  submatrixName: string;
  templateTabIndexId: number;
  templateTabId: number;
  templateTabConfigIndexId: number;
  templateViewModel: ITemplateModel = <ITemplateModel>{};
  templateTabViewModel: ITemplateTabModel = <ITemplateTabModel>{};
  templateTabConfigurationViewModel: ITemplateTabConfigurationModel = <ITemplateTabConfigurationModel>{};
  templateTabConfigurationArray: Array<ITemplateTabConfigurationModel> = [];
  templateTabConfigurationRowModel: Array<ITemplateTabConfigurationRowModel> = [];
  disableTabPanel: boolean = true;
  showTabView: boolean = false;
  tabConfigurationTableDialog: boolean = false;
  templateTabConfigId: number;
  templatetabTableConfigId: number;
  templateTabTableConfigName: string;
  templateTabChartTypeId: number;
  templateTabChartConfigId: number;
  templateTabChartConfigName: string;
  templateTabChartConfigLastUpdatedBy: string;
  isVisibleModal: boolean = false;
  displayFormatTypeId: string;
  displayFormatType: string;
  showAddTabConfigurationTableDialog: boolean = false;
  showEditTabConfigurationTableDialog: boolean = false;
  showPreviewTabConfigurationDialog: boolean = false;
  selectedCellArray: Array<any> = [];
  finalIndexArray: Array<any> = [];
  showTabChartConfigurationDialog: boolean = false;
  isEditChartConfiguration: boolean = true;
  uniqueIdsViewModel: Array<IUniqueIdsViewModel> = [];
  addTemplateDisable = false;

  constructor(
    private breadcrumbsService: BreadcrumbsService,
    private templateService: TemplateService,
    private templateTabService: TemplateTabService,
    private templateTabConfigurationService: TemplateTabConfigurationService,
    private testTypeService: TestTypeService,
    private testModeService: TestModeService,
    private asamService: ASAMService,
    private notifyService: MessageService,
    private confirmationService: ConfirmationService
  ) {
    this.breadcrumbsService.setItems([
      // { label: 'Home', routerLink: [environment.scpUrl] },
      // { label: 'Admin', routerLink: [environment.scpAdminUrl] },
      { label: 'Template', routerLink: [''] },
      { label: 'Add Template' }
    ]);
  }

  ngOnInit(): void {
    this.onLoadTestType();
  }

  success(message, summaryMessage = 'Success Message') {
    this.notifyService.add({ severity: 'success', summary: summaryMessage, detail: message });
  }

  error(message, summaryMessage = 'Error Message') {
    this.notifyService.add({ severity: 'error', summary: summaryMessage, detail: message });
  }

  reOrdertemplateTabs() {
    //Reorder Row Data
    if (this.templateTabs.length > 0) {

      for (let i = 0; i < this.templateTabs.length; i++) {
        let tabIndex = i;
        let columnsDataList = [];
        let rowsList = this.templateTabs[i];
        for (let j = 0; j < rowsList.length; j++) {
          let rowData = new DynamicTableRows();
          rowData = rowsList[j];
          rowData.RowId = j;
          rowData.TemplateTabIndex = tabIndex;

          //Load Columns Data
          for (let k = 0; k < rowData.Columns.length; k++) {
            let columnsData = new DynamicTableRowColumns();
            columnsData = rowData.Columns[k];
            columnsData.ColumnId = k;
            columnsData.DisplayFormatType = columnsData.DisplayFormatType.split('_')[0].toString() + '_' + tabIndex.toString() + this.rowData.RowId.toString() + k.toString();
            columnsData.ColumnName = 'Tab' + tabIndex.toString() + '-Row' + (this.rowData.RowId.toString()) + '-Col' + k.toString();
            columnsData.ColumnSpan = columnsData.ColumnSpan;
            columnsDataList.push(this.columnsData);
          }
          rowData.Columns = this.columnsDataList;
          this.templateTabs[i].rows[j] = rowData;
        }
      }
    }
  }

  onLoadTestType() {
    this.testTypeService.getAll().subscribe(response => {
      const resultList = [];
      for (const listItem of response) {
        const dropDownDataFormat = {
          label: listItem.name,
          value: listItem.id
        };
        resultList.push(dropDownDataFormat);
      }
      this.testTypeDropdownData = resultList.slice();
    });
  }
  onTestModeChangeForAdd(selectedTestMode, changeStatus) {
    this.uniqueIdsViewModel.push({ Id: parseInt(selectedTestMode[selectedTestMode.length - 1]) });
  }
  onLoadTestMode(testTypeId) {
    this.testModeDropdownData = [];
    this.templateViewModel.testModeList = [];
    this.testModeService.getByTestType(testTypeId).subscribe(response => {
      const resultList = [];
      for (const listItem of response) {
        const dropDownDataFormat = {
          label: listItem.name,
          value: listItem.id
        };
        resultList.push(dropDownDataFormat);
      }
      this.testModeDropdownData = resultList.slice();
    });
  }

  onLoadTestDropdown() {
    this.templateService.getById(this.templateId).subscribe(res => {
      this.asamService.getASAMTestData(res.testType, res.testMode).subscribe(response => {
        this.testDropdownData = [];
        this.testDropdownData = response.map(item => {
          return { label: item.TestName + ' ~ ' + item.SubMatrix, value: item.TestId };
        });
        if (this.testDropdownData.length > 0) {
          this.testName = this.testDropdownData[0].label;
          this.testId = this.testDropdownData[0].label.split('_')[1];
        }
      });
    });
  }

  onTestChange(selectedTest, dd: Dropdown) {
    this.testId = selectedTest;
    this.testName = selectedTest;
    this.submatrixName = dd.selectedOption.label.split('~')[1].trim();
  }

  loadTemplatTabConfiguration(templateTabId, tabIndex) {
    this.templateTabConfigurationService.getByTemplateTabId(templateTabId).subscribe(response => {
      if (response.length > 0) {
        //Load Tabconfiguartion Data
        this.templateTabConfigurationRowModel = response;
        this.templateTabs[tabIndex].rows = [];

        //Load Tabconfiguartion Row Data
        if (this.templateTabConfigurationRowModel.length > 0) {

          for (let j = 0; j < this.templateTabConfigurationRowModel.length; j++) {
            this.editRowData = <ITemplateTabConfigurationRowModel>{};
            this.editRowData = this.templateTabConfigurationRowModel[j];
            this.columnsDataList = [];
            this.rowData = new DynamicTableRows();
            this.rowData.RowId = j;
            this.rowData.TemplateTabId = this.editRowData.templateTabId;

            //Load Tabconfiguartion COlumns Data
            for (let k = 0; k < this.templateTabConfigurationRowModel[j].columns.length; k++) {
              this.columnsData = new DynamicTableRowColumns();
              this.editColumnsData = <ITemplateTabConfigurationColumnsModel>{};
              this.editColumnsData = this.templateTabConfigurationRowModel[j].columns[k];
              let displayFormatType = ((this.editColumnsData.displayFormatTypeId == 1) ? 'Table' : (this.editColumnsData.displayFormatTypeId == 2) ? 'Chart' : 'NoData');
              this.columnsData.Id = this.editColumnsData.id;
              this.columnsData.ColumnId = this.editColumnsData.columnId - 1;
              this.columnsData.DisplayFormatType = displayFormatType + '_' + tabIndex.toString() + this.rowData.RowId.toString() + (this.editColumnsData.columnId - 1).toString();
              this.columnsData.ColumnName = 'Tab' + tabIndex.toString() + '-Row' + (this.rowData.RowId.toString()) + '-Col' + (this.editColumnsData.columnId - 1).toString();
              this.columnsData.ColumnSpan = this.editColumnsData.columnSpan;
              this.columnsData.TemplateTabTableConfigurationId = this.editColumnsData.templateTabTableConfigurationId;
              this.columnsData.TemplateTabTableConfigurationName = this.editColumnsData.templateTabTableConfigurationName;
              this.columnsData.TemplateTabTableConfigurationLastUpdatedBy = this.editColumnsData.templateTabTableConfigurationLastUpdatedBy;
              this.columnsData.IsTabTableConfigExists = this.editColumnsData.isTabTableConfigExists;
              this.columnsData.TemplateTabChartConfigurationId = this.editColumnsData.templateTabChartConfigurationId;
              this.columnsData.TemplateTabChartConfigurationName = this.editColumnsData.templateTabChartConfigurationName;
              this.columnsData.TemplateTabChartConfigurationLastUpdatedBy = this.editColumnsData.templateTabChartConfigurationLastUpdatedBy;
              this.columnsData.IsTabChartConfigExists = this.editColumnsData.isTabChartConfigExists;

              this.columnsDataList.push(this.columnsData);
            }
            this.rowData.Columns = this.columnsDataList;
            this.templateTabs[tabIndex].rows.push(this.rowData);
            this.templateTabs[tabIndex].showFormatSelection = false;
          }
          this.templateTabs[tabIndex].showAddNewRow = true;
          this.templateTabs[tabIndex].showSave = false;
        }
        else {
          this.templateTabs[tabIndex].showAddNewRow = false;
          this.templateTabs[tabIndex].showSave = false;
        }
      }
      else {
        this.templateTabs[tabIndex].showAddNewRow = false;
        this.templateTabs[tabIndex].showSave = false;
      }
    });
  }

  loadTemplatTabEditConfiguration(eventData) {
    let responseData = eventData.split('_');
    let templateTabId = responseData[0];
    let tabIndex = responseData[1];

    this.templateTabConfigurationService.getByTemplateTabId(templateTabId).subscribe(response => {
      if (response.length > 0) {
        //Load Tabconfiguartion Data
        this.templateTabConfigurationRowModel = response;
        this.templateTabs[tabIndex].rows = [];

        //Load Tabconfiguartion Row Data
        if (this.templateTabConfigurationRowModel.length > 0) {

          for (let j = 0; j < this.templateTabConfigurationRowModel.length; j++) {
            this.editRowData = <ITemplateTabConfigurationRowModel>{};
            this.editRowData = this.templateTabConfigurationRowModel[j];
            this.columnsDataList = [];
            this.rowData = new DynamicTableRows();
            this.rowData.RowId = j;
            this.rowData.TemplateTabId = this.editRowData.templateTabId;

            //Load Tabconfiguartion COlumns Data
            for (let k = 0; k < this.templateTabConfigurationRowModel[j].columns.length; k++) {
              this.columnsData = new DynamicTableRowColumns();
              this.editColumnsData = <ITemplateTabConfigurationColumnsModel>{};
              this.editColumnsData = this.templateTabConfigurationRowModel[j].columns[k];
              let displayFormatType = ((this.editColumnsData.displayFormatTypeId == 1) ? 'Table' : (this.editColumnsData.displayFormatTypeId == 2) ? 'Chart' : 'NoData');
              this.columnsData.Id = this.editColumnsData.id;
              this.columnsData.ColumnId = this.editColumnsData.columnId - 1;
              this.columnsData.DisplayFormatType = displayFormatType + '_' + tabIndex.toString() + this.rowData.RowId.toString() + (this.editColumnsData.columnId - 1).toString();
              this.columnsData.ColumnName = 'Tab' + tabIndex.toString() + '-Row' + (this.rowData.RowId.toString()) + '-Col' + (this.editColumnsData.columnId - 1).toString();
              this.columnsData.ColumnSpan = this.editColumnsData.columnSpan;
              this.columnsData.TemplateTabTableConfigurationId = this.editColumnsData.templateTabTableConfigurationId;
              this.columnsData.TemplateTabTableConfigurationName = this.editColumnsData.templateTabTableConfigurationName;
              this.columnsData.TemplateTabTableConfigurationLastUpdatedBy = this.editColumnsData.templateTabTableConfigurationLastUpdatedBy;
              this.columnsData.IsTabTableConfigExists = this.editColumnsData.isTabTableConfigExists;
              this.columnsData.TemplateTabChartConfigurationId = this.editColumnsData.templateTabChartConfigurationId;
              this.columnsData.TemplateTabChartConfigurationName = this.editColumnsData.templateTabChartConfigurationName;
              this.columnsData.TemplateTabChartConfigurationLastUpdatedBy = this.editColumnsData.templateTabChartConfigurationLastUpdatedBy;
              this.columnsData.IsTabChartConfigExists = this.editColumnsData.isTabChartConfigExists;

              this.columnsDataList.push(this.columnsData);
            }
            this.rowData.Columns = this.columnsDataList;
            this.templateTabs[tabIndex].rows.push(this.rowData);
            this.templateTabs[tabIndex].showFormatSelection = false;
          }
          this.templateTabs[tabIndex].showAddNewRow = true;
          this.templateTabs[tabIndex].showSave = false;
        }
        else {
          this.templateTabs[tabIndex].showAddNewRow = false;
          this.templateTabs[tabIndex].showSave = false;
        }
      }
      else {
        this.templateTabs[tabIndex].showAddNewRow = false;
        this.templateTabs[tabIndex].showSave = false;
      }
    });
  }

  onAddNewTab() {
    if (this.templateId != undefined) {
      const arrLength = this.templateTabs.length;
      this.templateTabViewModel.TemplateId = this.templateId;
      this.templateTabViewModel.Name = 'Tab' + (arrLength + 1);
      this.templateTabViewModel.DisplayOrder = this.templateId;
      this.templateTabViewModel.TabIndex = arrLength;
      this.templateTabService.postData(this.templateTabViewModel).subscribe(response => {
        if (response.isSuccess) {
          this.templateTabId = response.id;
          this.templateTabs.push({ id: parseInt(response.id), name: 'Tab' + (arrLength + 1), tabIndex: arrLength, isdeleted: false, dynamicTableData: 'Tab' + (arrLength + 1), rows: [], showSave: false, showFormatSelection: true, showAddNewRow: false });
          this.success('Tab Successfully Added');
        }
        else {
          this.error("Failed To Save");
        }
      });
    }
  }

  onDeleteTab(event) {
    this.confirmationService.confirm({
      header: "Delete Confirmation",
      message: "Are you sure?",
      key: 'deleteTabConfirmation',
      accept: () => {
        if (this.templateTabs.length > 1) {
          this.templateTabService.deleteData(this.templateId, event.index).subscribe(response => {
            if (response.isSuccess) {
              this.templateTabs.splice(event.index, 1);
              for (let i = 0; i < this.templateTabs.length; i++) {
                this.templateTabs[i].tabIndex = i;
              }
              this.reOrdertemplateTabs();
              this.success('Tab Successfully Deleted');
            }
            else {
              this.error("Failed To Delete Tab");
            }
          });
        } else {
          this.error("At least one tab should exists");
        }
      }
    });
  }

  changeName(setName, index) {
    this.templateTabs[index].name
  }

  tabChange(event) {
    this.templateTabIndexId = event.index;
  }

  newRowFormat(tabIndex: number, lastRowId: number, columns: number, templateTabId: number) {
    this.rowData = new DynamicTableRows();
    this.rowData.RowId = lastRowId;
    this.rowData.TemplateTabId = templateTabId;
    this.rowData.TemplateTabIndex = tabIndex;
    this.columnsDataList = [];
    for (let j = 0; j < columns; j++) {
      let columnName = 'Tab' + tabIndex.toString() + '-Row' + (this.rowData.RowId.toString()) + '-Col' + j.toString();
      this.columnsData = new DynamicTableRowColumns();
      this.columnsData.ColumnId = j;
      this.columnsData.DisplayFormatType = '_' + tabIndex.toString() + this.rowData.RowId.toString() + j.toString();
      this.columnsData.ColumnName = columnName;
      this.columnsData.ColumnSpan = 1;
      this.columnsData.IsDeleted = false;
      this.columnsDataList.push(this.columnsData);
    }
    this.rowData.Columns = this.columnsDataList;
    this.templateTabs[tabIndex].rows.push(this.rowData);

    this.onUpdateTabConfiguration(tabIndex);
  }

  onNumberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  createDynamicTable(row: number, column: number) {
    if (row < 11) {
      this.noOfColumn = column;
      this.templateTabs[this.templateTabIndexId].rows = [];
      for (let i = 0; i < row; i++) {
        this.rowData = new DynamicTableRows();
        this.rowData.RowId = i;
        this.rowData.TemplateTabId = this.templateTabs[this.templateTabIndexId].id;
        this.rowData.TemplateTabIndex = this.templateTabIndexId;
        this.columnsDataList = [];
        for (let j = 0; j < column; j++) {
          let columnName = 'Tab' + this.templateTabIndexId.toString() + '-Row' + (this.rowData.RowId.toString()) + '-Col' + j.toString();
          this.columnsData = new DynamicTableRowColumns();
          this.columnsData.Id = 0;
          this.columnsData.ColumnId = j;
          this.columnsData.DisplayFormatType = '_' + this.templateTabIndexId.toString() + this.rowData.RowId.toString() + j.toString();
          this.columnsData.ColumnName = columnName;
          this.columnsData.ColumnSpan = 1;
          this.columnsData.IsDeleted = false;
          this.columnsDataList.push(this.columnsData);
        }
        this.rowData.Columns = this.columnsDataList;
        this.templateTabs[this.templateTabIndexId].rows.push(this.rowData);
        this.templateTabs[this.templateTabIndexId].showFormatSelection = false;
        this.templateTabs[this.templateTabIndexId].showAddNewRow = true;
        this.templateTabs[this.templateTabIndexId].showSave = false;

      }
      this.displayFormatSelection = false;
      this.numberOfRows = "";

      //Save the configuration
      this.onSaveTabConfiguration(this.templateTabIndexId);
    }
    else {
      this.error("Row limit is 10");

    }
  }

  onPreviewTabConfiguration() {
    this.showPreviewTabConfigurationDialog = true;
  }

  showDialog(tabIndex) {
    this.displayFormatSelection = true;
    this.templateTabIndexId = tabIndex;
  }

  showResponseDialog() {
    this.displayFormatSelection = true;
  }

  addNewRow(tabIndex) {
    this.newRowFormat(tabIndex, this.templateTabs[tabIndex].rows.length, this.noOfColumn, this.templateTabs[tabIndex].id);
  }

  onSaveTabConfiguration(tabIndexId) {

    this.templateTabConfigurationArray = [];
    let existingTabsData = this.templateTabs;
    let existingRows = existingTabsData[tabIndexId].rows;

    for (let i = 0; i < existingRows.length; i++) {
      this.rowData = existingRows[i];

      for (let j = 0; j < this.rowData.Columns.length; j++) {
        this.templateTabConfigurationViewModel = <ITemplateTabConfigurationModel>{};
        this.columnsData = this.rowData.Columns[j];
        let displayFormatTypeId = (this.columnsData.DisplayFormatType.split('_')[0].toString() == "Table") ? 1 : (this.columnsData.DisplayFormatType.split('_')[0].toString() == "Chart") ? 2 : 3;
        this.columnsData.ColumnId = parseInt(j.toString()) + 1;
        this.templateTabConfigurationViewModel.TemplateTabId = this.rowData.TemplateTabId;
        this.templateTabConfigurationViewModel.Id = this.columnsData.Id;
        this.templateTabConfigurationViewModel.RowId = parseInt(this.rowData.RowId.toString()) + 1;
        this.templateTabConfigurationViewModel.ColumnId = this.columnsData.ColumnId;
        this.templateTabConfigurationViewModel.ColumnSpan = this.columnsData.ColumnSpan;
        this.templateTabConfigurationViewModel.DisplayFormatTypeId = displayFormatTypeId;
        this.templateTabConfigurationViewModel.IsDeleted = this.columnsData.IsDeleted;
        this.templateTabConfigurationArray.push(this.templateTabConfigurationViewModel);
      }
    }

    this.templateTabConfigurationService.postData(this.templateTabConfigurationArray).subscribe(response => {
      if (response.isSuccess) {
        //this.templateTabId = response.id;
        this.success('Tab Configuration Successfully Added');
        this.templateTabs[tabIndexId].showSave = false;
        this.templateTabs[tabIndexId].showFormatSelection = false;
        this.templateTabs[tabIndexId].showAddNewRow = true;
        this.loadTemplatTabConfiguration(this.templateTabId, tabIndexId);
      }
      else {
        this.error("Failed To Update");
      }
    });
  }

  onUpdateTabConfiguration(tabIndexId) {
    this.templateTabConfigurationArray = [];
    this.templateTabIndexId = tabIndexId;
    let existingTabsData = this.templateTabs;
    let existingRows = existingTabsData[this.templateTabIndexId].rows;

    for (let i = 0; i < existingRows.length; i++) {
      this.rowData = existingRows[i];
      for (let j = 0; j < this.rowData.Columns.length; j++) {
        this.templateTabConfigurationViewModel = <ITemplateTabConfigurationModel>{};
        this.columnsData = this.rowData.Columns[j];
        let displayFormatTypeId = (this.columnsData.DisplayFormatType.split('_')[0].toString() == "Table") ? 1 : (this.columnsData.DisplayFormatType.split('_')[0].toString() == "Chart") ? 2 : 3;
        this.columnsData.ColumnId = parseInt(j.toString()) + 1;
        this.templateTabConfigurationViewModel.TemplateTabId = this.rowData.TemplateTabId;
        this.templateTabConfigurationViewModel.Id = this.columnsData.Id;
        this.templateTabConfigurationViewModel.RowId = parseInt(this.rowData.RowId.toString()) + 1;
        this.templateTabConfigurationViewModel.ColumnId = this.columnsData.ColumnId;
        this.templateTabConfigurationViewModel.ColumnSpan = this.columnsData.ColumnSpan;
        this.templateTabConfigurationViewModel.DisplayFormatTypeId = displayFormatTypeId;
        this.templateTabConfigurationViewModel.IsDeleted = this.columnsData.IsDeleted;
        this.templateTabConfigurationArray.push(this.templateTabConfigurationViewModel);
      }
    }

    this.templateTabConfigurationService.putData(this.templateTabConfigurationArray).subscribe(response => {
      if (response.isSuccess) {
        this.loadTemplatTabConfiguration(this.templateTabId, tabIndexId);
        this.success('Tab Configuration Successfully Updated');
      }
      else {
        this.error("Failed To Update");
      }
    });
  }

  onSaveTemplate() {
    if (this.addTemplateDisable == false) {
      this.templateViewModel.testModeIds = this.uniqueIdsViewModel;
      if (this.templateId == undefined || this.templateId == null)
        this.templateViewModel.id = 0;
      else
        this.templateViewModel.id = this.templateId;
      if (this.templateViewModel.name != undefined || this.templateViewModel.name != null) {
        if (this.templateViewModel.testTypeId != undefined) {
          if ((typeof this.templateViewModel.testModeList !== 'undefined' && this.templateViewModel.testModeList.length > 0)
            || Object.keys(this.templateViewModel.testModeIds).length !== 0) {
            //if (this.templateViewModel.testModeIds != null || this.templateViewModel.testModeList != null) {
            //this.templateService.checkTemplateName(this.templateViewModel.name, this.templateViewModel.testTypeId, this.templateViewModel.testModeIds).subscribe(result => {
            this.templateService.checkTemplateName(this.templateViewModel).subscribe(result => {
              if (!result) {
                this.templateService.postData(this.templateViewModel).subscribe(response => {
                  if (response.isSuccess) {
                    this.templateId = response.id;
                    this.templateTabViewModel.TemplateId = response.id;
                    this.templateTabViewModel.Name = this.templateViewModel.name;
                    this.templateTabViewModel.TabIndex = 0;
                    this.templateTabService.postData(this.templateTabViewModel).subscribe(response => {
                      if (response.isSuccess) {
                        this.addTemplateDisable = true;
                        const templateTabId = response.id;
                        this.templateTabId = templateTabId;
                        this.templateTabs = [{ id: templateTabId, name: this.templateViewModel.name, tabIndex: 0, isdeleted: false, dynamicTableData: 'table0', rows: [], showSave: false, showFormatSelection: true, showAddNewRow: false }];
                        this.onLoadTestDropdown();
                      }
                      else {
                        this.templateTabId = 0;
                      }
                    });
                    this.disableTabPanel = false;
                    this.showTabView = true;
                    this.success('Template Successfully Added');
                  }
                  else {
                    this.error("Failed To Save");
                  }
                });

              }
              else {
                this.error("Template Name Already Exists");
              }
            });
          }
          else {
            this.error("Please select test mode", "Validation Message");
          }
        }
        else {
          this.error("Please select test type", "Validation Message");
        }
      }
      else {
        this.error("Please enter template name", "Validation Message");
      }
    }
    else {
      return;
    }
  }

  onDeleteCell(tabIndexId, rowId, columnName) {
    this.confirmationService.confirm({
      header: "Delete Confirmation",
      message: "Are you sure?",
      key: 'deleteCellConfirmation',
      accept: () => {
        let tabData = this.templateTabs[tabIndexId];
        let rowData = tabData.rows[rowId];
        let columnsData = rowData.Columns;
        let existingColumnData = columnsData.filter(item => { return item.ColumnName != columnName; });
        let DeletedColumnData = columnsData.filter(item => { return item.ColumnName == columnName; });
        DeletedColumnData[0].IsDeleted = true;
        if (existingColumnData.length > 0) {
          existingColumnData[0].ColumnSpan = 2;
          existingColumnData[1] = DeletedColumnData[0];
        }
        else {
          existingColumnData[0] = DeletedColumnData[0];
        }
        columnsData = existingColumnData;
        this.templateTabs[tabIndexId].rows[rowId].Columns = columnsData;

        this.onUpdateTabConfiguration(tabIndexId);
      }
    });

  }

  onUpdateTemplateTab(tabIndex, tabName) {

    this.templateTabViewModel.TemplateId = this.templateId;
    this.templateTabViewModel.Id = this.templateTabs[tabIndex].id;
    this.templateTabViewModel.TabIndex = this.templateTabs[tabIndex].tabIndex;
    this.templateTabViewModel.Name = tabName;
    this.templateTabService.putData(this.templateTabViewModel).subscribe(response => {
      if (response.isSuccess) {
        this.success('Tab Successfully Updated');
      }
      else {
        this.error("Tab Failed To Update");
      }
    });
  }

  openTabTableConfiguration(columnCellData, rowData, tabIndex) {
    this.showAddTabConfigurationTableDialog = false;
    this.templateTabConfigurationService.getTabConfigTypeByTabId(columnCellData.Id, 2).subscribe(response => {
      if (response != null) {
        if (response.isSuccess && response.isConfigExists) {
          this.confirmationService.confirm({
            header: "Tab Configuration Update Confirmation",
            message: "You have selected table configuration and " + response.statusMessage + " will be disabled, are you sure?",
            key: 'updateTabConfigConfirmation',
            accept: () => {
              this.templateTabConfigurationService.putTabConfigTypeByTabId(columnCellData.Id, 2).subscribe(response => {
                if (response.isSuccess) {
                  this.success('Table Configuration Successfully Saved');
                  if (columnCellData.IsTabTableConfigExists) {
                    this.showAddTabConfigurationTableDialog = false;
                  }
                  else {
                    this.showAddTabConfigurationTableDialog = true;
                  }
                }
              });
            },
            reject: () => {
              this.showAddTabConfigurationTableDialog = false;
              let formatType = columnCellData.DisplayFormatType.split('_')[0];
              if (formatType === "Table") {
                columnCellData.DisplayFormatType = "Chart_" + columnCellData.DisplayFormatType.split('_')[1];
              }
              else if (formatType === "Chart") {
                columnCellData.DisplayFormatType = "Table_" + columnCellData.DisplayFormatType.split('_')[1];
              }
            }
          });
        }
      }
      else {
        if (columnCellData.IsTabTableConfigExists) {
          this.showAddTabConfigurationTableDialog = false;
        }
        else {
          this.showAddTabConfigurationTableDialog = true;
        }
      }
    });

    this.templatetabTableConfigId = columnCellData.TemplateTabTableConfigurationId;
    this.templateTabTableConfigName = columnCellData.TemplateTabTableConfigurationName;
    this.templateTabId = rowData.TemplateTabId;
    this.templateTabConfigIndexId = tabIndex;
    this.templateTabConfigId = columnCellData.Id;
    this.displayFormatType = columnCellData.DisplayFormatType;
    this.selectedCellArray = [];
    this.finalIndexArray = [];
  }

  openTabChartConfiguration(columnCellData, rowData, tabIndex) {
    this.showTabChartConfigurationDialog = false;
    this.templateTabConfigurationService.getTabConfigTypeByTabId(columnCellData.Id, 1).subscribe(response => {
      if (response != null) {
        if (response.isSuccess && response.isConfigExists) {
          this.confirmationService.confirm({
            header: "Tab Configuration Update Confirmation",
            message: "You have selected chart configuration and " + response.statusMessage + " will be disabled, are you sure?",
            key: 'updateTabConfigConfirmation',
            accept: () => {
              this.templateTabConfigurationService.putTabConfigTypeByTabId(columnCellData.Id, 1).subscribe(response => {
                if (response.isSuccess) {
                  this.success('Chart Configuration Successfully Saved');
                  if (columnCellData.IsTabChartConfigExists) {
                    this.showTabChartConfigurationDialog = false;
                  }
                  else {
                    this.showTabChartConfigurationDialog = true;
                  }
                }
              });
            },
            reject: () => {
              this.showTabChartConfigurationDialog = false;
              let formatType = columnCellData.DisplayFormatType.split('_')[0];
              if (formatType === "Table") {
                columnCellData.DisplayFormatType = "Chart_" + columnCellData.DisplayFormatType.split('_')[1];
              }
              else if (formatType === "Chart") {
                columnCellData.DisplayFormatType = "Table_" + columnCellData.DisplayFormatType.split('_')[1];
              }
            }
          });
        }
      }
      else {
        if (columnCellData.IsTabChartConfigExists) {
          this.showTabChartConfigurationDialog = false;
        }
        else {
          this.showTabChartConfigurationDialog = true;
        }
      }
    });

    this.templateTabChartConfigId = columnCellData.TemplateTabChartConfigurationId;
    this.templateTabChartConfigName = columnCellData.TemplateTabChartConfigurationName;
    this.templateTabChartTypeId = columnCellData.ChartTypeId;
    this.templateTabChartConfigLastUpdatedBy = columnCellData.TemplateTabChartConfigurationLastUpdatedBy;
    this.templateTabId = rowData.TemplateTabId;
    this.templateTabConfigIndexId = tabIndex;
    this.templateTabConfigId = columnCellData.Id;
    this.displayFormatType = columnCellData.DisplayFormatType;
    if (columnCellData.IsTabChartConfigExists) {
      this.isEditChartConfiguration = true;
    }
    else {
      this.isEditChartConfiguration = false;
    }
  }

  onEditTableConfiguration(columnCellData, rowData, tabIndex) {
    this.templatetabTableConfigId = columnCellData.TemplateTabTableConfigurationId;
    this.templateTabTableConfigName = columnCellData.TemplateTabTableConfigurationName;
    this.templateTabId = rowData.TemplateTabId;
    this.templateTabConfigIndexId = tabIndex;
    this.templateTabConfigId = columnCellData.Id;
    this.displayFormatType = columnCellData.DisplayFormatType;
    this.showAddTabConfigurationTableDialog = false;
    this.showEditTabConfigurationTableDialog = true;
    this.selectedCellArray = [];
    this.finalIndexArray = [];
  }

  onEditChartConfiguration(columnCellData, rowData, tabIndex) {
    this.templateTabChartConfigId = columnCellData.TemplateTabChartConfigurationId;
    this.templateTabChartConfigName = columnCellData.TemplateTabChartConfigurationName;
    this.templateTabChartTypeId = columnCellData.ChartTypeId;
    this.templateTabChartConfigLastUpdatedBy = columnCellData.TemplateTabChartConfigurationLastUpdatedBy;
    this.templateTabId = rowData.TemplateTabId;
    this.templateTabConfigIndexId = tabIndex;
    this.templateTabConfigId = columnCellData.Id;
    this.displayFormatType = columnCellData.DisplayFormatType;
    if (columnCellData.IsTabChartConfigExists) {
      this.isEditChartConfiguration = true;
    }
    else {
      this.isEditChartConfiguration = false;
    }
    this.showTabChartConfigurationDialog = true;
  }
}