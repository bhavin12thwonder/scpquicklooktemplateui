import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { TemplateService } from '../../../services/template.service';
import { BreadcrumbsService } from '../../breadcrumb/breadcrumbs.service';
import { environment } from '../../../environments/environment';

interface Status {
  name: string,
  code: string
}
@Component({
  selector: 'template-grid',
  templateUrl: './template-grid.component.html',
  styleUrls: ['./template-grid.component.css'],
  styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }
    `]
})
export class TemplateGridComponent implements OnInit {
  status: SelectItem[];
  selectedStatus: Status;
  //templates: Template[];
  templateData: Array<any>;
  totalRecords: number;
  first = 0;
  rows = 5;
  displayData = [];

  constructor(private templateService: TemplateService,
    private breadcrumbsService: BreadcrumbsService,
    private router: Router,
    private notifyService: MessageService,
    private confirmationService: ConfirmationService) {
    this.status = [
      { label: 'Active', value: false },
      { label: 'InActive', value: true },
      { label: 'All', value: "all" },
    ];
    this.breadcrumbsService.setItems([
      // { label: 'Home', routerLink: [environment.scpUrl] },
      // { label: 'Admin', routerLink: [environment.scpAdminUrl] },
      { label: 'Templates' }
    ]);
  }

  ngOnInit(): void {
    this.getTemplate();
  }
  onNavigate() {
    this.router.navigate(['/addTemplate']);
  }
  success(message, summaryMessage = 'Success Message') {
    this.notifyService.add({ severity: 'success', summary: summaryMessage, detail: message });
  }

  error(message, summaryMessage = 'Error Message') {
    this.notifyService.add({ severity: 'error', summary: summaryMessage, detail: message });
  }

  changeGridByStatus(name) {
    if (name !== "all") {
      this.displayData = this.templateData.filter(m => m.isDeleted === name);
    }
    else {
      this.displayData = this.templateData;
    }
  }
  onEditTemplate(id) {
    this.router.navigate(['editTemplate', id]);
  }
  onDeleteTemplate(id) {
    this.confirmationService.confirm({
      header: "Delete Confirmation",
      message: "Are you sure?",
      accept: () => {
        this.templateService.deleteData(id).subscribe(res => {
          if (res.isSuccess) {
            this.success('Template Successfully deleted');
            this.getTemplate();
          }
          else {
            this.error("Failed To delete");
          }
        });
      }
    });
  }

  onEnableTemplate(id) {
    this.confirmationService.confirm({
      header: "Enable Confirmation",
      message: "Are you sure?",
      accept: () => {
        this.templateService.enableData(id).subscribe(res => {
          if (res.isSuccess) {
            this.success('Template Successfully Enabled');
            this.getTemplate();
          }
          else {
            this.error("Failed To Enable");
          }
        });
      }
    });
  }

  next() {
    this.first = this.first + this.rows;
  }
  prev() {
    this.first = this.first - this.rows;
  }
  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.first === (this.templateData.length - this.rows);
  }

  isFirstPage(): boolean {
    return this.first === 0;
  }
  getTemplate() {
    this.templateService.getTemplate().subscribe(res => {
      this.templateData = res;
      this.changeGridByStatus(false);
      //this.displayData=this.templateData;
      //console.log(res);
    })
    //this.totalRecords = this.templateData.length;
  }
}
