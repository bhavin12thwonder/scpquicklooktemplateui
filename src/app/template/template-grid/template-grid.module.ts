import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
//import { TemplateGridComponent } from '../template-grid/template-grid.component';
import { Routes, RouterModule } from '@angular/router';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';

const routes: Routes = [
  //{ path: '', component: TemplateGridComponent }
];

@NgModule({
  declarations: [],
  imports: [
    TableModule,
    CommonModule,
    ButtonModule,
    ToastModule,
    MessagesModule,
    MessageModule
  ]
})
export class TemplateGridModule { }
