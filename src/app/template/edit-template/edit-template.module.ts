import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route } from '@angular/compiler/src/core';
import { Routes, RouterModule } from '@angular/router';
import { EditTemplateComponent } from '../edit-template/edit-template.component';
import { TabViewModule } from 'primeng/tabview';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { RadioButtonModule } from 'primeng/radiobutton';
import { Component, ViewChild, ViewContainerRef, Input, EventEmitter, Output } from '@angular/core';
import { TabView, TabPanel } from 'primeng/primeng';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { ToastModule } from 'primeng/toast';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { AddTabTableConfigurationModalModule } from '../../modal-popup/tabTableConfiguration/add/add-tabTableConfiguration-modal.module';
import { EditTabTableConfigurationModalModule } from '../../modal-popup/tabTableConfiguration/edit/edit-tabTableConfiguration-modal.module';
import { TabChartConfigurationModalModule } from '../../modal-popup/tabChartConfiguration/tabChartConfiguration-modal.module';
import { PreviewTemplateModule } from '../../modal-popup/template/preview/preview-template-modal.module';

const routes: Routes = [
  { path: '', component: EditTemplateComponent }
];

@NgModule({
  declarations: [EditTemplateComponent],
  imports: [
    MultiSelectModule,
    DialogModule,
    DropdownModule,
    FormsModule,
    ButtonModule,
    TableModule,
    TabViewModule,
    CommonModule,
    ToastModule,
    MessagesModule,
    MessageModule,
    ConfirmDialogModule,
    RadioButtonModule,
    AddTabTableConfigurationModalModule,
    EditTabTableConfigurationModalModule,
    TabChartConfigurationModalModule,
    PreviewTemplateModule,
    RouterModule.forChild(routes),
  ],
})
export class EditTemplateModule { }
