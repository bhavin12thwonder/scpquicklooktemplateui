import { Component, OnInit } from '@angular/core';
import { MenuItem, SelectItem, SelectItemGroup } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = 'QuickLookApp';
  mini: boolean = true;

  constructor() {
  }

  ngOnInit(): void {
  }

  toggleSidebar() {
    if (this.mini) {
      document.getElementById("mySidebar").style.width = "12%";
      document.getElementById("main").style.marginLeft = "12%";
      this.mini = false;
    } else {
      document.getElementById("mySidebar").style.width = "3%";
      document.getElementById("main").style.marginLeft = "3%";
      this.mini = true;
    }
  }
}
