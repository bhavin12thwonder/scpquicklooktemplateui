import { Injectable } from '@angular/core';
import { Subject, Observable } from "rxjs";
import { Notification, NotificationType } from "./notification";

@Injectable()
export class NotificationService {

    private _subject = new Subject<Notification>();
    private _idx = 0;

    constructor() { }

    getObservable(): Observable<Notification> {
        return this._subject.asObservable();
    }

    info(message: string, title: string = 'Information Message', timeout = 2000) {
        this._subject.next(new Notification(this._idx++, NotificationType.info, title, message, timeout));
    }

    success(message: string, title: string = 'Success Message', timeout = 2000) {
        this._subject.next(new Notification(this._idx++, NotificationType.success, title, message, timeout));
    }

    warning(message: string, title: string = 'Waring Message', timeout = 2000) {
        this._subject.next(new Notification(this._idx++, NotificationType.warning, title, message, timeout));
    }

    error(message: string, title: string = 'Error Message', timeout = 1000) {
        debugger;
        this._subject.next(new Notification(this._idx++, NotificationType.error, title, message, timeout));
    }

}