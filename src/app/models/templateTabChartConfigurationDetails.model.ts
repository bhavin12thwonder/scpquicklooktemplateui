import { IChartAnnotationModel } from './chartAnnotation.model';
import { IChartChannelModel } from './chartChannel.model';

export interface ITemplateTabChartConfigurationDetailsModel {
    Id: number,
    TemplateTabChartConfigurationId: number,
    TemplateTabChartConfigurationName: string,
    XAxisChannelId: number,
    XAxisUnits: string,
    XAxisMinValue: number,
    XAxisMaxValue: number,
    XAxisInterval: number,
    XAxisLabel: string,
    YAxisUnits: string,
    YAxisMinValue: number,
    YAxisMaxValue: number,
    YAxisInterval: number,
    YAxisLabel: string,
    IsDeleted: boolean,
    IsParentDisabled: boolean,
    XAxisAutoFill: boolean,
    YAxisAutoFill: boolean
}

export interface ITemplateTabChartConfigurationDetailsUpsertModel {
    Id: number,
    TemplateTabChartConfigurationId: number,
    TemplateTabChartConfigurationName: string,
    XAxisChannelId: number,
    XAxisUnits: string,
    XAxisMinValue: number,
    XAxisMaxValue: number,
    XAxisInterval: number,
    XAxisLabel: string,
    YAxisUnits: string,
    YAxisMinValue: number,
    YAxisMaxValue: number,
    YAxisInterval: number,
    YAxisLabel: string,
    IsDeleted: boolean,
    IsParentDisabled: boolean,
    XAxisAutoFill: boolean,
    YAxisAutoFill: boolean
    ChartAnnotationList: Array<IChartAnnotationModel>
}

export interface ITemplateTabChartConfigurationDetailsDbModel {
    id: number,
    templateTabChartConfigurationId: number,
    templateTabChartConfigurationName: string,
    xAxisChannelId: number,
    xAxisUnits: string,
    xAxisMinValue: number,
    xAxisMaxValue: number,
    xAxisInterval: number,
    xAxisLabel: string,
    yAxisUnits: string,
    yAxisMinValue: number,
    yAxisMaxValue: number,
    yAxisInterval: number,
    yAxisLabel: string,
    isDeleted: boolean,
    isParentDisabled: boolean,
    xAxisAutoFill: boolean,
    yAxisAutoFill: boolean
}

export interface ITemplateTabChartConfigurationPreviewViewModel {
    id: number,
    chartTypeId: number,
    chartTypeValue: string,
    templateTabChartConfigurationId: number,
    templateTabChartConfigurationName: string,
    xAxis: Array<number>,
    yAxis: Array<number>,
    xAxisChannel: string
    xAxisUnits: string,
    xAxisMinValue: number,
    xAxisMaxValue: number,
    xAxisInterval: number,
    xAxisLabel: string,
    yAxisUnits: string,
    yAxisMinValue: number,
    yAxisMaxValue: number,
    yAxisInterval: number,
    yAxisLabel: string,
    chartChannelConfiguration: Array<ChartChannelConfigurationPreviewViewModel>,
    chartAnnotationConfiguration: Array<ChartAnnotationConfigurationPreviewViewModel>,
    isDeleted: boolean,
    chartOrientation: string,
    xAxisAutoFill: boolean,
    yAxisAutoFill: boolean
}

export interface ChartChannelConfigurationPreviewViewModel {
    chartChannelId: number,
    chartChannelName: string,
    chartLineColour: string,
    chartLineStyleId: number,
    chartLineThicknessId: number,
    lineStyleName: string,
    lineStyleValue: string,
    lineThickness: number,
    legendName: string,
    goodValue: number,
    acceptableValue: number,
    marginalValue: number,
    poorValue: number,
    fillArea: boolean,
    polarity: boolean,
    atdPosition: number,
    atdPositionValue: string,
    isLegendVisible: boolean,
    atdPositionAxisValue: number,
    chartChannelATDPositions: Array<ChartChannelATDPositionsDbViewModel>,
    barChartXAxisValue: number,
    channelXAxis: Array<number>,
    channelYAxis: Array<number>
}

export interface ChartAnnotationConfigurationPreviewViewModel {
    chartAnnotationId: number,
    annotationLineStartXCoOrdinate: string,
    annotationLineEndXCoOrdinate: string,
    annotationLineStartYCoOrdinate: string,
    annotationLineEndYCoOrdinate: string,
    annotationLineThicknessId: string,
    annotationLineColour: string,
    annotationLineStyleId: string,
    annotationText: string,
    annotationTextStartXCoOrdinate: string,
    annotationTextStartYCoOrdinate: string
}

export interface ChartChannelATDPositionsDbViewModel {
    atdPositionId: number,
    atdPositionValue: string,
    atdPositionAxisValue: number
}
export interface IAxisUnitValue {
    label: string,
    value: string
}