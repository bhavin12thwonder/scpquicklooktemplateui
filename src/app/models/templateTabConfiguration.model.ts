import { ITemplateTabTableConfigurationDetailsPreviewRowsDbModel } from '../models/templateTabTableConfigurationDetails.model'
import { ITemplateTabChartConfigurationPreviewViewModel } from './templateTabChartConfigurationDetails.model';
export interface ITemplateTabConfigurationModel {
    Id: number,
    TemplateTabId: number,
    TemplateTabIndex: number,
    RowId: number,
    ColumnId: number,
    ColumnSpan: number,
    DisplayFormatTypeId: number,
    IsDeleted: boolean
}
export class TemplateTabConfigurationModel implements ITemplateTabConfigurationModel {
    Id: number = 0;
    TemplateTabId: number = 0;
    TemplateTabIndex: number = 0;
    RowId: number = 0;
    ColumnId: number = 0;
    ColumnSpan: number = 0;
    DisplayFormatTypeId: number = 0;
    IsDeleted: boolean = false;
}

export interface ITemplateTabConfigurationRowModel {
    templateTabId: number;
    rowId: number,
    columns: Array<ITemplateTabConfigurationColumnsModel>
}

export interface ITemplateTabConfigurationColumnsModel {
    id: number;
    rowId: number,
    columnId: number,
    columnSpan: number,
    templateTabTableConfigurationId: number,
    templateTabTableConfigurationName: string,
    templateTabTableConfigurationLastUpdatedBy: string
    isTabTableConfigExists: boolean,
    chartTypeId: number,
    templateTabChartConfigurationId: number,
    templateTabChartConfigurationName: string,
    templateTabChartConfigurationLastUpdatedBy: string
    isTabChartConfigExists: boolean,
    displayFormatTypeId: number
}

export interface ITemplateTabPreviewModel {
    id: number;
    templateId: number;
    templateTabName: string,
    templateTabConfigurationPreviewModel: Array<ITemplateTabConfigurationPreviewRowModel>
}

export interface ITemplateTabConfigurationPreviewRowModel {
    templateTabId: number;
    rowId: number,
    columns: Array<ITemplateTabConfigurationPreviewColumnsModel>
}

export interface ITemplateTabConfigurationPreviewColumnsModel {
    id: number;
    rowId: number,
    columnId: number,
    columnSpan: number,
    isDeleted: boolean,
    displayFormatTypeId: number,
    tabTableConfigurationId: number,
    tabTableConfigurationName: number,
    tabChartConfigurationId: number,
    tabChartConfigurationName: number,
    templateTabTableConfigurationPreviewViewModel: ITemplateTabTableConfigurationDetailsPreviewRowsDbModel,
    templateTabChartConfigurationPreviewViewModel: ITemplateTabChartConfigurationPreviewViewModel,
    tabTableConfiguration: any,
    tabChartConfiguration: any
}