export interface IChartLineThicknessModel {
    Id: number,
    Name: string
    IsDeleted: boolean
}

export interface IChartLineThicknessDbModel {
    id: number,
    name: string
    isDeleted: boolean
}