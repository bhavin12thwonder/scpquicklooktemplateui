export interface IATDPosition {
    id: number,
    name: string
    isDeleted: boolean
}