export interface IChartTypeModel {
    Id: number,
    Name: string,
    ChartTypeValue: string,
    IsDeleted: boolean
}

export interface IChartTypeDbModel {
    id: number,
    name: string
    chartTypeValue: string,
    isDeleted: boolean
}