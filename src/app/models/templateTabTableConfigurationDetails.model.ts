export interface ITemplateTabTableConfigurationDetailsRowsModel {
    Id: number,
    TemplateTabTableConfigurationId: number;
    RowId: number,
    TotalRows: number,
    TotalCoumns: number,
    Columns: Array<ITemplateTabTableConfigurationDetailsColumnsModel>
}

export interface ITemplateTabTableConfigurationDetailsColumnsModel {
    Id: number,
    RowId: number,
    ColumnId: number,
    RowSpan: number,
    ColumnSpan: number,
    ColumnWidth: number,
    ColumnType: number,
    UserDefinedColumn: string,
    DbColumn: string,
    ValueType: string,
    AtdPosition: string,
    ConditionalUserDefinedColumn: string,
    ResultColumn: string,
    ColourCode: string,
    IsConfigHeader: boolean,
    IsHeader: boolean,
    IsDbColumn: boolean,
    IsUserDefined: boolean,
    IsConditional: boolean,
    IsReadOnly: boolean,
    FormulaOperatorId: number,
    TemplateTabTableConfigurationFormulaId: number,
    ShowConditionalDefinedValueTextBox: boolean,
    IsDeleted: boolean,
    ShowDeleteIcon: boolean,
    TDSelectionCss: string
}

export interface ITemplateTabTableConfigurationDetailsDbModel {
    id: number,
    templateTabTableConfigurationId: number,
    templateTabTableConfigurationName: string,
    rowId: number,
    rowSpan: number,
    columnId: number,
    columnSpan: number,
    columnWidth: number,
    columnType: number,
    userDefinedColumn: string,
    dbColumn: string,
    valueType: string,
    atdPosition: string,
    conditionalUserDefinedColumn: string,
    resultColumn: string,
    colourCode: string,
    isConfigHeader: boolean,
    isHeader: boolean,
    isDbColumn: boolean,
    isUserDefined: boolean,
    isConditional: boolean,
    isReadOnly: boolean,
    formulaOperatorId: number,
    templateTabTableConfigurationFormulaId: number,
    isDeleted: boolean,
    isParentDisabled: boolean
}

export class TemplateTabTableConfigurationDetailsDbModel implements ITemplateTabTableConfigurationDetailsDbModel {
    id: number = 0;
    templateTabTableConfigurationId: number = 0;
    templateTabTableConfigurationName: string = "";
    rowId: number = 0;
    rowSpan: number = 0;
    columnId: number = 0;
    columnSpan: number = 0;
    columnWidth: number = 0;
    columnType: number = 0;
    userDefinedColumn: string = "";
    dbColumn: string = "";
    valueType: string = "";
    atdPosition: string = "";
    conditionalUserDefinedColumn: string = "";
    resultColumn: string = "";
    colourCode: string = "";
    isConfigHeader: boolean = false;
    isHeader: boolean = false;
    isDbColumn: boolean = false;
    isUserDefined: boolean = false;
    isConditional: boolean = false;
    isReadOnly: boolean = false;
    formulaOperatorId: number = 0;
    templateTabTableConfigurationFormulaId: number = 0;
    isDeleted: boolean = false;
    isParentDisabled: boolean = false;
}

export interface ITemplateTabTableConfigurationDetailsRowsDbModel {
    templateTabTableConfigurationId: number;
    rowId: number,
    columns: Array<ITemplateTabTableConfigurationDetailsColumnsDbModel>
}

export interface ITemplateTabTableConfigurationDetailsColumnsDbModel {
    id: number,
    rowId: number,
    columnId: number,
    rowSpan: number,
    columnSpan: number,
    columnWidth: number,
    columnType: number,
    userDefinedColumn: string,
    dbColumn: string,
    valueType: string,
    atdPosition: string,
    conditionalUserDefinedColumn: string,
    resultColumn: string,
    colourCode: string,
    isDeleted: boolean,
    isConfigHeader: boolean,
    isHeader: boolean,
    isDbColumn: boolean,
    isUserDefined: boolean,
    isConditional: boolean,
    isReadOnly: boolean,
    formulaOperatorId: number,
    templateTabTableConfigurationFormulaId: number
}
export interface ITemplateTabTableConfigurationDetailsPreviewRowsDbModel {
    templateTabTableConfigurationId: number,
    templateTabTableConfigurationName: string,
    rowId: number,
    columns: Array<ITemplateTabTableConfigurationDetailsPreviewColumnsDbModel>
}
export interface ITemplateTabTableConfigurationDetailsPreviewColumnsDbModel {
    id: number,
    rowId: number,
    columnId: number,
    rowSpan: number,
    columnSpan: number,
    columnWidth: number,
    columnType: number,
    dbColumn: string,
    columnHeader: string,
    columnData: string,
    colourCode: string,
    isHeader: boolean,
    isDeleted: boolean
}