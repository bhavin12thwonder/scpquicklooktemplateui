import { IUniqueIdsViewModel } from './chartChannel.model';

export interface IDynamicTableRows {
    TemplateTabId: number;
    TemplateTabIndex: number
    RowId: number,
    RowName: string,
    IsDeleted: boolean,
    Columns: Array<IDynamicTableRowColumns>
}

export interface IDynamicTableRowColumns {
    Id: number;
    ColumnId: number,
    ColumnName: string,
    DisplayFormatType: string,
    DisplayFormatTypeId: number,
    ColumnSpan: number,
    TemplateTabTableConfigurationId: number,
    TemplateTabTableConfigurationName: string,
    TemplateTabTableConfigurationLastUpdatedBy: string,
    IsTabTableConfigExists: boolean,
    ChartTypeId: number,
    TemplateTabChartConfigurationId: number,
    TemplateTabChartConfigurationName: string,
    TemplateTabChartConfigurationLastUpdatedBy: string,
    IsTabChartConfigExists: boolean,
    IsDeleted: boolean
}

export class DynamicTableRows implements IDynamicTableRows {
    TemplateTabId: number = 0;
    TemplateTabIndex: number = 0;
    RowId: number = 0;
    RowName: string = '';
    IsDeleted: boolean = false;
    Columns: Array<IDynamicTableRowColumns> = [];
}

export class DynamicTableRowColumns implements IDynamicTableRowColumns {
    Id: number = 0;
    ColumnId: number = 0;
    ColumnName: string = '';
    DisplayFormatType: string = "";
    DisplayFormatTypeId: number = 0;
    ColumnSpan: number = 1;
    TemplateTabTableConfigurationId: number = 0;
    TemplateTabTableConfigurationName: string = "";
    TemplateTabTableConfigurationLastUpdatedBy: string = "";
    IsTabTableConfigExists: boolean = false;
    ChartTypeId: number = 0;
    TemplateTabChartConfigurationId: number = 0;
    TemplateTabChartConfigurationName: string = '';
    TemplateTabChartConfigurationLastUpdatedBy: string = '';
    IsTabChartConfigExists: boolean = false;
    IsDeleted: boolean = false;
}

export interface ITemplateModel {
    id: number,
    name: string,
    testTypeId: number,
    //testModeId: number,
    testModeIds: Array<IUniqueIdsViewModel>,
    testModeList: Array<number>,
    isDeleted: boolean
}
export class TemplateModel implements ITemplateModel {
    id: number = 0;
    name: string = '';
    testTypeId: number = 0;
    //testModeId: number = 0;
    testModeIds: Array<IUniqueIdsViewModel> = [];
    testModeList: Array<number> = [];
    isDeleted: boolean = false;
}
