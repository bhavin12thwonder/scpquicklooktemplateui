export interface ITemplateTabChartConfigurationModel {
    Id: number,
    TemplateTabConfigId: number,
    Name: string,
    ChartTypeId: number,
    IsDeleted: boolean,
    IsParentDisabled: boolean,
    ChartOrientation: string
}
export interface ITemplateTabChartConfigurationDbModel {
    id: number,
    templateTabConfigId: number,
    name: string,
    chartTypeId: number,
    isDeleted: boolean,
    isParentDisabled: boolean,
    chartOrientation: string
}