export interface IChartChannelModel {
    Id: number,
    TemplateTabChartConfigurationId: number,
    Name: string,
    ChartLineColour: string,
    ChartLineStyleId: number,
    ChartLineThicknessId: number,
    LegendName: string,
    GoodValue: number,
    AcceptableValue: number,
    MarginalValue: number,
    PoorValue: number,
    ATDPosition: string,
    ATDPositionNames: Array<string>,
    //ATDPositionIds: Array<number>,
    ATDPositionIds: Array<IUniqueIdsViewModel>,
    FillArea: boolean,
    Polarity: boolean,
    IsLegendVisible: boolean,
    IsAreaChart: boolean,
    IsDeleted: boolean,
    SiUnit: string
}


export interface IChartChannelDbModel {
    id: number,
    templateTabChartConfigurationId: number,
    name: string,
    chartLineColour: string,
    chartLineStyleId: number,
    chartLineThicknessId: number,
    legendName: string,
    goodValue: number,
    acceptableValue: number,
    marginalValue: number,
    poorValue: number,
    aTDPosition: Array<number>,
    aTDPositionIds: Array<IUniqueIdsViewDbModel>,
    aTDPositionIdsArray: Array<number>,
    aTDPositionNames: Array<string>,
    fillArea: boolean,
    ploarity: string,
    isLegendVisible: string,
    isAreaChart: boolean,
    isDeleted: boolean,
    SiUnit: string
}

export interface IUniqueIdsViewModel {
    Id: number
    //AtdPositionName: string
}
export interface IUniqueIdsViewDbModel {
    id: number
    //atdPositionName: string
}