export interface ITabConfigurationTableRows {
    RowId: number,
    RowName: string,
    IsDeleted: boolean,
    Columns: Array<ITabConfigurationTableRowsColumns>
}

export interface ITabConfigurationTableRowsColumns {
    Id: number;
    ColumnId: number,
    ColumnName: string,
    ColumnSpan: number,
    ColumnType: number,
    IsHeader: boolean,
    IsDeleted: boolean,
    ShowDeleteIcon: boolean
}

export class TabConfigurationTableRows implements ITabConfigurationTableRows {
    RowId: number = 0;
    RowName: string = '';
    IsDeleted: boolean = false;
    Columns: Array<ITabConfigurationTableRowsColumns> = [];
}

export class TabConfigurationTableRowsColumns implements ITabConfigurationTableRowsColumns {
    Id: number = 0;
    ColumnId: number = 0;
    ColumnName: string = '';
    ColumnSpan: number = 1;
    ColumnType: number = 0;
    IsHeader: boolean = false;
    IsDeleted: boolean = false;
    ShowDeleteIcon: boolean = false;
}