export interface ICalculationValue {
    name: string,
    value: number,
    Unit: string
}

export interface IChannelData {
    name: string,
    value: Array<number>,
    Unit: string
}

export interface IChannelConvertData {
    ChannelName: string,
    Unit: string,
    TestId: number,
    SubMatrix: string
}