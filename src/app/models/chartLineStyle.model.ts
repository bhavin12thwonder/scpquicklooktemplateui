export interface IChartLineStyleModel {
    Id: number,
    Name: string,
    Style: string
    StyleValue: string
    IsDeleted: boolean
}

export interface IChartLineStyleDbModel {
    id: number,
    name: string,
    style: string,
    styleValue: string,
    isDeleted: boolean
}