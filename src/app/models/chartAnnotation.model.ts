export interface IChartAnnotationModel {
    Id: number,
    TemplateTabChartConfigurationId: number,
    AnnotationLineStartXCoOrdinate: string,
    AnnotationLineEndXCoOrdinate: string,
    AnnotationLineStartYCoOrdinate: string,
    AnnotationLineEndYCoOrdinate: string,
    AnnotationLineThicknessId: number,
    AnnotationLineColour: string,
    AnnotationLineStyleId: number,
    AnnotationText: string,
    AnnotationTextStartXCoOrdinate: string,
    AnnotationTextStartYCoOrdinate: string,
    IsDeleted: boolean
}

export interface IChartAnnotationDbModel {
    id: number,
    templateTabChartConfigurationId: number,
    annotationLineStartXCoOrdinate: string,
    annotationLineEndXCoOrdinate: string,
    annotationLineStartYCoOrdinate: string,
    annotationLineEndYCoOrdinate: string,
    annotationLineThicknessId: number,
    annotationLineColour: string,
    annotationLineStyleId: number,
    annotationText: string,
    annotationTextStartXCoOrdinate: string,
    annotationTextStartYCoOrdinate: string,
    isDeleted: boolean
}