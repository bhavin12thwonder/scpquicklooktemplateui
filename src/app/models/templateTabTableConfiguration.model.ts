export interface ITemplateTabTableConfigurationModel {
    Id: number,
    TemplateTabConfigId: number,
    Name: string,
    IsDeleted: boolean,
    IsParentDisabled: boolean
}
export class TemplateTabTableConfigurationModel implements ITemplateTabTableConfigurationModel {
    Id: number = 0;
    TemplateTabConfigId: number = 0;
    Name: string = '';
    IsDeleted: boolean = false;
    IsParentDisabled: boolean = false;
}