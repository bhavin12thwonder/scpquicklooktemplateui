export interface ITemplateTabModel {
    Id: number,
    Name: string,
    TemplateId: number,
    TabIndex: number,
    DisplayOrder: number,
    Rows: number,
    Columns: number,
    RowsLimit: number,
    IsDeleted: boolean
}
export class TemplateTabModel implements ITemplateTabModel {
    Id: number = 0;
    Name: string = '';
    TemplateId: number = 0;
    TabIndex: number = 0;
    DisplayOrder: number = 0;
    Rows: number = 0;
    Columns: number = 0;
    RowsLimit: number = 0;
    IsDeleted: boolean = false;
}