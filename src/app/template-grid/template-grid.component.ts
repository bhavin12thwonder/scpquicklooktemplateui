import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Router } from '@angular/router';
import { TemplateService } from 'src/services/template.service';



interface Status {
  name: string,
  code: string
}
@Component({
  selector: 'template-grid',
  templateUrl: 'template-grid.component.html',
  styleUrls: ['./template-grid.component.css']
})
export class TemplateGridComponent implements OnInit {
  status: SelectItem[];
  selectedStatus: Status;
  templates:Template[];
  templateData: Array<any>;
  totalRecords: number;
  first = 0;
  rows = 5;
  displayData = [];

  constructor(private templateService:TemplateService,private router:Router) {
    this.status = [
      { label: 'Active', value: false },
      { label: 'InActive', value: true },
      { label: 'All', value: "all" },
  ];
   }

  ngOnInit(): void {
    this.getTemplate();
  }
  onNavigate() {
    console.log("under navigation");
    this.router.navigate(['/addTemplate']);
 }

  changeGridByStatus(name){
    if(name !== "all")
    {
        this.displayData = this.templateData.filter(m => m.isDeleted === name );
    }
    else{
      this.displayData = this.templateData;
    }
        //this.ProductDetails = obj;  
        //return this.ProductDetails; 
  }

  next(){
    this.first = this.first + this.rows;
  }
  prev(){
    this.first = this.first - this.rows;
  }
  reset() {
    this.first = 0;
}

isLastPage(): boolean {
    return this.first === (this.templateData.length - this.rows);
}

isFirstPage(): boolean {
    return this.first === 0;
}
  getTemplate()
  {
    this.templateService.getTemplate().subscribe(res => {
      this.templateData= res;
      this.changeGridByStatus(false);
      //this.displayData=this.templateData;
      //console.log(res);
    })
  //this.totalRecords = this.templateData.length;
  }
}
