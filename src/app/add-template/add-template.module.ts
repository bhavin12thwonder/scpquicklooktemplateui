import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddTemplateComponent } from './add-template.component';
import { Route } from '@angular/compiler/src/core';
import { Routes,RouterModule } from '@angular/router';

const routes: Routes = [
  {path:'',component:AddTemplateComponent}
 ];

@NgModule({
  declarations: [AddTemplateComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class AddTemplateModule { }
