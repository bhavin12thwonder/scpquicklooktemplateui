import { environment } from '../../environments/environment';

export const baseApiUrl = `${environment.baseApiUrl}`;