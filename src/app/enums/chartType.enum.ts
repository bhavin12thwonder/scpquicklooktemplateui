export enum ChartType {
    LineChart = 1,
    BarChart = 2,
    AreaChart = 3,
    StackBarChart = 4
}