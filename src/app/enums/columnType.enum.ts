export enum ColumnType {
    DbColumn = 0,
    DbTestResult = 1,
    UserDefinedColumn = 2,
    ConditionalColumn = 3
} 