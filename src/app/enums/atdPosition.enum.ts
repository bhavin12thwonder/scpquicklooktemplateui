export enum ATDPosition {
    Driver = 1,
    Passenger = 2,
    RRL = 3,
    RRC = 4,
    RRR = 5,
    RL = 6,
    RC = 7,
    RR = 8
}