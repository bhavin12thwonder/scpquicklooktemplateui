import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { MenuItem } from 'primeng/primeng';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbsService {
  private itemsSource$ = new BehaviorSubject<MenuItem[]>([]);
  private controlSettings$ = new Subject<any>();
  itemsHandler = this.itemsSource$.asObservable();
  settingsHandler = this.controlSettings$.asObservable();

  constructor() { }
  setItems(items: MenuItem[], controlObj = {}) {
    this.itemsSource$.next(items);
    this.controlSettings$.next(controlObj);
  }
}
