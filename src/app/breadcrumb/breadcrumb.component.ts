import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { from, Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { BreadcrumbsService } from '../breadcrumb/breadcrumbs.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styles: [`
        .layout-breadcrumb{
          margin-top:1em;
        }
        .layout-breadcrumb ul li {          
            padding: 0 4px;
          display: inline-block;
          vertical-align: middle;
        }
    `],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BreadcrumbComponent implements OnInit {
  items$: Observable<MenuItem[]>;
  settings$: Observable<{ controls: Array<any> }>;

  constructor(public breadcrumbService: BreadcrumbsService
  ) { }

  ngOnInit(): void {
    this.items$ = this.breadcrumbService.itemsHandler.pipe(
      delay(0),
      map(response => {
        this.setTitle(response);
        return response;
      })
    );
    this.settings$ = this.breadcrumbService.settingsHandler.pipe(
      map(response => {
        if (response) {
          return response;
        } else {
          return { controls: [] };
        }
      })
    );
  }

  setTitle(items) {
    let title = '';
    if (items && items.length > 0) {
      title = "QuickLook - " + items[items.length - 1].label;
    }
    document.title = title;
  }
}
