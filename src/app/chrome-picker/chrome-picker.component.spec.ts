import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChromePickerComponent } from './chrome-picker.component';

describe('ChromePickerComponent', () => {
  let component: ChromePickerComponent;
  let fixture: ComponentFixture<ChromePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChromePickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChromePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
