import { NgModule } from '@angular/core';
import { ColorPickerModule } from '@iplab/ngx-color-picker';
import { ChromePickerComponent } from './../chrome-picker/chrome-picker.component';
import { CommonModule } from '@angular/common';


@NgModule({
    declarations: [
        ChromePickerComponent
    ],
    imports: [
        CommonModule,
        ColorPickerModule
    ],
    exports: [
        ChromePickerComponent
    ]
})
export class ChromePickerModule { }
