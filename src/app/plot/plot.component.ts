import { Component, OnInit, Input, NgZone, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';

declare var Plotly: any;

@Component({
  selector: 'plot-chart',
  //templateUrl: './plotly.component.html',
  template: `
        <div>
            <div id="chart{{ id }}"></div>
        </div>
    `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlotComponent implements OnInit {

  constructor(private zone: NgZone) { }

  @Input() name: string;
  @Input() data: any[];
  @Input() layout: any[];
  @Input() options: any;
  @Input() id: string = '';
  @Input() viewInitialized: boolean = false;
  _container;

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    let d3 = Plotly.d3;
    let gd3 = d3
      .select(`#chart${this.id}`)
      .append('div');
    this._container = gd3.node();

    this.plotData();
  }

  ngOnChanges() {
    if (this.viewInitialized) {
      this.plotData();
    }
  }

  plotData() {
    let self = this;
    this.zone.runOutsideAngular(() => {
      Plotly.newPlot(
        self._container,
        self.data,
        self.layout,
        self.options
      );
    });
  }
}
