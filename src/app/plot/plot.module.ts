import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { SelectButtonModule } from 'primeng/selectbutton';
import { TooltipModule } from 'primeng/tooltip';
import { PlotComponent } from '../plot/plot.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MultiSelectModule,
        ButtonModule,
        DropdownModule,
        CheckboxModule,
        SelectButtonModule,
        TooltipModule,
        InputTextModule
    ],
    declarations: [PlotComponent],
    exports: [PlotComponent, CommonModule]
})
export class PlotModule { }
