import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownModule } from 'primeng/dropdown';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TemplateGridComponent } from '../app/template/template-grid/template-grid.component';
import { TemplateService } from '../services/template.service';
import { Routes, RouterModule } from '@angular/router';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { MessageService, ConfirmationService } from 'primeng/api';
import { FormsModule } from '@angular/forms';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { from } from 'rxjs';
import { NotificationService } from './notification-system/notification.service';
import { NotificationListComponent } from './notification-system/notification.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { TemplateGridModule } from '../app/template/template-grid/template-grid.module';
import { NgxSpinnerModule } from "ngx-spinner";
import { HttpRequestInterceptor } from './HttpRequestInterceptor';
import { ColorPickerModule } from '@iplab/ngx-color-picker';
import { ChromePickerModule } from './chrome-picker/chrome-picker.module';

const routes: Routes = [
  { path: '', component: TemplateGridComponent },
  // {
  //   path: 'quicklookTemplate',
  //   loadChildren: () => import('../app/template/template-grid/template-grid.module').then(m => m.TemplateGridModule)
  // },
  {
    path: 'addTemplate',
    loadChildren: () => import('../app/template/add-template/add-template.module').then(m => m.AddTemplateModule)
  },
  {
    path: 'editTemplate/:id',
    loadChildren: () => import('../app/template/edit-template/edit-template.module').then(m => m.EditTemplateModule)
  }
];

@NgModule({
  declarations: [
    AppComponent,
    TemplateGridComponent,
    NotificationListComponent,
    BreadcrumbComponent
  ],
  imports: [
    BreadcrumbModule,
    ButtonModule,
    TabViewModule,
    TableModule,
    FormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    ColorPickerModule,
    ChromePickerModule,
    HttpClientModule,
    CommonModule,
    DropdownModule,
    ConfirmDialogModule,
    ToastModule,
    MessagesModule,
    MessageModule,
    RouterModule,
    NgxSpinnerModule,
    RouterModule.forRoot(routes)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
    TemplateService, NotificationService, MessageService, ConfirmationService
  ],
  bootstrap: [AppComponent],
  exports: [RouterModule]
})
export class AppModule { }
