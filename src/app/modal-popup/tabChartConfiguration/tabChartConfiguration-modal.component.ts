import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { MessageService, ConfirmationService } from 'primeng/api';
import { ChartType } from '../../enums/chartType.enum';
import { ITemplateTabConfigurationModel } from '../../models/templateTabConfiguration.model';
import { IChartAnnotationModel, IChartAnnotationDbModel } from '../../models/chartAnnotation.model';
import { IChartChannelDbModel, IChartChannelModel, IUniqueIdsViewModel, IUniqueIdsViewDbModel } from '../../models/chartChannel.model';
import { IChartTypeModel, IChartTypeDbModel } from '../../models/chartType.model';
import { IChartLineThicknessModel, IChartLineThicknessDbModel } from '../../models/chartLineThickness.model';
import { IChartLineStyleModel, IChartLineStyleDbModel } from '../../models/chartLineStyle.model';
import { ITemplateTabChartConfigurationModel, ITemplateTabChartConfigurationDbModel } from '../../models/templateTabChartConfiguration.model';
import { ITemplateTabChartConfigurationDetailsModel, ITemplateTabChartConfigurationDetailsUpsertModel, ITemplateTabChartConfigurationDetailsDbModel, IAxisUnitValue } from '../../../app/models/templateTabChartConfigurationDetails.model';
import { IATDPosition } from 'src/app/models/atdPosition.model';
import { TemplateService } from '../../../services/template.service';
import { TemplateTabConfigurationService } from '../../../services/templateTabConfiguration.service';
import { ChartAnnotationService } from '../../../services/chartAnnotation.service';
import { ChartChannelService } from '../../../services/chartChannel.service';
import { ChartTypeService } from '../../../services/chartType.service';
import { ChartLineThicknessService } from '../../../services/chartLineThickness.service';
import { ChartLineStyleService } from '../../../services/chartLineStyle.service';
import { TemplateTabChartConfigurationService } from '../../../services/templateTabChartConfiguration.service';
import { TemplateTabChartConfigurationDetailsService } from '../../../services/templateTabChartConfigurationDetails.service';
import { ASAMService } from '../../../services/asam.service';
import { ATDPositionService } from 'src/services/atdPosition.service';
import { parse } from 'querystring';
import { ColorPickerControl } from '@iplab/ngx-color-picker';
import { IChannelConvertData } from 'src/app/models/asam.model';

var _self: any;
declare var $: any;
@Component({
    selector: 'tabChartConfiguration-modal',
    templateUrl: './tabChartConfiguration-modal.component.html',
    styleUrls: ['./tabChartConfiguration-modal.component.scss'],
    styles: [`
    :host ::ng-deep .ui-dropdown {
                        display: inline-flex;
                        position: relative;
                        cursor: pointer;
                        min-width: 15em;
                        float:left;
                        }
    :host ::ng-deep .ui-multiselect {
                            display: inline-flex;
                            position: relative;
                            cursor: pointer;
                            min-width: 15em;
                        }

    :host ::ng-deep .ui-dialog {
                    padding: 0;
                    pointer-events: auto;
                    display: flex;
                    flex-direction: column;
                    position: relative;
                }
    `]
})

export class TabChartConfigurationModalComponent implements OnInit {
    @Input() templateId: number;
    @Input() testId: number;
    @Input() submatrixName: string;
    @Input() templateTabId: number;
    @Input() templateTabConfigIndexId: number;
    @Input() templateTabConfigId: number;
    @Input() templateTabChartConfigId: number;
    @Input() templateTabChartConfigName: string;
    @Input() chartTypeId: number;
    @Input() displayFormatType: string;
    @Input() isVisibleModal: boolean = false;
    @Input() isEditChartConfiguration: boolean;
    @Output() isVisibleModalChange = new EventEmitter();
    @Output() onLoadTemplatTabEditConfiguration = new EventEmitter();
    isChartConfigDetailsExists: boolean = false;
    showPreviewTabChartConfigurationDialog: boolean = false;
    showChartConfigurationDetails: boolean = false;
    showChartChannelConfiguration: boolean = false;
    showChartChannelConfigurationData: boolean = false;
    showStackBarChartChannelConfiguration: boolean = false;
    showStackBarChartChannelConfigurationData: boolean = false;
    showChartAnnotationConfiguration: boolean = false;
    showChartAnnotationConfigurationData: boolean = false;
    showChartConfigurationDetailsSaveButton: boolean = false;
    showFillArea: boolean = false;
    showATDPosition: boolean = false;
    showXAxisChannel: boolean = true;
    channelsDropdownDataList: Array<any> = [];
    chartTypeDropdownDataList: Array<IChartTypeModel> = [];
    chartChannelDropdownDataList: Array<IChartChannelModel> = [];
    chartChannelDataList: Array<IChartChannelDbModel> = [];
    stackBarChartChannelDropdownDataList: Array<IChartChannelModel> = [];
    stackBarChartChannelDataList: Array<IChartChannelDbModel> = [];
    chartATDPositionDropdownDataList: Array<IATDPosition> = [];
    chartLineThicknessDropdownDataList: Array<IChartLineThicknessModel> = [];
    chartLineStyleDropdownDataList: Array<IChartLineStyleModel> = [];
    chartChannelModel: IChartChannelModel = <IChartChannelModel>{};
    chartChannelDbModel: IChartChannelDbModel = <IChartChannelDbModel>{};
    chartChannelListModel: Array<IChartChannelModel> = [];
    stackBarChartChannelModel: IChartChannelModel = <IChartChannelModel>{};
    stackBarChartChannelDbModel: IChartChannelDbModel = <IChartChannelDbModel>{};
    stackBarChartChannelListModel: Array<IChartChannelModel> = [];
    uniqueIdsViewModel: Array<IUniqueIdsViewModel> = [];
    uniqueIdsViewDbModel: Array<IUniqueIdsViewDbModel> = [];
    chartAnnotationModel: IChartAnnotationModel = <IChartAnnotationModel>{};
    chartAnnotationDbModel: IChartAnnotationDbModel = <IChartAnnotationDbModel>{};
    chartAnnotationListModel: Array<any> = [];
    chartAnnotationDataList: Array<IChartAnnotationDbModel> = [];
    axisUnitValues: Array<IAxisUnitValue> = [];
    yaxisUnitValue: Array<IAxisUnitValue> = [];

    tabChartConfigData: Array<ITemplateTabChartConfigurationDetailsModel> = [];
    templateTabChartConfigurationModel: ITemplateTabChartConfigurationModel = <ITemplateTabChartConfigurationModel>{};
    templateTabChartConfigurationDetailsModel: ITemplateTabChartConfigurationDetailsModel = <ITemplateTabChartConfigurationDetailsModel>{};
    templateTabChartConfigurationDetailsUpsertModel: ITemplateTabChartConfigurationDetailsUpsertModel = <ITemplateTabChartConfigurationDetailsUpsertModel>{};
    channelConvertData: IChannelConvertData = <IChannelConvertData>{};
    chartTypeDisabled: boolean;
    showOrientation: boolean = false;
    selectedATDPositionNames: Array<any> = [];
    atdPositionNamesArray: Array<any> = [];
    defaultLegendText: string;
    xaxisChannel: string;
    SiUnit: string;
    xAxisMin: number;
    xAxisMax: number;
    xAxisInterval: number;
    yAxisMin: number;
    yAxisMax: number;
    yAxisInterval: number;
    constructor(
        private notifyService: MessageService,
        private confirmationService: ConfirmationService,
        private templateService: TemplateService,
        private templateTabConfigurationService: TemplateTabConfigurationService,
        private chartAnnotationService: ChartAnnotationService,
        private chartChannelService: ChartChannelService,
        private chartTypeService: ChartTypeService,
        private chartLineThicknessService: ChartLineThicknessService,
        private chartLineStyleService: ChartLineStyleService,
        private templateTabChartConfigurationService: TemplateTabChartConfigurationService,
        private templateTabChartConfigurationDetailsService: TemplateTabChartConfigurationDetailsService,
        private asamService: ASAMService,
        private atdPositionService: ATDPositionService
    ) {
        this.axisUnitValues = [
            { label: 's', value: 's' },
            { label: 'ms', value: 'ms' }
        ];
    }

    parentEventHandlerFunction(pickedColor, field, data: any = null) {
        // debugger;
        if (field == 'chartLine') {
            this.chartChannelModel.ChartLineColour = pickedColor;
            if (data != null) {
                data.ChartLineColour = pickedColor;
                this.onUpdateChartChannel(data);
            }
        }
        else if (field == 'annotation')
            this.chartAnnotationModel.AnnotationLineColour = pickedColor;
    }

    ngOnInit() {
        _self = this;
        this.loadChartTypeDropdown();
        this.loadChartLineStyleDropdown();
        this.loadChartLineThicknessDropdown();
        this.loadATDPosition();
    }
    public orientationList: any = [
        { label: 'Horizontal', value: 'h' },
        { label: 'Vertical', value: 'v' }
    ];

    success(message, summaryMessage = 'Success Message') {
        this.notifyService.add({ severity: 'success', summary: summaryMessage, detail: message });
    }

    error(message, summaryMessage = 'Error Message') {
        this.notifyService.add({ severity: 'error', summary: summaryMessage, detail: message });
    }

    showModal() {
        if (this.chartTypeId === ChartType.AreaChart) {
            this.showFillArea = true;
        }
        else {
            this.showFillArea = false;
        }

        if (this.chartTypeId === ChartType.BarChart) {
            this.showATDPosition = true;
        }
        else {
            this.showATDPosition = false;
        }

        if (this.chartTypeId === ChartType.BarChart || this.chartTypeId === ChartType.StackBarChart) {
            this.showOrientation = true;
            this.showXAxisChannel = false;
        }
        else {
            this.showOrientation = false;
            this.showXAxisChannel = true;
        }

        if (this.chartTypeId === ChartType.LineChart || this.chartTypeId === ChartType.AreaChart) {
            this.loadChannelsDropdown();
        }
        else if (this.chartTypeId === ChartType.BarChart || this.chartTypeId === ChartType.StackBarChart) {
            this.loadCalculationsDropdown();
        }

        if (this.isEditChartConfiguration == true) {
            this.chartTypeDisabled = true;
            this.showChartConfigurationDetails = true;
            this.showChartConfigurationDetailsSaveButton = true;

            this.loadChartConfiguration();
            this.loadChartDetailsConfiguration();
            this.loadChartChannelConfiguration();
            this.loadChartAnnotationConfiguration();

        }
        else {
            this.chartTypeDisabled = false;
        }
    }

    cancelModal() {
        this.onSubmitTabChartConfigurationDetails('save', true);
    }

    reSetFormData() {
        this.tabChartConfigData = [];
        this.chartChannelModel = <IChartChannelModel>{};
        this.chartChannelDbModel = <IChartChannelDbModel>{};
        this.chartChannelListModel = [];
        this.showStackBarChartChannelConfiguration = false;
        this.stackBarChartChannelModel = <IChartChannelModel>{};
        this.chartAnnotationModel = <IChartAnnotationModel>{};
        this.chartAnnotationDbModel = <IChartAnnotationDbModel>{};
        this.chartAnnotationListModel = [];
        this.chartChannelDataList = [];
        this.stackBarChartChannelDataList = [];

        this.templateTabChartConfigurationModel = <ITemplateTabChartConfigurationModel>{};
        this.templateTabChartConfigurationDetailsModel = <ITemplateTabChartConfigurationDetailsModel>{};
        this.templateTabChartConfigurationDetailsUpsertModel = <ITemplateTabChartConfigurationDetailsUpsertModel>{};

        this.showChartConfigurationDetails = false;
        this.showChartChannelConfiguration = false;
        this.showChartAnnotationConfiguration = false;
        this.showChartChannelConfigurationData = false;
        this.showStackBarChartChannelConfiguration = false;
        this.showStackBarChartChannelConfigurationData = false;
        this.showChartConfigurationDetailsSaveButton = false;
    }

    public orientionList: any = [
        { label: 'Horizontal', value: 'h' },
        { label: 'Vertical', value: 'v' }
    ];

    onShowChartChannelConfiguration() {
        this.chartChannelModel = <IChartChannelModel>{};
        this.showStackBarChartChannelConfiguration = false;
        this.chartChannelModel.FillArea = false;
        this.showChartChannelConfiguration = false;
        this.chartChannelModel.IsLegendVisible = true;
        this.chartChannelModel.Polarity = false;
        this.chartChannelModel.ChartLineStyleId = 1;
        this.chartChannelModel.ChartLineThicknessId = 1;

        if (this.chartTypeId === ChartType.StackBarChart) {
            this.showStackBarChartChannelConfiguration = true;
        }
        else if (this.chartTypeId === ChartType.AreaChart) {
            this.chartChannelModel.FillArea = true;
            this.showChartChannelConfiguration = true;
        }
        else {
            this.showChartChannelConfiguration = true;
        }
    }

    onShowChartAddAnnotation() {
        this.showChartAnnotationConfiguration = true;
    }

    onChartTypeChange(chartTypeId) {
        if (chartTypeId === ChartType.BarChart) {
            this.showFillArea = true;
        }
        else {
            this.showFillArea = false;
        }

        if (chartTypeId === ChartType.BarChart) {
            this.showOrientation = true;
            this.showXAxisChannel = false;
        }
        else {
            this.showOrientation = false;
        }

        if (chartTypeId === ChartType.BarChart || chartTypeId === ChartType.StackBarChart) {
            this.showXAxisChannel = false;
        }
        else {
            this.showXAxisChannel = true;
        }
    }

    loadChartTypeDropdown() {
        this.chartTypeService.getAll().subscribe(response => {
            this.chartTypeDropdownDataList = [];
            this.chartTypeDropdownDataList = response.map(item => {
                return { label: item.name, value: item.id };
            });
        });
    }

    loadCalculationsDropdown() {
        this.channelsDropdownDataList = [];
        this.templateService.getById(this.templateId).subscribe(res => {
            this.asamService.getASAMCalculation(res.testType, res.testMode).subscribe(response => {
                this.channelsDropdownDataList = response.map(item => {
                    return { label: item, value: item };
                });
            });
        });
    }

    loadChannelsDropdown() {
        this.channelsDropdownDataList = [];
        this.templateService.getById(this.templateId).subscribe(res => {
            this.asamService.getASAMChannels(res.testType, res.testModeNames).subscribe(response => {
                this.channelsDropdownDataList = response.map(item => {
                    return { label: item, value: item };
                });

                if (response.indexOf('Time') >= 0 && this.showXAxisChannel) {
                    this.xaxisChannel = 'Time';
                    this.onXaxisChannelChange(this.xaxisChannel);

                } else {
                    this.loadXaxisChannelUnitDropDown();
                }
            });
        });
    }

    loadATDPosition() {
        this.atdPositionService.getATDPositionData().subscribe(response => {
            this.chartATDPositionDropdownDataList = [];
            this.chartATDPositionDropdownDataList = response.map(item => {
                return { label: item.name, value: item.id };
            });
        });
    }

    loadChartChannelDropdown() {
        this.chartChannelDropdownDataList = [];
        if (this.templateTabChartConfigId > 0) {
            this.chartChannelService.getXaxisChannel(this.templateTabChartConfigId).subscribe(response => {
                this.chartChannelDropdownDataList = response.map(item => {
                    return { label: item.name, value: item.id };
                });
            });
        }
    }

    loadXaxisChannelUnitDropDown() {
        this.chartChannelService.getXaxisChannel(this.templateTabChartConfigId).subscribe(result => {
            if (result.length > 0) {
                this.xaxisChannel = result[0].name;
                this.asamService.getChannelUnit(this.xaxisChannel).subscribe(response => {
                    if (response.length > 0) {
                        this.axisUnitValues = [];
                        this.axisUnitValues = response.map(item => {
                            return { label: item, value: item };
                        });
                        this.loadYaxisChannelUnitDropDown();
                    } else {
                        this.loadYaxisChannelUnitDropDown();
                    }
                });
            }
        });
    }

    loadYaxisChannelUnitDropDown() {
        this.chartChannelService.getByChartConfigId(this.templateTabChartConfigId).subscribe(response => {
            if (response.length > 0) {
                var yaxisChannel = response[0].name;
                this.asamService.getChannelUnit(yaxisChannel).subscribe(response => {
                    this.yaxisUnitValue = [];
                    this.yaxisUnitValue = response.map(item => {
                        return { label: item, value: item };
                    });

                    if (this.testId != undefined && this.testId != null && !this.templateTabChartConfigurationDetailsModel.YAxisAutoFill) {
                        this.asamService.getASAMChannelData(this.testId, yaxisChannel).subscribe(result => {
                            if (result != null && result.length > 0) {
                                var minY = Math.min.apply(Math, result[0].Value);
                                var maxY = Math.max.apply(Math, result[0].Value);
                                var intervalY = Math.abs((minY + maxY) / 20);
                                this.yAxisMin = minY.toFixed(2);
                                this.yAxisMax = maxY.toFixed(2);
                                this.yAxisInterval = Math.round(intervalY * 100) / 100;
                                this.templateTabChartConfigurationDetailsModel.YAxisUnits = result[0].Unit;
                            } else {
                                this.error("Error while fetching data or data unavailable");
                            }
                        });
                    } else {
                        this.error("Please select a test to find min max");
                    }
                });
            }
        });
    }

    loadChartLineThicknessDropdown() {
        this.chartLineThicknessService.getAll().subscribe(response => {
            this.chartLineThicknessDropdownDataList = [];
            this.chartLineThicknessDropdownDataList = response.map(item => {
                return { label: item.name, value: item.id };
            });
        });
    }

    loadChartLineStyleDropdown() {
        this.chartLineStyleService.getAll().subscribe(response => {
            this.chartLineStyleDropdownDataList = [];
            this.chartLineStyleDropdownDataList = response.map(item => {
                return { label: item.name + ' (' + item.style + ')', value: item.id };
            });
        });
    }

    loadChartChannelConfiguration() {
        if (this.templateTabChartConfigId > 0) {
            let chartChannelDataList: any = [];
            this.chartChannelService.getByChartConfigId(this.templateTabChartConfigId).subscribe(response => {
                chartChannelDataList = response;
                if (chartChannelDataList.length > 0) {
                    this.chartChannelDataList = chartChannelDataList;
                    this.showStackBarChartChannelConfigurationData = false;
                    this.showChartChannelConfigurationData = false;
                    this.chartChannelDataList = [];
                    this.stackBarChartChannelDataList = [];
                    if (this.chartTypeId === ChartType.StackBarChart) {
                        this.stackBarChartChannelDataList = chartChannelDataList;
                        this.showStackBarChartChannelConfigurationData = true;
                    }
                    else {
                        this.chartChannelDataList = chartChannelDataList;
                        this.showChartChannelConfigurationData = true;
                    }
                }
            });
        }
    }

    loadChartConfiguration() {
        this.templateTabChartConfigurationService.getById(this.templateTabChartConfigId).subscribe(response => {
            if (response !== null) {
                this.templateTabChartConfigurationModel.Id = response.id;
                this.templateTabChartConfigurationModel.ChartTypeId = response.chartTypeId;
                this.templateTabChartConfigurationModel.Name = response.name;
                this.showChartConfigurationDetails = true;
                this.showStackBarChartChannelConfigurationData = false;
                this.showChartChannelConfigurationData = false;

                if (this.chartTypeId === ChartType.StackBarChart) {
                    this.showStackBarChartChannelConfigurationData = true;
                }
                else {
                    this.showChartChannelConfigurationData = true;
                }
            }
        });
    }

    loadChartDetailsConfiguration() {
        this.templateTabChartConfigurationDetailsService.getByTemplateTabChartConfigId(this.templateTabChartConfigId).subscribe(response => {
            if (response !== null) {
                this.isChartConfigDetailsExists = true;
                this.templateTabChartConfigurationDetailsModel.Id = response.id;
                this.templateTabChartConfigurationDetailsModel.XAxisChannelId = response.xAxisChannelId;
                this.templateTabChartConfigurationDetailsModel.XAxisInterval = response.xAxisInterval;
                this.templateTabChartConfigurationDetailsModel.XAxisMaxValue = response.xAxisMaxValue;
                this.templateTabChartConfigurationDetailsModel.XAxisMinValue = response.xAxisMinValue;
                this.templateTabChartConfigurationDetailsModel.XAxisUnits = response.xAxisUnits;
                this.templateTabChartConfigurationDetailsModel.YAxisInterval = response.yAxisInterval;
                this.templateTabChartConfigurationDetailsModel.YAxisMaxValue = response.yAxisMaxValue;
                this.templateTabChartConfigurationDetailsModel.YAxisMinValue = response.yAxisMinValue;
                this.templateTabChartConfigurationDetailsModel.YAxisUnits = response.yAxisUnits;
                this.templateTabChartConfigurationDetailsModel.XAxisAutoFill = response.xAxisAutoFill;
                this.templateTabChartConfigurationDetailsModel.YAxisAutoFill = response.yAxisAutoFill;
                this.xAxisMin = this.templateTabChartConfigurationDetailsModel.XAxisMinValue;
                this.templateTabChartConfigurationDetailsModel.XAxisLabel = response.xAxisLabel;
                this.templateTabChartConfigurationDetailsModel.YAxisLabel = response.yAxisLabel;
                this.xAxisMax = this.templateTabChartConfigurationDetailsModel.XAxisMaxValue;
                this.xAxisInterval = this.templateTabChartConfigurationDetailsModel.XAxisInterval;
                this.yAxisMin = this.templateTabChartConfigurationDetailsModel.YAxisMinValue;
                this.yAxisMax = this.templateTabChartConfigurationDetailsModel.YAxisMaxValue;
                this.yAxisInterval = this.templateTabChartConfigurationDetailsModel.YAxisInterval;

            }
        });
    }

    loadChartAnnotationConfiguration() {
        this.chartAnnotationService.getByChartConfigId(this.templateTabChartConfigId).subscribe(response => {
            this.chartAnnotationDataList = response;
            if (this.chartAnnotationDataList.length > 0) {
                this.showChartAnnotationConfigurationData = true;
            }
        });
    }

    onChannelChange(channelValue) {
        this.asamService.getPhysicalUnit(channelValue).subscribe(response => {
            this.chartChannelService.getByChartConfigId(this.templateTabChartConfigId).subscribe(result => {
                if (result.length > 0) {
                    if (result[0].siUnit != response) {
                        this.error("Please select same physical unit channel");
                        this.chartChannelModel = <IChartChannelModel>{};
                        this.defaultLegendText = "";
                        this.SiUnit = response;
                        return;
                    }
                    else {
                        this.defaultLegendText = channelValue;
                        this.SiUnit = response;
                    }
                } else {
                    this.defaultLegendText = channelValue;
                    this.SiUnit = response;
                }
            });
        });
    }

    onXaxisChannelChange(channelValue) {
        this.chartChannelModel.TemplateTabChartConfigurationId = this.templateTabChartConfigId
        this.chartChannelModel.LegendName = channelValue;
        this.chartChannelModel.Name = channelValue;
        if (channelValue != null && channelValue != '' && channelValue != undefined) {
            this.chartChannelService.updateXAxisChannel(this.chartChannelModel).subscribe(response => {
                if (response.isSuccess) {
                    this.asamService.getChannelUnit(channelValue).subscribe(response => {
                        this.axisUnitValues = [];
                        this.axisUnitValues = response.map(item => {
                            return { label: item, value: item };
                        });
                        if (this.testId != undefined && this.testId != null && !this.templateTabChartConfigurationDetailsModel.XAxisAutoFill) {
                            this.asamService.getASAMChannelData(this.testId, channelValue).subscribe(result => {
                                if (result != null && result.length > 0) {
                                    var minX = Math.min.apply(Math, result[0].Value);
                                    var maxX = Math.max.apply(Math, result[0].Value);
                                    var intervalX = Math.abs((minX + maxX) / 20);
                                    this.xAxisMin = minX.toFixed(2);
                                    this.xAxisMax = maxX.toFixed(2);
                                    this.xAxisInterval = Math.round(intervalX * 100) / 100;
                                    this.templateTabChartConfigurationDetailsModel.XAxisUnits = result[0].Unit;
                                } else {
                                    this.error("Error while fetching data or data unavailable");
                                }
                            });
                        }
                        this.loadYaxisChannelUnitDropDown();
                    });
                }
            });
        }
    }

    onAxisUnitChange(unitValue, axisValue) {
        if (axisValue === 'x' && this.templateTabChartConfigurationDetailsModel.XAxisAutoFill) {
            return;
        }
        else if (axisValue === 'y' && this.templateTabChartConfigurationDetailsModel.YAxisAutoFill) {
            return;
        }
        this.channelConvertData = <IChannelConvertData>{};
        this.channelConvertData.Unit = unitValue;
        this.channelConvertData.SubMatrix = this.submatrixName;
        this.channelConvertData.TestId = this.testId;
        this.channelConvertData.ChannelName = this.xaxisChannel;

        if (this.channelConvertData.ChannelName == undefined) {
            this.error("Please select a x-axis channel");
        }
        else {
            if (this.testId != undefined && this.submatrixName != undefined) {
                this.asamService.getConvertDataUnit(this.channelConvertData).subscribe(response => {
                    if (response != null && response.length > 0) {
                        if (axisValue === 'x') {
                            var minX = Math.min.apply(Math, response[0].Value);
                            var maxX = Math.max.apply(Math, response[0].Value);
                            var intervalX = Math.abs((minX + maxX) / 20);
                            this.xAxisMin = minX.toFixed(2);
                            this.xAxisMax = maxX.toFixed(2);
                            this.xAxisInterval = Math.round(intervalX * 100) / 100;
                            this.templateTabChartConfigurationDetailsModel.XAxisUnits = response[0].Unit;
                        }
                        else if (axisValue === 'y') {
                            var minY = Math.min.apply(Math, response[0].Value);
                            var maxY = Math.max.apply(Math, response[0].Value);
                            var intervalY = Math.abs((minY + maxY) / 20);
                            this.yAxisMin = minY.toFixed(2);
                            this.yAxisMax = maxY.toFixed(2);
                            this.yAxisInterval = Math.round(intervalY * 100) / 100;
                            this.templateTabChartConfigurationDetailsModel.YAxisUnits = response[0].Unit;
                        }
                    }
                });
            }
        }
    }

    onAxisAutoFillCriteriaChange(value, axisValue, channelName = null) {
        debugger;
        if (this.testId != undefined && this.testId != null) {
            if (axisValue === 'x' && !value) {
                this.asamService.getASAMChannelData(this.testId, channelName).subscribe(result => {
                    if (result != null && result.length > 0) {
                        var minX = Math.min.apply(Math, result[0].Value);
                        var maxX = Math.max.apply(Math, result[0].Value);
                        var intervalX = Math.abs((minX + maxX) / 20);
                        this.xAxisMin = minX.toFixed(2);
                        this.xAxisMax = maxX.toFixed(2);
                        this.xAxisInterval = Math.round(intervalX * 100) / 100;
                        this.templateTabChartConfigurationDetailsModel.XAxisUnits = result[0].Unit;
                    } else {
                        this.error("Error while fetching data or data unavailable");
                    }
                });
            }
            else if (axisValue === 'x' && !value) {
                this.chartChannelService.getByChartConfigId(this.templateTabChartConfigId).subscribe(response => {
                    if (response.length > 0) {
                        var yaxisChannel = response[0].name;
                        this.asamService.getChannelUnit(yaxisChannel).subscribe(response => {
                            this.yaxisUnitValue = [];
                            this.yaxisUnitValue = response.map(item => {
                                return { label: item, value: item };
                            });
                            this.asamService.getASAMChannelData(this.testId, yaxisChannel).subscribe(result => {
                                if (result != null && result.length > 0) {
                                    var minY = Math.min.apply(Math, result[0].Value);
                                    var maxY = Math.max.apply(Math, result[0].Value);
                                    var intervalY = Math.abs((minY + maxY) / 20);
                                    this.yAxisMin = minY.toFixed(2);
                                    this.yAxisMax = maxY.toFixed(2);
                                    this.yAxisInterval = Math.round(intervalY * 100) / 100;
                                    this.templateTabChartConfigurationDetailsModel.YAxisUnits = result[0].Unit;
                                } else {
                                    this.error("Error while fetching data or data unavailable");
                                }
                            });
                        });
                    }
                });
            }
        } else {
            this.error("Please select a test to find min max");
        }
    }

    onAddNewChartChannel() {
        debugger;
        this.chartChannelModel.TemplateTabChartConfigurationId = this.templateTabChartConfigId;
        this.chartChannelModel.LegendName = this.defaultLegendText;
        this.chartChannelModel.SiUnit = this.SiUnit;
        if (this.chartChannelModel.Name != undefined || this.chartChannelModel.Name != null) {
            if (this.chartChannelModel.ChartLineStyleId != undefined) {
                if (this.chartChannelModel.ChartLineThicknessId != undefined) {
                    if (this.chartChannelModel.LegendName != undefined || this.chartChannelModel.LegendName != null) {
                        if (this.chartChannelModel.Polarity != undefined) {
                            if (this.chartChannelModel.IsLegendVisible != undefined) {
                                if (this.chartChannelModel.ChartLineColour === undefined || this.chartChannelModel.ChartLineColour === null) {
                                    this.chartChannelModel.ChartLineColour = '#FF0000';
                                }
                                this.chartChannelService.postData(this.chartChannelModel).subscribe(response => {
                                    if (response.isSuccess) {
                                        //this.loadChartChannelDropdown();
                                        this.loadChartChannelConfiguration();
                                        this.defaultLegendText = "";
                                        this.SiUnit = "";
                                        this.success('Channel Successfully Added');
                                        this.chartChannelModel = <IChartChannelModel>{};
                                        this.chartChannelModel.IsLegendVisible = true;
                                        this.chartChannelModel.Polarity = false;
                                        this.chartChannelModel.ChartLineStyleId = 1;
                                        this.chartChannelModel.ChartLineThicknessId = 1;
                                        this.chartChannelModel.ChartLineColour = '#FF0000';
                                        if (this.chartTypeId === ChartType.AreaChart) {
                                            this.chartChannelModel.FillArea = true;
                                        }
                                    }
                                });
                            }
                            else {
                                this.error("Please select legend visibility");
                            }
                        }
                        else {
                            this.error("Please select polarity");
                        }
                    }
                    else {
                        this.error("Please enter legend name");
                    }
                }
                else {
                    this.error("Please select thickness");
                }
            }
            else {
                this.error("Please select style");
            }
        }
        else {
            this.error("Please select channel");
        }
    }

    onUpdateChartChannelName(chartChannelData) {
        this.asamService.getPhysicalUnit(chartChannelData.name).subscribe(response => {
            this.chartChannelService.getByChartConfigId(this.templateTabChartConfigId).subscribe(result => {
                if (result.length > 0) {
                    if (result[0].siUnit != response) {
                        this.error("Please select same physical unit channel");
                        this.chartChannelModel = <IChartChannelModel>{};
                        this.defaultLegendText = "";
                        this.SiUnit = response;
                        return;
                    }
                    else {
                        this.defaultLegendText = chartChannelData.name;
                        this.SiUnit = response;
                        this.onUpdateChartChannel(chartChannelData);
                    }
                } else {
                    this.defaultLegendText = chartChannelData.name;
                    this.SiUnit = response;
                    this.onUpdateChartChannel(chartChannelData);
                }
            });
        });

    }

    onUpdateChartChannel(chartChannelData) {
        debugger;
        chartChannelData.templateTabChartConfigurationId = this.templateTabChartConfigId;
        this.chartChannelService.putData(chartChannelData).subscribe(response => {
            if (response.isSuccess) {
                //this.loadChartChannelDropdown();
                this.loadChartChannelConfiguration();
                this.success('Channel Successfully Added');
            }
        });
    }

    onDeleteChartChannel(chartChannelId) {
        this.confirmationService.confirm({
            header: "Delete Confirmation",
            message: "Are you sure?",
            key: 'deleteChaartChannelConfirmation',
            accept: () => {
                this.chartChannelService.deleteData(chartChannelId).subscribe(response => {
                    if (response.isSuccess) {
                        //this.loadChartChannelDropdown();
                        this.loadChartChannelConfiguration();
                        this.success('Chart Channel Successfully Deleted');
                    }
                });
            }
        });
    }

    onCancelChartChannel() {
        this.showChartChannelConfiguration = false;
        this.chartChannelModel = <IChartChannelModel>{};
    }

    onAddNewStackBarChartChannel() {
        this.stackBarChartChannelModel.TemplateTabChartConfigurationId = this.templateTabChartConfigId;
        this.stackBarChartChannelModel.LegendName = this.defaultLegendText;
        this.stackBarChartChannelModel.SiUnit = this.SiUnit;
        if (this.stackBarChartChannelModel.Name != undefined || this.stackBarChartChannelModel.Name != null) {
            if (this.stackBarChartChannelModel.GoodValue != undefined || this.stackBarChartChannelModel.GoodValue != null) {
                if (this.stackBarChartChannelModel.AcceptableValue != undefined || this.stackBarChartChannelModel.AcceptableValue != null) {
                    if (this.stackBarChartChannelModel.MarginalValue != undefined || this.stackBarChartChannelModel.MarginalValue != null) {
                        if (this.stackBarChartChannelModel.PoorValue != undefined || this.stackBarChartChannelModel.PoorValue != null) {
                            if (this.stackBarChartChannelModel.LegendName != undefined || this.stackBarChartChannelModel.LegendName != null) {
                                if (this.stackBarChartChannelModel.Polarity != undefined) {
                                    if (this.stackBarChartChannelModel.IsLegendVisible != undefined) {
                                        if (this.stackBarChartChannelModel.ATDPositionIds != undefined || this.stackBarChartChannelModel.ATDPositionIds.length > 0) {
                                            if (this.stackBarChartChannelModel.ChartLineColour === undefined || this.stackBarChartChannelModel.ChartLineColour === null) {
                                                this.stackBarChartChannelModel.ChartLineColour = '#FF0000';
                                            }
                                            this.stackBarChartChannelModel.ATDPositionIds = this.uniqueIdsViewModel;
                                            this.chartChannelService.postData(this.stackBarChartChannelModel).subscribe(response => {
                                                if (response.isSuccess) {
                                                    //this.loadChartChannelDropdown();
                                                    this.loadChartChannelConfiguration();
                                                    this.success('Channel Successfully Added');
                                                    this.stackBarChartChannelModel = <IChartChannelModel>{};
                                                }
                                            });
                                        }
                                        else {
                                            this.error("Please select atd position");
                                        }
                                    }
                                    else {
                                        this.error("Please select legend visibility");
                                    }
                                }
                                else {
                                    this.error("Please select polarity");
                                }
                            }
                            else {
                                this.error("Please enter legend name");
                            }
                        }
                        else {
                            this.error("Please enter poor value");
                        }
                    }
                    else {
                        this.error("Please enter marginal value");
                    }
                }
                else {
                    this.error("Please enter acceptable value");
                }

            }
            else {
                this.error("Please enter good value");
            }
        }
        else {
            this.error("Please select channel");
        }
    }

    onUpdateStackBarChartChannel(stackBarhartChannelData) {
        stackBarhartChannelData.templateTabChartConfigurationId = this.templateTabChartConfigId;
        this.stackBarChartChannelModel.ATDPositionIds = this.uniqueIdsViewModel;
        this.chartChannelService.putData(stackBarhartChannelData).subscribe(response => {
            if (response.isSuccess) {
                //this.loadChartChannelDropdown();
                this.loadChartChannelConfiguration();
                this.success('Channel Successfully Added');
            }
        });
    }

    onDeleteStackBarChartChannel(stackBarhartchartChannelId) {
        this.confirmationService.confirm({
            header: "Delete Confirmation",
            message: "Are you sure?",
            key: 'deleteChaartChannelConfirmation',
            accept: () => {
                this.chartChannelService.deleteData(stackBarhartchartChannelId).subscribe(response => {
                    if (response.isSuccess) {
                        //this.loadChartChannelDropdown();
                        this.loadChartChannelConfiguration();
                        this.success('Chart Channel Successfully Deleted');
                    }
                });
            }
        });
    }

    onCancelStackBarChartChannel() {
        this.showStackBarChartChannelConfiguration = false;
        this.stackBarChartChannelModel = <IChartChannelModel>{};
    }

    onAddNewChartAnnotation() {
        this.chartAnnotationModel.TemplateTabChartConfigurationId = this.templateTabChartConfigId;
        let chartAnnotationModel: IChartAnnotationModel = <IChartAnnotationModel>{};
        if (this.chartAnnotationModel.AnnotationText != undefined || this.chartAnnotationModel.AnnotationText != null) {
            if (this.chartAnnotationModel.AnnotationLineStyleId != undefined) {
                if (this.chartAnnotationModel.AnnotationLineThicknessId != undefined) {
                    if (this.chartAnnotationModel.AnnotationLineStartXCoOrdinate != undefined || this.chartAnnotationModel.AnnotationLineStartXCoOrdinate != null) {
                        if (this.chartAnnotationModel.AnnotationLineEndXCoOrdinate != undefined || this.chartAnnotationModel.AnnotationLineEndXCoOrdinate != null) {
                            if (this.chartAnnotationModel.AnnotationTextStartXCoOrdinate != undefined || this.chartAnnotationModel.AnnotationTextStartXCoOrdinate != null) {
                                if (this.chartAnnotationModel.AnnotationLineStartYCoOrdinate != undefined || this.chartAnnotationModel.AnnotationLineStartYCoOrdinate != null) {
                                    if (this.chartAnnotationModel.AnnotationLineEndYCoOrdinate != undefined || this.chartAnnotationModel.AnnotationLineEndYCoOrdinate != null) {
                                        if (this.chartAnnotationModel.AnnotationTextStartYCoOrdinate != undefined || this.chartAnnotationModel.AnnotationTextStartYCoOrdinate != null) {
                                            chartAnnotationModel = this.chartAnnotationModel;
                                            if (chartAnnotationModel.AnnotationLineColour != undefined || chartAnnotationModel.AnnotationLineColour != null) {
                                                chartAnnotationModel.AnnotationLineColour = '#000000';
                                            }
                                            this.chartAnnotationListModel.push(chartAnnotationModel);
                                            this.chartAnnotationModel = <IChartAnnotationModel>{};
                                        }
                                        else {
                                            this.error("Please enter annotation y-axis coordinate text");
                                        }
                                    }
                                    else {
                                        this.error("Please enter annotation y-axis end coordinate");
                                    }
                                }
                                else {
                                    this.error("Please enter annotation y-axis start coordinate");
                                }
                            }
                            else {
                                this.error("Please enter annotation x-axis coordinate text");
                            }
                        }
                        else {
                            this.error("Please enter annotation x-axis end coordinate");
                        }
                    }
                    else {
                        this.error("Please enter annotation x-axis start coordinate");
                    }
                }
                else {
                    this.error("Please select annotation line thickness");
                }
            }
            else {
                this.error("Please select annotation line style");
            }
        }
        else {
            this.error("Please enter annotation text");
        }
    }

    onUpdateChartAnnotation(chartAnnotationData) {
        chartAnnotationData.templateTabChartConfigurationId = this.templateTabChartConfigId;
        this.chartAnnotationService.putData(chartAnnotationData).subscribe(response => {
            if (response.isSuccess) {
                this.loadChartAnnotationConfiguration();
                this.success('Chart Annotation Successfully Updated');
            }
        });
    }

    onDeleteChartAnnotation(chartAnnotationId) {
        this.confirmationService.confirm({
            header: "Delete Confirmation",
            message: "Are you sure?",
            key: 'deleteChaartAnnotationConfirmation',
            accept: () => {
                this.chartAnnotationService.deleteData(chartAnnotationId).subscribe(response => {
                    if (response.isSuccess) {
                        this.loadChartAnnotationConfiguration();
                        this.success('Chart Annotation Successfully Deleted');
                    }
                });
            }
        });
    }

    onCancelChartAnnotation() {
        this.showChartAnnotationConfiguration = false;
        this.chartAnnotationModel = <IChartAnnotationModel>{};
    }

    onUpdateTemplateTabConfiguration(templateTabConfigId) {
        this.templateTabConfigurationService.getById(templateTabConfigId).subscribe(response => {
            let templateTabConfigurationData = <ITemplateTabConfigurationModel>{};
            templateTabConfigurationData = response;
            templateTabConfigurationData.TemplateTabId = response.templateTabId;
            templateTabConfigurationData.Id = response.id;
            templateTabConfigurationData.RowId = response.rowId;
            templateTabConfigurationData.ColumnId = response.columnId;
            templateTabConfigurationData.ColumnSpan = response.columnSpan;
            templateTabConfigurationData.DisplayFormatTypeId = (this.displayFormatType.split('_')[0].toString() == "Table") ? 1 : (this.displayFormatType.split('_')[0].toString() == "Chart") ? 2 : 3;
            templateTabConfigurationData.IsDeleted = response.isDeleted;
            this.templateTabConfigurationService.putDataById(templateTabConfigurationData).subscribe(response => {
                if (response.isSuccess) {
                    this.success('Tab Configuration Successfully Updated');
                }
            });
        });
    }

    onUpsertTabChartConfiguration() {
        if (this.templateTabChartConfigurationModel.ChartOrientation == undefined) {
            this.templateTabChartConfigurationModel.ChartOrientation = 'v';
        }
        this.templateTabChartConfigurationModel.TemplateTabConfigId = this.templateTabConfigId;
        if (this.templateTabChartConfigurationModel.Name != undefined || this.templateTabChartConfigurationModel.Name != null) {
            this.templateTabChartConfigurationService.checkTemplateTabChartConfigName(this.templateTabChartConfigurationModel.TemplateTabConfigId, this.templateTabChartConfigurationModel.ChartTypeId, this.templateTabChartConfigurationModel.Name).subscribe(result => {
                if (!result) {
                    if (this.isEditChartConfiguration) {
                        this.templateTabChartConfigurationService.putData(this.templateTabChartConfigurationModel).subscribe(response => {
                            if (response.isSuccess) {
                                this.templateTabChartConfigId = response.id;
                                this.onUpdateTemplateTabConfiguration(this.templateTabConfigId);
                                this.success('Tab Chart Configuration Successfully Updated');
                            }
                            else {
                                this.error("Failed To Update");
                            }
                        });
                    }
                    else {
                        this.templateTabChartConfigurationService.postData(this.templateTabChartConfigurationModel).subscribe(response => {
                            if (response.isSuccess) {
                                this.templateTabChartConfigId = response.id;
                                this.onUpdateTemplateTabConfiguration(this.templateTabConfigId);

                                this.showChartConfigurationDetails = true;
                                this.isEditChartConfiguration = true;
                                this.showChartConfigurationDetailsSaveButton = true;

                                this.showStackBarChartChannelConfigurationData = false;
                                this.showChartChannelConfigurationData = false;
                                this.chartTypeId = this.templateTabChartConfigurationModel.ChartTypeId;

                                if (this.chartTypeId === ChartType.LineChart || this.chartTypeId === ChartType.AreaChart) {
                                    this.loadChannelsDropdown();
                                    this.showXAxisChannel = true;
                                }
                                else if (this.chartTypeId === ChartType.BarChart || this.chartTypeId === ChartType.StackBarChart) {
                                    this.loadCalculationsDropdown();
                                    this.showXAxisChannel = false;
                                }

                                if (this.chartTypeId === ChartType.StackBarChart) {
                                    this.showStackBarChartChannelConfigurationData = true;
                                }
                                else {
                                    this.showChartChannelConfigurationData = true;
                                }

                                if (this.chartTypeId === ChartType.BarChart) {
                                    this.showATDPosition = true;
                                    this.showOrientation = true;
                                }
                                else {
                                    this.showATDPosition = false;
                                    this.showOrientation = false;
                                }

                                this.success('Tab Chart Configuration Successfully Saved');
                            }
                            else {
                                this.error("Failed To Update");
                            }
                        });
                    }
                }
                else {
                    this.error("Config Name Is Already Exists");
                }
            });

        } else {
            this.error("Please Enter Configuration Name");
        }
    }

    onSubmitTabChartConfigurationDetails(saveOrPreview: string, closePopup: boolean = null) {
        var tempId = this.templateTabChartConfigurationDetailsModel.Id;
        this.templateTabChartConfigurationDetailsService.getByTemplateTabChartConfigId(this.templateTabChartConfigId).subscribe(response => {
            tempId = response.id;
            if (this.isEditChartConfiguration && this.isChartConfigDetailsExists) {
                this.onUpdateTabChartConfigurationDetails(saveOrPreview, tempId);
            }
            else {
                this.onSaveTabChartConfigurationDetails(saveOrPreview, tempId);
            }
            if (closePopup) {
                this.reSetFormData();
                this.isVisibleModalChange.emit(this.isVisibleModal);
                this.onLoadTemplatTabEditConfiguration.emit(this.templateTabId + "_" + this.templateTabConfigIndexId);
            }
            this.loadChartAnnotationConfiguration();
        });
    }

    onSaveTabChartConfigurationDetails(saveOrPreview: string, tempId: number) {

        this.showPreviewTabChartConfigurationDialog = false;
        this.templateTabChartConfigurationDetailsUpsertModel.Id = tempId;//this.templateTabChartConfigurationDetailsModel.Id;
        this.templateTabChartConfigurationDetailsUpsertModel.TemplateTabChartConfigurationId = this.templateTabChartConfigId;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisChannelId = this.templateTabChartConfigurationDetailsModel.XAxisChannelId;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisAutoFill = this.templateTabChartConfigurationDetailsModel.XAxisAutoFill;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisUnits = this.templateTabChartConfigurationDetailsModel.XAxisUnits;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisLabel = this.templateTabChartConfigurationDetailsModel.XAxisLabel;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisMinValue = this.xAxisMin;//this.templateTabChartConfigurationDetailsModel.XAxisMinValue;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisMaxValue = this.xAxisMax;//this.templateTabChartConfigurationDetailsModel.XAxisMaxValue;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisInterval = this.xAxisInterval;//this.templateTabChartConfigurationDetailsModel.XAxisInterval;
        this.templateTabChartConfigurationDetailsUpsertModel.YAxisUnits = this.templateTabChartConfigurationDetailsModel.YAxisUnits;
        this.templateTabChartConfigurationDetailsUpsertModel.YAxisMinValue = this.yAxisMin;//this.templateTabChartConfigurationDetailsModel.YAxisMinValue;
        this.templateTabChartConfigurationDetailsUpsertModel.YAxisMaxValue = this.yAxisMax;//this.templateTabChartConfigurationDetailsModel.YAxisMaxValue;
        this.templateTabChartConfigurationDetailsUpsertModel.YAxisInterval = this.yAxisInterval;//this.templateTabChartConfigurationDetailsModel.YAxisInterval;
        this.templateTabChartConfigurationDetailsUpsertModel.YAxisLabel = this.templateTabChartConfigurationDetailsModel.YAxisLabel;
        this.templateTabChartConfigurationDetailsUpsertModel.YAxisAutoFill = this.templateTabChartConfigurationDetailsModel.YAxisAutoFill;
        this.templateTabChartConfigurationDetailsUpsertModel.IsDeleted = false;
        this.templateTabChartConfigurationDetailsUpsertModel.IsParentDisabled = this.templateTabChartConfigurationDetailsModel.IsParentDisabled;
        this.templateTabChartConfigurationDetailsUpsertModel.ChartAnnotationList = this.chartAnnotationListModel;
        this.templateTabChartConfigurationDetailsUpsertModel.TemplateTabChartConfigurationName = this.templateTabChartConfigurationModel.Name;

        if (this.templateTabChartConfigurationDetailsUpsertModel.XAxisUnits != undefined || this.templateTabChartConfigurationDetailsUpsertModel.XAxisUnits != null) {
            if (this.templateTabChartConfigurationDetailsUpsertModel.YAxisUnits != undefined || this.templateTabChartConfigurationDetailsUpsertModel.YAxisUnits != null) {
                if (this.chartTypeId === ChartType.LineChart || this.chartTypeId === ChartType.AreaChart) {
                    if (this.templateTabChartConfigurationDetailsUpsertModel.XAxisMinValue != undefined || this.templateTabChartConfigurationDetailsUpsertModel.XAxisMinValue != null) {
                        if (this.templateTabChartConfigurationDetailsUpsertModel.XAxisMaxValue != undefined || this.templateTabChartConfigurationDetailsUpsertModel.XAxisMaxValue != null) {
                            if (this.templateTabChartConfigurationDetailsUpsertModel.XAxisInterval != undefined || this.templateTabChartConfigurationDetailsUpsertModel.XAxisInterval != null) {
                                //if (this.templateTabChartConfigurationDetailsUpsertModel.XAxisChannelId != undefined) {
                                if (this.templateTabChartConfigurationDetailsUpsertModel.YAxisMinValue != undefined || this.templateTabChartConfigurationDetailsUpsertModel.YAxisMinValue != null) {
                                    if (this.templateTabChartConfigurationDetailsUpsertModel.YAxisMaxValue != undefined || this.templateTabChartConfigurationDetailsUpsertModel.YAxisMaxValue != null) {
                                        if (this.templateTabChartConfigurationDetailsUpsertModel.YAxisInterval != undefined || this.templateTabChartConfigurationDetailsUpsertModel.YAxisInterval != null) {
                                            this.templateTabChartConfigurationDetailsService.putData(this.templateTabChartConfigurationDetailsUpsertModel).subscribe(response => {
                                                if (response.isSuccess) {
                                                    this.success('Tab Chart Configuration Details Successfully Saved');
                                                    if (saveOrPreview === "preview") {
                                                        this.showPreviewTabChartConfigurationDialog = true;
                                                    }
                                                }
                                                else if (response.Message === "Configuration Name Alsready Exists") {
                                                    this.error("Configuration Name Alsready Exists");
                                                }
                                                else {
                                                    this.error("Tab Chart Configuration Details Failed To Save");
                                                }
                                            });
                                        }
                                        else {
                                            this.error("Enter y-axis interval");
                                        }
                                    }
                                    else {
                                        this.error("Enter y-axis max value");
                                    }
                                }
                                else {
                                    this.error("Enter y-axis min value");
                                }
                                //}
                                // else {
                                //     this.error("Select x-axis channel");
                                // }
                            }
                            else {
                                this.error("Enter x-axis interval");
                            }
                        }
                        else {
                            this.error("Enter x-axis max value");
                        }
                    }
                    else {
                        this.error("Enter x-axis min value");
                    }
                }
                else {
                    this.templateTabChartConfigurationDetailsService.postData(this.templateTabChartConfigurationDetailsUpsertModel).subscribe(response => {
                        if (response.isSuccess) {
                            this.success('Tab Chart Configuration Details Successfully Saved');
                            if (saveOrPreview === "preview") {
                                this.showPreviewTabChartConfigurationDialog = true;
                            }
                        }
                        else {
                            this.error("Tab Chart Configuration Details Failed To Save");
                        }
                    });
                }
            }

            else {
                this.error("Enter y-axis units");
            }
        }
        else {
            this.error("Enter x-axis units");
        }
    }

    onUpdateTabChartConfigurationDetails(saveOrPreview: string, tempId: number) {
        this.showPreviewTabChartConfigurationDialog = false;
        this.templateTabChartConfigurationDetailsUpsertModel.Id = tempId;//this.templateTabChartConfigurationDetailsModel.Id;
        this.templateTabChartConfigurationDetailsUpsertModel.TemplateTabChartConfigurationId = this.templateTabChartConfigId;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisChannelId = this.templateTabChartConfigurationDetailsModel.XAxisChannelId;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisAutoFill = this.templateTabChartConfigurationDetailsModel.XAxisAutoFill;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisUnits = this.templateTabChartConfigurationDetailsModel.XAxisUnits;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisLabel = this.templateTabChartConfigurationDetailsModel.XAxisLabel;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisMinValue = this.xAxisMin;//this.templateTabChartConfigurationDetailsModel.XAxisMinValue;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisMaxValue = this.xAxisMax;//this.templateTabChartConfigurationDetailsModel.XAxisMaxValue;
        this.templateTabChartConfigurationDetailsUpsertModel.XAxisInterval = this.xAxisInterval;//this.templateTabChartConfigurationDetailsModel.XAxisInterval;
        this.templateTabChartConfigurationDetailsUpsertModel.YAxisUnits = this.templateTabChartConfigurationDetailsModel.YAxisUnits;
        this.templateTabChartConfigurationDetailsUpsertModel.YAxisMinValue = this.yAxisMin;//this.templateTabChartConfigurationDetailsModel.YAxisMinValue;
        this.templateTabChartConfigurationDetailsUpsertModel.YAxisMaxValue = this.yAxisMax;//this.templateTabChartConfigurationDetailsModel.YAxisMaxValue;
        this.templateTabChartConfigurationDetailsUpsertModel.YAxisInterval = this.yAxisInterval;//this.templateTabChartConfigurationDetailsModel.YAxisInterval;
        this.templateTabChartConfigurationDetailsUpsertModel.YAxisLabel = this.templateTabChartConfigurationDetailsModel.YAxisLabel;
        this.templateTabChartConfigurationDetailsUpsertModel.YAxisAutoFill = this.templateTabChartConfigurationDetailsModel.YAxisAutoFill;
        this.templateTabChartConfigurationDetailsUpsertModel.IsDeleted = false;
        this.templateTabChartConfigurationDetailsUpsertModel.IsParentDisabled = this.templateTabChartConfigurationDetailsModel.IsParentDisabled;
        this.templateTabChartConfigurationDetailsUpsertModel.ChartAnnotationList = this.chartAnnotationListModel;
        this.templateTabChartConfigurationDetailsUpsertModel.TemplateTabChartConfigurationName = this.templateTabChartConfigurationModel.Name;

        if (this.templateTabChartConfigurationDetailsUpsertModel.XAxisUnits != undefined || this.templateTabChartConfigurationDetailsUpsertModel.XAxisUnits != null) {
            if (this.templateTabChartConfigurationDetailsUpsertModel.YAxisUnits != undefined || this.templateTabChartConfigurationDetailsUpsertModel.YAxisUnits != null) {
                if (this.chartTypeId === ChartType.LineChart || this.chartTypeId === ChartType.AreaChart) {
                    if (this.templateTabChartConfigurationDetailsUpsertModel.XAxisMinValue != undefined || this.templateTabChartConfigurationDetailsUpsertModel.XAxisMinValue != null) {
                        if (this.templateTabChartConfigurationDetailsUpsertModel.XAxisMaxValue != undefined || this.templateTabChartConfigurationDetailsUpsertModel.XAxisMaxValue != null) {
                            if (this.templateTabChartConfigurationDetailsUpsertModel.XAxisInterval != undefined || this.templateTabChartConfigurationDetailsUpsertModel.XAxisInterval != null) {
                                if (this.templateTabChartConfigurationDetailsUpsertModel.XAxisChannelId != undefined) {
                                    if (this.templateTabChartConfigurationDetailsUpsertModel.YAxisMinValue != undefined || this.templateTabChartConfigurationDetailsUpsertModel.YAxisMinValue != null) {
                                        if (this.templateTabChartConfigurationDetailsUpsertModel.YAxisMaxValue != undefined || this.templateTabChartConfigurationDetailsUpsertModel.YAxisMaxValue != null) {
                                            if (this.templateTabChartConfigurationDetailsUpsertModel.YAxisInterval != undefined || this.templateTabChartConfigurationDetailsUpsertModel.YAxisInterval != null) {
                                                this.templateTabChartConfigurationDetailsService.putData(this.templateTabChartConfigurationDetailsUpsertModel).subscribe(response => {
                                                    if (response.isSuccess) {
                                                        this.success('Tab Chart Configuration Details Successfully Saved');
                                                        if (saveOrPreview === "preview") {
                                                            this.showPreviewTabChartConfigurationDialog = true;
                                                        }
                                                    }
                                                    else if (response.Message === "Configuration Name Alsready Exists") {
                                                        this.error("Configuration Name Alsready Exists");
                                                    }
                                                    else {
                                                        this.error("Tab Chart Configuration Details Failed To Save");
                                                    }
                                                });
                                            }
                                            else {
                                                this.error("Enter y-axis interval");
                                            }
                                        }
                                        else {
                                            this.error("Enter y-axis max value");
                                        }
                                    }
                                    else {
                                        this.error("Enter y-axis min value");
                                    }
                                }
                                else {
                                    this.error("Select x-axis channel");
                                }
                            }
                            else {
                                this.error("Enter x-axis interval");
                            }
                        }
                        else {
                            this.error("Enter x-axis max value");
                        }
                    }
                    else {
                        this.error("Enter x-axis min value");
                    }
                }
                else {
                    this.templateTabChartConfigurationDetailsService.putData(this.templateTabChartConfigurationDetailsUpsertModel).subscribe(response => {
                        if (response.isSuccess) {
                            this.success('Tab Chart Configuration Details Successfully Saved');
                            if (saveOrPreview === "preview") {
                                this.showPreviewTabChartConfigurationDialog = true;
                            }
                        }
                        else {
                            this.error("Tab Chart Configuration Details Failed To Save");
                        }
                    });
                }
            }

            else {
                this.error("Enter y-axis units");
            }
        }
        else {
            this.error("Enter x-axis units");
        }
    }

    onOrientationChange(orientaion: any) {
        if (this.isEditChartConfiguration) {
            this.templateTabChartConfigurationService.putData(this.templateTabChartConfigurationModel).subscribe(response => {
                if (response.isSuccess) {
                    this.templateTabChartConfigId = response.id;
                    this.onUpdateTemplateTabConfiguration(this.templateTabConfigId);
                    this.success('Tab Chart Orientation Successfully Updated');
                }
                else {
                    this.error("Failed To Update");
                }
            });
        }
    }
    onAtdPositionChange(selectedData, changeStatus) {
        if (changeStatus == 'save') {
            if (this.stackBarChartChannelModel.ATDPositionIds.length > 2) {
                this.stackBarChartChannelModel.ATDPositionIds.length = 2;
                selectedData = [];
                this.error("Maximum two selection required.");
            }
            else {
                this.uniqueIdsViewModel.push({ Id: parseInt(selectedData[selectedData.length - 1]) });
            }
        }
        if (changeStatus == 'update') {
            if (this.stackBarChartChannelModel.ATDPositionIds.length > 2) {
                this.stackBarChartChannelModel.ATDPositionIds.length = 2;
                this.error("Maximum two selection required.");
            }
            else {
                this.uniqueIdsViewModel.push({ Id: parseInt(selectedData[selectedData.length - 1]) });
            }
        }
    }
}
