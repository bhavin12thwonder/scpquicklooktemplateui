import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route } from '@angular/compiler/src/core';
import { Routes, RouterModule } from '@angular/router';
import { TabViewModule } from 'primeng/tabview';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { RadioButtonModule } from 'primeng/radiobutton';
import { Component, ViewChild, ViewContainerRef, Input, EventEmitter, Output } from '@angular/core';
import { TabView, TabPanel } from 'primeng/primeng';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { ToastModule } from 'primeng/toast';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { TabChartConfigurationModalComponent } from './tabChartConfiguration-modal.component';
import { PreviewTabChartConfigurationModalModule } from '../tabChartConfiguration/preview/preview-tabChartConfiguration-modal.module';
import { ColorPickerModule } from '@iplab/ngx-color-picker';
import { ChromePickerModule } from '../../chrome-picker/chrome-picker.module';

@NgModule({
    declarations: [TabChartConfigurationModalComponent],
    imports: [
        DialogModule,
        DropdownModule,
        MultiSelectModule,
        FormsModule,
        ButtonModule,
        TableModule,
        TabViewModule,
        CommonModule,
        ToastModule,
        MessagesModule,
        MessageModule,
        ConfirmDialogModule,
        RadioButtonModule,
        PreviewTabChartConfigurationModalModule,
        ColorPickerModule,
        ChromePickerModule
    ],
    exports: [TabChartConfigurationModalComponent]
})
export class TabChartConfigurationModalModule { }
