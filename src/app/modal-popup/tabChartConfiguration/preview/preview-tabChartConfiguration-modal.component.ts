import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { ATDPosition } from '../../../enums/atdPosition.enum';
import { ChartType } from 'src/app/enums/chartType.enum';
import { ITemplateTabChartConfigurationPreviewViewModel, ChartAnnotationConfigurationPreviewViewModel, ChartChannelConfigurationPreviewViewModel } from '../../../models/templateTabChartConfigurationDetails.model';
import { TemplateTabChartConfigurationDetailsService } from '../../../../services/templateTabChartConfigurationDetails.service';
import { NgxSpinnerService } from "ngx-spinner";

declare var Plotly: any;

@Component({
    selector: 'preview-tabChartConfiguration-modal',
    templateUrl: './preview-tabChartConfiguration-modal.component.html',
    styleUrls: ['./preview-tabChartConfiguration-modal.component.scss'],
    styles: [`
    :host ::ng-deep .ui-dialog {
                    padding: 0;
                    pointer-events: auto;
                    display: flex;
                    flex-direction: column;
                    position: relative;
                }
    `]
})

export class PreviewTabChartConfigurationModalComponent implements OnInit {
    @Input() templateTabChartConfigId: number;
    @Input() testId: number;
    @Input() submatrixName: string;
    @Input() isVisiblePreviewModal: boolean = false;
    @Output() isVisibleModalChange = new EventEmitter();

    constructor(
        private templateTabChartConfigurationDetailsService: TemplateTabChartConfigurationDetailsService,
        private SpinnerService: NgxSpinnerService
    ) { }

    ngOnInit() {
    }

    showModal() {
        //this.testData();
        this.loadTabCharConfiguration(this.testId, this.templateTabChartConfigId, this.submatrixName);
    }

    cancelModal() {
        this.isVisibleModalChange.emit(this.isVisiblePreviewModal);
    }

    testData() {
        var trace1 = {
            type: 'scatter',
            x: [1, 2, 3, 4],
            y: [10, 15, 13, 17],
            mode: 'lines',
            name: 'LegendName',
            line: {
                color: '#cdda16',
                width: 15
            },
            showlegend: true
        };

        var trace2 = {
            type: 'scatter',
            x: [1, 2, 3, 4],
            y: [12, 9, 15, 12],
            mode: 'lines',
            name: 'Blue',
            line: {
                color: '#7dd411',
                width: 1
            }
        };

        var layout = {
            title: 'Line Dash',
            width: 500,
            height: 500
        };

        var data = [trace1, trace2];

        Plotly.newPlot('plotChartConfiguration', data, layout);
    }

    loadTabCharConfiguration(testId, tabChartConfigId, submatrixName) {
        let tabChartConfigDataPreview = [];
        let templateTabChartConfigurationPreviewViewModel: ITemplateTabChartConfigurationPreviewViewModel = <ITemplateTabChartConfigurationPreviewViewModel>{};
        let chartChannelConfigurationPreviewViewModelList: Array<ChartChannelConfigurationPreviewViewModel> = [];
        let chartAnnotationConfigurationPreviewViewModelList: Array<ChartAnnotationConfigurationPreviewViewModel> = [];
        let annotationsData = [];
        let layout: any;
        let trace: any;
        let isAreaChart: boolean;
        let isLineChart: boolean;
        let isBarChart: boolean;
        let isStackBarChart: boolean;
        let barChartThickness: Array<number> = [];
        let barChartXAxis: Array<string> = [];
        let barChartYAxis: Array<number> = [];
        let barChartWidth: number = 0.5;
        let barChartColors: Array<string> = [];
        let barChartLegendNames: Array<string> = [];
        let barChartShowLegends: Array<boolean> = [];
        let barChartDriverXAxis: Array<string> = [];
        let barChartDriverYAxis: Array<number> = [];
        let barChartPassengerXAxis: Array<string> = [];
        let barChartPassengerYAxis: Array<number> = [];

        let goodXAxis: Array<number> = [];
        let acceptableXAxis: Array<number> = [];
        let marginalXAxis: Array<number> = [];
        let poorXAxis: Array<number> = [];
        let driverXAxis: Array<number> = [];
        let passengerXAxis: Array<number> = [];

        let goodYAxis: Array<string> = [];
        let acceptableYAxis: Array<string> = [];
        let marginalYAxis: Array<string> = [];
        let poorYAxis: Array<string> = [];
        let driverYAxis: Array<string> = [];
        let passengerYAxis: Array<string> = [];
        this.SpinnerService.show();
        this.templateTabChartConfigurationDetailsService.previewByTemplateTabChartConfigId(tabChartConfigId, testId, submatrixName).subscribe(response => {
            if (response !== null) {
                isLineChart = false;
                isBarChart = false;
                isAreaChart = false;
                isStackBarChart = false;

                templateTabChartConfigurationPreviewViewModel = response;
                if (templateTabChartConfigurationPreviewViewModel.chartTypeId === ChartType.LineChart) {
                    isLineChart = true;
                }
                else if (templateTabChartConfigurationPreviewViewModel.chartTypeId === ChartType.BarChart) {
                    isBarChart = true;
                }
                else if (templateTabChartConfigurationPreviewViewModel.chartTypeId === ChartType.AreaChart) {
                    isAreaChart = true;
                }
                else if (templateTabChartConfigurationPreviewViewModel.chartTypeId === ChartType.StackBarChart) {
                    isStackBarChart = true;
                }

                chartChannelConfigurationPreviewViewModelList = templateTabChartConfigurationPreviewViewModel.chartChannelConfiguration;
                for (let i = 0; i < chartChannelConfigurationPreviewViewModelList.length; i++) {
                    let chartChannelConfigurationPreviewViewModel: ChartChannelConfigurationPreviewViewModel = <ChartChannelConfigurationPreviewViewModel>{};
                    let yAxis: Array<number> = [];
                    chartChannelConfigurationPreviewViewModel = chartChannelConfigurationPreviewViewModelList[i];
                    if (isLineChart === true) {
                        trace = {
                            mode: 'lines',
                            //type: 'scatter',
                            type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                            x: chartChannelConfigurationPreviewViewModel.channelXAxis, //templateTabChartConfigurationPreviewViewModel.xAxis,
                            y: chartChannelConfigurationPreviewViewModel.channelYAxis,//templateTabChartConfigurationPreviewViewModel.yAxis,
                            name: chartChannelConfigurationPreviewViewModel.legendName,
                            showlegend: chartChannelConfigurationPreviewViewModel.isLegendVisible,
                            line: {
                                color: chartChannelConfigurationPreviewViewModel.chartLineColour,
                                dash: chartChannelConfigurationPreviewViewModel.lineStyleValue,
                                width: chartChannelConfigurationPreviewViewModel.lineThickness
                            }
                        };
                        tabChartConfigDataPreview.push(trace);
                    }
                    else if (isBarChart === true) {
                        barChartThickness.push(barChartWidth);
                        barChartXAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);
                        barChartYAxis.push(chartChannelConfigurationPreviewViewModel.barChartXAxisValue);
                        barChartColors.push(chartChannelConfigurationPreviewViewModel.chartLineColour);
                        barChartLegendNames.push(chartChannelConfigurationPreviewViewModel.legendName);
                        barChartShowLegends.push(chartChannelConfigurationPreviewViewModel.isLegendVisible);
                        if (chartChannelConfigurationPreviewViewModel.atdPosition === ATDPosition.Driver) {
                            barChartDriverXAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);
                            barChartDriverYAxis.push(chartChannelConfigurationPreviewViewModel.atdPositionAxisValue);
                        }
                        else if (chartChannelConfigurationPreviewViewModel.atdPosition === ATDPosition.Passenger) {
                            barChartPassengerXAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);
                            barChartPassengerYAxis.push(chartChannelConfigurationPreviewViewModel.atdPositionAxisValue);
                        }

                        //#region TestData

                        // trace = {
                        //     type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                        //     x: chartChannelConfigurationPreviewViewModel.chartChannelName,
                        //     y: chartChannelConfigurationPreviewViewModel.barChartXAxisValue,
                        //     name: chartChannelConfigurationPreviewViewModel.legendName,
                        //     showlegend: chartChannelConfigurationPreviewViewModel.isLegendVisible,
                        //     width: barChartThickness,
                        //     orientation: templateTabChartConfigurationPreviewViewModel.chartOrientation,
                        //     marker: { 'color': chartChannelConfigurationPreviewViewModel.chartLineColour },
                        //     offset: 0
                        // };

                        // if (chartChannelConfigurationPreviewViewModel.atdPosition === ATDPosition.Passenger) {
                        //     trace = {
                        //         type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                        //         x: chartChannelConfigurationPreviewViewModel.chartChannelName,
                        //         y: chartChannelConfigurationPreviewViewModel.atdPositionAxisValue,
                        //         name: 'Passenger',
                        //         showlegend: chartChannelConfigurationPreviewViewModel.isLegendVisible,
                        //         width: barChartThickness,
                        //         orientation: templateTabChartConfigurationPreviewViewModel.chartOrientation,
                        //         marker: { 'color': 'pink' },
                        //         offset: 0
                        //     };
                        // }
                        // else if (chartChannelConfigurationPreviewViewModel.atdPosition === ATDPosition.Driver) {
                        //     trace = {
                        //         type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                        //         x: chartChannelConfigurationPreviewViewModel.chartChannelName,
                        //         y: chartChannelConfigurationPreviewViewModel.atdPositionAxisValue,
                        //         name: 'Driver',
                        //         showlegend: chartChannelConfigurationPreviewViewModel.isLegendVisible,
                        //         width: barChartThickness,
                        //         orientation: templateTabChartConfigurationPreviewViewModel.chartOrientation,
                        //         marker: { 'color': 'blue' },
                        //         offset: 0
                        //     };
                        // }
                        // tabChartConfigDataPreview.push(trace);

                        //#endregion
                    }
                    else if (isAreaChart === true) {
                        let fillArea = (chartChannelConfigurationPreviewViewModel.fillArea) ? 'tonexty' : '';
                        trace = {
                            type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                            fillcolor: chartChannelConfigurationPreviewViewModel.chartLineColour,
                            fill: fillArea,
                            x: chartChannelConfigurationPreviewViewModel.channelXAxis, //templateTabChartConfigurationPreviewViewModel.xAxis,
                            y: chartChannelConfigurationPreviewViewModel.channelYAxis,//templateTabChartConfigurationPreviewViewModel.yAxis,
                            name: chartChannelConfigurationPreviewViewModel.legendName,
                            showlegend: chartChannelConfigurationPreviewViewModel.isLegendVisible
                        };
                        tabChartConfigDataPreview.push(trace);
                    }
                    else if (isStackBarChart === true) {
                        goodXAxis.push(chartChannelConfigurationPreviewViewModel.goodValue);
                        goodYAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);

                        acceptableXAxis.push(chartChannelConfigurationPreviewViewModel.acceptableValue);
                        acceptableYAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);

                        marginalXAxis.push(chartChannelConfigurationPreviewViewModel.marginalValue);
                        marginalYAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);

                        poorXAxis.push(chartChannelConfigurationPreviewViewModel.poorValue);
                        poorYAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);

                        for (let i = 0; i < chartChannelConfigurationPreviewViewModel.chartChannelATDPositions.length; i++) {
                            if (chartChannelConfigurationPreviewViewModel.chartChannelATDPositions[i].atdPositionId === ATDPosition.Driver) {
                                driverXAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelATDPositions[i].atdPositionAxisValue);
                                driverYAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);
                            }
                            else if (chartChannelConfigurationPreviewViewModel.chartChannelATDPositions[i].atdPositionId === ATDPosition.Passenger) {
                                passengerXAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelATDPositions[i].atdPositionAxisValue);
                                passengerYAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);
                            }
                        }
                    }
                }

                //Chart Annotation
                chartAnnotationConfigurationPreviewViewModelList = templateTabChartConfigurationPreviewViewModel.chartAnnotationConfiguration;
                for (let i = 0; i < chartAnnotationConfigurationPreviewViewModelList.length; i++) {
                    let annotationData = {
                        xref: 'paper',
                        yref: 'paper',
                        x: chartAnnotationConfigurationPreviewViewModelList[i].annotationLineStartXCoOrdinate,
                        y: chartAnnotationConfigurationPreviewViewModelList[i].annotationLineStartYCoOrdinate,
                        x1: chartAnnotationConfigurationPreviewViewModelList[i].annotationLineEndXCoOrdinate,
                        y1: chartAnnotationConfigurationPreviewViewModelList[i].annotationLineEndYCoOrdinate,
                        text: chartAnnotationConfigurationPreviewViewModelList[i].annotationText,
                        font: {
                            family: 'Arial',
                            size: 30,
                            color: chartAnnotationConfigurationPreviewViewModelList[i].annotationLineColour
                        },
                        showarrow: false
                    };
                    annotationsData.push(annotationData);
                }

                if (isLineChart === true) {
                    debugger;
                    layout = {
                        title: templateTabChartConfigurationPreviewViewModel.templateTabChartConfigurationName,
                        xaxis: {
                            range: [templateTabChartConfigurationPreviewViewModel.xAxisMinValue, templateTabChartConfigurationPreviewViewModel.xAxisMaxValue],
                            //autorange: false
                            tickmode: 'linear',
                            tick0: templateTabChartConfigurationPreviewViewModel.xAxisMinValue,
                            dtick: templateTabChartConfigurationPreviewViewModel.xAxisInterval,
                            ticktext: templateTabChartConfigurationPreviewViewModel.xAxisUnits,
                            title: {
                                text: templateTabChartConfigurationPreviewViewModel.xAxisLabel + '[' +
                                    templateTabChartConfigurationPreviewViewModel.xAxisUnits + ']'
                            }
                        },
                        yaxis: {
                            range: [templateTabChartConfigurationPreviewViewModel.yAxisMinValue, templateTabChartConfigurationPreviewViewModel.yAxisMaxValue],
                            //autorange: false
                            tickmode: 'linear',
                            tick0: templateTabChartConfigurationPreviewViewModel.yAxisMinValue,
                            dtick: templateTabChartConfigurationPreviewViewModel.yAxisInterval,
                            ticktext: templateTabChartConfigurationPreviewViewModel.yAxisUnits,
                            title: {
                                text: templateTabChartConfigurationPreviewViewModel.yAxisLabel + '[' +
                                    templateTabChartConfigurationPreviewViewModel.yAxisUnits + ']'
                            }
                        },
                        // shapes: [
                        //     //line vertical
                        //     {
                        //         type: 'line',
                        //         x0: 1,
                        //         y0: 46,
                        //         x1: 1,
                        //         y1: 21,
                        //         line: {
                        //             color: 'rgb(55, 128, 191)',
                        //             width: 3
                        //         }
                        //     },
                        //     {
                        //         type: 'line',
                        //         x0: 3,
                        //         y0: 46,
                        //         x1: 3,
                        //         y1: 23,
                        //         line: {
                        //             color: 'rgb(55, 128, 191)',
                        //             width: 3
                        //         }
                        //     }
                        // ],
                    };
                }
                else if (isAreaChart === true) {
                    layout = {
                        title: templateTabChartConfigurationPreviewViewModel.templateTabChartConfigurationName,
                        xaxis: {
                            range: [templateTabChartConfigurationPreviewViewModel.xAxisMinValue, templateTabChartConfigurationPreviewViewModel.xAxisMaxValue],
                            //autorange: false
                            tickmode: 'linear',
                            tick0: templateTabChartConfigurationPreviewViewModel.xAxisMinValue,
                            dtick: templateTabChartConfigurationPreviewViewModel.xAxisInterval,
                            ticktext: templateTabChartConfigurationPreviewViewModel.xAxisUnits,
                            title: {
                                text: templateTabChartConfigurationPreviewViewModel.xAxisLabel + '[' +
                                    templateTabChartConfigurationPreviewViewModel.xAxisUnits + ']'
                            }
                        },
                        yaxis: {
                            range: [templateTabChartConfigurationPreviewViewModel.yAxisMinValue, templateTabChartConfigurationPreviewViewModel.yAxisMaxValue],
                            //autorange: false
                            tickmode: 'linear',
                            tick0: templateTabChartConfigurationPreviewViewModel.yAxisMinValue,
                            dtick: templateTabChartConfigurationPreviewViewModel.yAxisInterval,
                            ticktext: templateTabChartConfigurationPreviewViewModel.yAxisUnits,
                            title: {
                                text: templateTabChartConfigurationPreviewViewModel.yAxisLabel + '[' +
                                    templateTabChartConfigurationPreviewViewModel.yAxisUnits + ']'
                            }
                        }
                    };
                }
                else if (isBarChart === true) {
                    trace = {
                        type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                        x: barChartXAxis,
                        y: barChartYAxis,
                        name: barChartLegendNames,
                        showlegend: barChartShowLegends,
                        width: barChartThickness,
                        orientation: templateTabChartConfigurationPreviewViewModel.chartOrientation,
                        marker: { 'color': barChartColors },
                        offset: 0
                    };

                    let barChartPassengerTrace = {
                        type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                        x: barChartPassengerXAxis,
                        y: barChartPassengerYAxis,
                        name: 'Passenger',
                        showlegend: barChartShowLegends,
                        width: barChartThickness,
                        orientation: templateTabChartConfigurationPreviewViewModel.chartOrientation,
                        marker: { 'color': 'pink' },
                        offset: 0
                    };

                    let barChartDriverTrace = {
                        type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                        x: barChartDriverXAxis,
                        y: barChartDriverYAxis,
                        name: 'Driver',
                        showlegend: barChartShowLegends,
                        width: barChartThickness,
                        orientation: templateTabChartConfigurationPreviewViewModel.chartOrientation,
                        marker: { 'color': 'blue' },
                        offset: 0
                    };

                    tabChartConfigDataPreview.push(trace);
                    tabChartConfigDataPreview.push(barChartPassengerTrace);
                    tabChartConfigDataPreview.push(barChartDriverTrace);
                    layout = {
                        title: templateTabChartConfigurationPreviewViewModel.templateTabChartConfigurationName,
                        showlegend: true,
                        barmode: 'group',
                        bargap: 0.5,
                    };
                }
                if (isStackBarChart === true) {

                    let acceptableTrace = {
                        y: acceptableYAxis,
                        x: acceptableXAxis,
                        name: 'Acceptable',
                        type: 'bar',
                        orientation: 'h',
                        marker: {
                            color: 'yellow'
                        }
                    };
                    let goodTrace = {
                        y: goodYAxis,
                        x: goodXAxis,
                        name: 'Good',
                        type: 'bar',
                        orientation: 'h',
                        marker: {
                            color: 'green'
                        }
                    };

                    let poorTrace = {
                        y: poorYAxis,
                        x: poorXAxis,
                        name: 'Poor',
                        type: 'bar',
                        orientation: 'h',
                        marker: {
                            color: 'red'
                        }
                    };
                    let marginalTrace = {
                        y: marginalYAxis,
                        x: marginalXAxis,
                        name: 'Marginal',
                        type: 'bar',
                        orientation: 'h',
                        marker: {
                            color: 'orange'
                        }
                    };

                    let driverTrace = {
                        y: driverYAxis,
                        x: driverXAxis,
                        name: 'Driver',
                        type: 'bar',
                        orientation: 'h',
                        width: [0.2, 0.2, 0.2],
                        offset: 0,
                        marker: {
                            color: 'blue'
                        }
                    };

                    let passengerTrace = {
                        y: passengerYAxis,
                        x: passengerXAxis,
                        name: 'Passenger',
                        type: 'bar',
                        orientation: 'h',
                        width: [0.2, 0.2, 0.2],
                        offset: -0.2,
                        marker: {
                            color: 'pink'
                        }
                    };

                    let data = [poorTrace, marginalTrace, acceptableTrace, goodTrace, passengerTrace, driverTrace];
                    Plotly.newPlot('plotChartConfiguration', data, { barmode: 'overlay' }, { displaylogo: false });
                }
                else {
                    Plotly.newPlot('plotChartConfiguration', tabChartConfigDataPreview, layout, { displaylogo: false });
                }
            }
            this.SpinnerService.hide();
        });
    }
}
