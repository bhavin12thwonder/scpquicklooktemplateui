import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route } from '@angular/compiler/src/core';
import { Routes, RouterModule } from '@angular/router';
import { TabViewModule } from 'primeng/tabview';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { RadioButtonModule } from 'primeng/radiobutton';
import { Component, ViewChild, ViewContainerRef, Input, EventEmitter, Output } from '@angular/core';
import { TabView, TabPanel } from 'primeng/primeng';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { ToastModule } from 'primeng/toast';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { PreviewTemplateComponent } from '../preview/preview-template-modal.component';
import { PlotModule } from '../../../plot/plot.module';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
    declarations: [PreviewTemplateComponent],
    imports: [
        DialogModule,
        DropdownModule,
        FormsModule,
        ButtonModule,
        TableModule,
        TabViewModule,
        CommonModule,
        ToastModule,
        MessagesModule,
        MessageModule,
        ConfirmDialogModule,
        RadioButtonModule,
        PlotModule,
        NgxSpinnerModule
    ],
    exports: [PreviewTemplateComponent]
})
export class PreviewTemplateModule { }
