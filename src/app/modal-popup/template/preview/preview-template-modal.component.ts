import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { MessageService, ConfirmationService } from 'primeng/api';
import { ATDPosition } from 'src/app/enums/atdPosition.enum';
import { TemplateService } from '../../../../services/template.service';
import { ITemplateTabPreviewModel, ITemplateTabConfigurationPreviewRowModel, ITemplateTabConfigurationPreviewColumnsModel } from 'src/app/models/templateTabConfiguration.model';
import { ITemplateTabTableConfigurationDetailsPreviewRowsDbModel, ITemplateTabTableConfigurationDetailsPreviewColumnsDbModel } from '../../../models/templateTabTableConfigurationDetails.model';
import { ITemplateTabChartConfigurationPreviewViewModel, ChartAnnotationConfigurationPreviewViewModel, ChartChannelConfigurationPreviewViewModel } from '../../../models/templateTabChartConfigurationDetails.model';
import { ChartType } from 'src/app/enums/chartType.enum';
import { NgxSpinnerService } from "ngx-spinner";

declare var Plotly: any;
var _self: any;

@Component({
    selector: 'preview-template-modal',
    templateUrl: './preview-template-modal.component.html',
    styleUrls: ['./preview-template-modal.component.scss'],
    styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }
    `]
})
export class PreviewTemplateComponent implements OnInit {
    @Input() templateId: number;
    @Input() testId: number;
    @Input() submatrixName: string;
    @Input() templateTabId: number;
    @Input() isVisibleModal: boolean = false;
    @Input() isViewInitialized: boolean = false;
    @Output() isVisibleModalChange = new EventEmitter();

    tabConfigRowData: ITemplateTabConfigurationPreviewRowModel = <ITemplateTabConfigurationPreviewRowModel>{};
    tabConfigurationRowsDataList: Array<ITemplateTabConfigurationPreviewRowModel> = [];
    tabConfigColumnData: ITemplateTabConfigurationPreviewColumnsModel = <ITemplateTabConfigurationPreviewColumnsModel>{};
    tabConfigColumnsDataList: Array<ITemplateTabConfigurationPreviewColumnsModel> = [];
    templateTabs: any;

    tabTableConfigDataPreview: Array<ITemplateTabTableConfigurationDetailsPreviewRowsDbModel>;
    templateTabTableConfigurationDetailsPreviewRowsDbModel: ITemplateTabTableConfigurationDetailsPreviewRowsDbModel = <ITemplateTabTableConfigurationDetailsPreviewRowsDbModel>{};
    templateTabTableConfigurationDetailsPreviewColumnsDbModel: ITemplateTabTableConfigurationDetailsPreviewColumnsDbModel = <ITemplateTabTableConfigurationDetailsPreviewColumnsDbModel>{};
    templateTabTableConfigurationDetailsPreviewRowsDbModelList: Array<ITemplateTabTableConfigurationDetailsPreviewRowsDbModel> = [];
    templateTabTableConfigurationDetailsPreviewColumnsDbModelList: Array<ITemplateTabTableConfigurationDetailsPreviewColumnsDbModel> = [];

    constructor(
        private templateService: TemplateService,
        private notifyService: MessageService,
        private confirmationService: ConfirmationService,
        private SpinnerService: NgxSpinnerService
    ) {
    }

    ngOnInit(): void {
    }

    showModal() {
        //this.loadTemplatTabConfiguration(this.templateTabId);
        this.loadTemplatPreviewData(this.templateId);
    }

    cancelModal() {
        this.tabTableConfigDataPreview = [];
        this.isVisibleModalChange.emit(this.isVisibleModal);
    }

    loadTemplatPreviewData(templateId) {
        this.templateTabs = [];
        let templateTabPreviewListModel = [];
        this.SpinnerService.show();
        this.templateService.previewByTemplateId(templateId, this.testId, this.submatrixName).subscribe(response => {
            templateTabPreviewListModel = response;
            //this.onAddNewPlotlyDiv(this.tabConfigurationRowsDataList);
            if (templateTabPreviewListModel.length > 0) {
                for (let i = 0; i < templateTabPreviewListModel.length; i++) {
                    if (templateTabPreviewListModel[i].templateTabConfigurationPreviewModel.length > 0) {
                        let tabConfigurationRowsDataList = templateTabPreviewListModel[i].templateTabConfigurationPreviewModel;
                        this.templateTabs.push({ id: templateTabPreviewListModel[i].id, name: templateTabPreviewListModel[i].templateTabName, tabIndex: i, templateTabData: this.loadTemplatTabConfigurationPreview(tabConfigurationRowsDataList) });
                    }
                }
            }
            this.SpinnerService.hide();
        });
    }

    loadTemplatTabConfigurationPreview(templateTabConfigData): any[] {
        let tabConfigurationDataList: any = [];
        let tabConfigurationRowsDataList = [];
        tabConfigurationRowsDataList = templateTabConfigData;
        //Load Tabconfiguartion Row Data
        if (tabConfigurationRowsDataList.length > 0) {

            for (let j = 0; j < tabConfigurationRowsDataList.length; j++) {
                let tabConfigRowData = <ITemplateTabConfigurationPreviewRowModel>{};
                tabConfigRowData = tabConfigurationRowsDataList[j];
                tabConfigRowData.rowId = j;
                tabConfigRowData.templateTabId = tabConfigRowData.templateTabId;
                let tabConfigColumnsDataList = [];

                //Load Tabconfiguartion COlumns Data
                for (let k = 0; k < tabConfigurationRowsDataList[j].columns.length; k++) {
                    let columnsData = <ITemplateTabConfigurationPreviewColumnsModel>{};
                    let columnData = tabConfigurationRowsDataList[j].columns[k];
                    let tabConfigColumnData = <ITemplateTabConfigurationPreviewColumnsModel>{};
                    tabConfigColumnData.id = columnData.id;
                    tabConfigColumnData.columnId = columnData.columnId - 1;
                    tabConfigColumnData.displayFormatTypeId = columnData.displayFormatTypeId;
                    tabConfigColumnData.columnSpan = columnData.columnSpan;
                    tabConfigColumnData.tabTableConfigurationId = columnData.tabTableConfigurationId;
                    tabConfigColumnData.tabTableConfigurationName = columnData.tabTableConfigurationName;
                    tabConfigColumnData.tabChartConfigurationId = columnData.tabChartConfigurationId;
                    tabConfigColumnData.tabChartConfigurationName = columnData.tabChartConfigurationName;
                    if (columnData.displayFormatTypeId === 1) {
                        tabConfigColumnData.tabTableConfiguration = this.loadDbTabTableConfigurationPreview(columnData.templateTabTableConfigurationPreviewViewModel)
                    }
                    else if (columnData.displayFormatTypeId === 2) {
                        tabConfigColumnData.tabChartConfiguration = this.loadTabChartConfiguration(columnData.templateTabChartConfigurationPreviewViewModel, (j.toString() + k.toString()));
                    }

                    tabConfigColumnsDataList.push(tabConfigColumnData);
                }
                tabConfigRowData.columns = tabConfigColumnsDataList;
                tabConfigurationDataList.push(tabConfigRowData);
            }
        }
        return tabConfigurationDataList;
    }

    loadDbTabTableConfigurationPreview(tabTableConfigurationPreviewData: any): any {
        let tabTableConfigData = [];
        let tabTableConfigurationRowsData: Array<ITemplateTabTableConfigurationDetailsPreviewRowsDbModel> = [];
        tabTableConfigurationRowsData = tabTableConfigurationPreviewData;
        if (tabTableConfigurationRowsData.length > 0 && tabTableConfigurationPreviewData !== undefined) {
            for (let k = 0; k < tabTableConfigurationRowsData.length; k++) {
                let tabTableConfigRowData = <ITemplateTabTableConfigurationDetailsPreviewRowsDbModel>{};
                tabTableConfigRowData = tabTableConfigurationRowsData[k];
                tabTableConfigRowData.rowId = k + 1;
                let tabTableConfigColumnsDataList = [];
                let tabTableConfigColumnsDbData = tabTableConfigRowData.columns;

                for (let l = 0; l <= tabTableConfigColumnsDbData.length; l++) {
                    let tabTableConfigColumnData = <ITemplateTabTableConfigurationDetailsPreviewColumnsDbModel>{};
                    if (l < tabTableConfigColumnsDbData.length) {
                        tabTableConfigColumnData.id = tabTableConfigColumnsDbData[l].id;
                        tabTableConfigColumnData.columnId = l;
                        tabTableConfigColumnData.dbColumn = tabTableConfigColumnsDbData[l].dbColumn;
                        tabTableConfigColumnData.columnType = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnType : 0;
                        tabTableConfigColumnData.columnHeader = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnHeader : "";
                        tabTableConfigColumnData.columnData = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnData : "";
                        tabTableConfigColumnData.rowSpan = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].rowSpan : 1;
                        tabTableConfigColumnData.columnSpan = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnSpan : 1;
                        tabTableConfigColumnData.columnWidth = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnWidth : 10;
                        tabTableConfigColumnData.colourCode = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].colourCode : "";
                        tabTableConfigColumnData.isHeader = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].isHeader : false;
                        tabTableConfigColumnData.isDeleted = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].isDeleted : false;
                        tabTableConfigColumnsDataList.push(tabTableConfigColumnData);
                    }
                }
                tabTableConfigRowData.columns = tabTableConfigColumnsDataList;
                tabTableConfigData.push(tabTableConfigRowData);
            }
        }
        return tabTableConfigData;
    }

    loadTabChartConfiguration(tabChartConfigurationPreviewData: any, index: string): any {
        let plotlyChartData: any[] = [];
        let plotlyData = [];
        let plotlyLayout: any;
        let plotlyOptions: any;
        let plotlyChartId: any;

        let tabChartConfigDataPreview = [];
        let templateTabChartConfigurationPreviewViewModel: ITemplateTabChartConfigurationPreviewViewModel = <ITemplateTabChartConfigurationPreviewViewModel>{};
        let chartChannelConfigurationPreviewViewModelList: Array<ChartChannelConfigurationPreviewViewModel> = [];
        let chartAnnotationConfigurationPreviewViewModelList: Array<ChartAnnotationConfigurationPreviewViewModel> = [];
        let annotationsData = [];
        let layout: any;
        let trace: any;
        let isAreaChart: boolean;
        let isLineChart: boolean;
        let isBarChart: boolean;
        let isStackBarChart: boolean;
        let barChartThickness: Array<number> = [];
        let barChartXAxis: Array<string> = [];
        let barChartYAxis: Array<number> = [];
        let barChartWidth: number = 0.5;
        let barChartColors: Array<string> = [];
        let barChartLegendNames: Array<string> = [];
        let barChartShowLegends: Array<boolean> = [];
        let barChartDriverXAxis: Array<string> = [];
        let barChartDriverYAxis: Array<number> = [];
        let barChartPassengerXAxis: Array<string> = [];
        let barChartPassengerYAxis: Array<number> = [];

        let goodXAxis: Array<number> = [];
        let acceptableXAxis: Array<number> = [];
        let marginalXAxis: Array<number> = [];
        let poorXAxis: Array<number> = [];
        let driverXAxis: Array<number> = [];
        let passengerXAxis: Array<number> = [];

        let goodYAxis: Array<string> = [];
        let acceptableYAxis: Array<string> = [];
        let marginalYAxis: Array<string> = [];
        let poorYAxis: Array<string> = [];
        let driverYAxis: Array<string> = [];
        let passengerYAxis: Array<string> = [];

        if (tabChartConfigurationPreviewData !== null) {
            isLineChart = false;
            isBarChart = false;
            isAreaChart = false;
            isStackBarChart = false;

            templateTabChartConfigurationPreviewViewModel = tabChartConfigurationPreviewData;
            if (templateTabChartConfigurationPreviewViewModel.chartTypeId === ChartType.LineChart) {
                isLineChart = true;
            }
            else if (templateTabChartConfigurationPreviewViewModel.chartTypeId === ChartType.BarChart) {
                isBarChart = true;
            }
            else if (templateTabChartConfigurationPreviewViewModel.chartTypeId === ChartType.AreaChart) {
                isAreaChart = true;
            }
            else if (templateTabChartConfigurationPreviewViewModel.chartTypeId === ChartType.StackBarChart) {
                isStackBarChart = true;
            }

            chartChannelConfigurationPreviewViewModelList = templateTabChartConfigurationPreviewViewModel.chartChannelConfiguration;
            for (let i = 0; i < chartChannelConfigurationPreviewViewModelList.length; i++) {
                let chartChannelConfigurationPreviewViewModel: ChartChannelConfigurationPreviewViewModel = <ChartChannelConfigurationPreviewViewModel>{};
                let yAxis: Array<number> = [];
                chartChannelConfigurationPreviewViewModel = chartChannelConfigurationPreviewViewModelList[i];
                if (isLineChart === true) {
                    trace = {
                        mode: 'lines',
                        //type: 'scatter',
                        type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                        x: chartChannelConfigurationPreviewViewModel.channelXAxis, //templateTabChartConfigurationPreviewViewModel.xAxis,
                        y: chartChannelConfigurationPreviewViewModel.channelYAxis,//templateTabChartConfigurationPreviewViewModel.yAxis,
                        name: chartChannelConfigurationPreviewViewModel.legendName,
                        showlegend: chartChannelConfigurationPreviewViewModel.isLegendVisible,
                        line: {
                            color: chartChannelConfigurationPreviewViewModel.chartLineColour,
                            dash: chartChannelConfigurationPreviewViewModel.lineStyleValue,
                            width: chartChannelConfigurationPreviewViewModel.lineThickness
                        }
                    };
                    plotlyData.push(trace);
                }
                else if (isBarChart === true) {
                    barChartThickness.push(barChartWidth);
                    barChartXAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);
                    barChartYAxis.push(chartChannelConfigurationPreviewViewModel.barChartXAxisValue);
                    barChartColors.push(chartChannelConfigurationPreviewViewModel.chartLineColour);
                    barChartLegendNames.push(chartChannelConfigurationPreviewViewModel.legendName);
                    barChartShowLegends.push(chartChannelConfigurationPreviewViewModel.isLegendVisible);
                    if (chartChannelConfigurationPreviewViewModel.atdPosition === ATDPosition.Driver) {
                        barChartDriverXAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);
                        barChartDriverYAxis.push(chartChannelConfigurationPreviewViewModel.atdPositionAxisValue);
                    }
                    else if (chartChannelConfigurationPreviewViewModel.atdPosition === ATDPosition.Passenger) {
                        barChartPassengerXAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);
                        barChartPassengerYAxis.push(chartChannelConfigurationPreviewViewModel.atdPositionAxisValue);
                    }
                }
                else if (isAreaChart === true) {
                    let fillArea = (chartChannelConfigurationPreviewViewModel.fillArea) ? 'tonexty' : '';
                    trace = {
                        type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                        fillcolor: chartChannelConfigurationPreviewViewModel.chartLineColour,
                        fill: fillArea,
                        x: chartChannelConfigurationPreviewViewModel.channelXAxis, //templateTabChartConfigurationPreviewViewModel.xAxis,
                        y: chartChannelConfigurationPreviewViewModel.channelYAxis,//templateTabChartConfigurationPreviewViewModel.yAxis,
                        name: chartChannelConfigurationPreviewViewModel.legendName,
                        showlegend: chartChannelConfigurationPreviewViewModel.isLegendVisible
                    };
                    plotlyData.push(trace);
                }
                else if (isStackBarChart === true) {
                    goodXAxis.push(chartChannelConfigurationPreviewViewModel.goodValue);
                    goodYAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);

                    acceptableXAxis.push(chartChannelConfigurationPreviewViewModel.acceptableValue);
                    acceptableYAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);

                    marginalXAxis.push(chartChannelConfigurationPreviewViewModel.marginalValue);
                    marginalYAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);

                    poorXAxis.push(chartChannelConfigurationPreviewViewModel.poorValue);
                    poorYAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);

                    for (let i = 0; i < chartChannelConfigurationPreviewViewModel.chartChannelATDPositions.length; i++) {
                        if (chartChannelConfigurationPreviewViewModel.chartChannelATDPositions[i].atdPositionId === ATDPosition.Driver) {
                            driverXAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelATDPositions[i].atdPositionAxisValue);
                            driverYAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);
                        }
                        else if (chartChannelConfigurationPreviewViewModel.chartChannelATDPositions[i].atdPositionId === ATDPosition.Passenger) {
                            passengerXAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelATDPositions[i].atdPositionAxisValue);
                            passengerYAxis.push(chartChannelConfigurationPreviewViewModel.chartChannelName);
                        }
                    }
                }
            }

            //Chart Annotation
            chartAnnotationConfigurationPreviewViewModelList = templateTabChartConfigurationPreviewViewModel.chartAnnotationConfiguration;
            for (let i = 0; i < chartAnnotationConfigurationPreviewViewModelList.length; i++) {
                let annotationData = {
                    xref: 'paper',
                    yref: 'paper',
                    x: chartAnnotationConfigurationPreviewViewModelList[i].annotationLineStartXCoOrdinate,
                    y: chartAnnotationConfigurationPreviewViewModelList[i].annotationLineStartYCoOrdinate,
                    x1: chartAnnotationConfigurationPreviewViewModelList[i].annotationLineEndXCoOrdinate,
                    y1: chartAnnotationConfigurationPreviewViewModelList[i].annotationLineEndYCoOrdinate,
                    text: chartAnnotationConfigurationPreviewViewModelList[i].annotationText,
                    font: {
                        family: 'Arial',
                        size: 30,
                        color: chartAnnotationConfigurationPreviewViewModelList[i].annotationLineColour
                    },
                    showarrow: false
                };
                annotationsData.push(annotationData);
            }

            if (isLineChart === true) {
                layout = {
                    title: templateTabChartConfigurationPreviewViewModel.templateTabChartConfigurationName,
                    xaxis: {
                        range: [templateTabChartConfigurationPreviewViewModel.xAxisMinValue, templateTabChartConfigurationPreviewViewModel.xAxisMaxValue],
                        //autorange: false
                        tickmode: 'linear',
                        tick0: templateTabChartConfigurationPreviewViewModel.xAxisMinValue,
                        dtick: templateTabChartConfigurationPreviewViewModel.xAxisInterval,
                        ticktext: templateTabChartConfigurationPreviewViewModel.xAxisUnits,
                        title: {
                            text: templateTabChartConfigurationPreviewViewModel.xAxisLabel + '[' +
                                templateTabChartConfigurationPreviewViewModel.xAxisUnits + ']'
                        }
                    },
                    yaxis: {
                        range: [templateTabChartConfigurationPreviewViewModel.yAxisMinValue, templateTabChartConfigurationPreviewViewModel.yAxisMaxValue],
                        //autorange: false
                        tickmode: 'linear',
                        tick0: templateTabChartConfigurationPreviewViewModel.yAxisMinValue,
                        dtick: templateTabChartConfigurationPreviewViewModel.yAxisInterval,
                        ticktext: templateTabChartConfigurationPreviewViewModel.yAxisUnits,
                        title: {
                            text: templateTabChartConfigurationPreviewViewModel.yAxisLabel + '[' +
                                templateTabChartConfigurationPreviewViewModel.yAxisUnits + ']'
                        }
                    },
                    // shapes: [
                    //     //line vertical
                    //     {
                    //         type: 'line',
                    //         x0: 1,
                    //         y0: 46,
                    //         x1: 1,
                    //         y1: 21,
                    //         line: {
                    //             color: 'rgb(55, 128, 191)',
                    //             width: 3
                    //         }
                    //     },
                    //     {
                    //         type: 'line',
                    //         x0: 3,
                    //         y0: 46,
                    //         x1: 3,
                    //         y1: 23,
                    //         line: {
                    //             color: 'rgb(55, 128, 191)',
                    //             width: 3
                    //         }
                    //     }
                    // ],
                    legend: {
                        y: 0.5,
                        traceorder: 'reversed',
                        font: {
                            size: 16
                        }
                    },
                    width: 500,
                    height: 500
                };
            }
            else if (isAreaChart === true) {
                layout = {
                    title: templateTabChartConfigurationPreviewViewModel.templateTabChartConfigurationName,
                    xaxis: {
                        range: [templateTabChartConfigurationPreviewViewModel.xAxisMinValue, templateTabChartConfigurationPreviewViewModel.xAxisMaxValue],
                        //autorange: false
                        tickmode: 'linear',
                        tick0: templateTabChartConfigurationPreviewViewModel.xAxisMinValue,
                        dtick: templateTabChartConfigurationPreviewViewModel.xAxisInterval,
                        ticktext: templateTabChartConfigurationPreviewViewModel.xAxisUnits,
                        title: {
                            text: templateTabChartConfigurationPreviewViewModel.xAxisLabel + '[' +
                                templateTabChartConfigurationPreviewViewModel.xAxisUnits + ']'
                        }
                    },
                    yaxis: {
                        range: [templateTabChartConfigurationPreviewViewModel.yAxisMinValue, templateTabChartConfigurationPreviewViewModel.yAxisMaxValue],
                        //autorange: false
                        tickmode: 'linear',
                        tick0: templateTabChartConfigurationPreviewViewModel.yAxisMinValue,
                        dtick: templateTabChartConfigurationPreviewViewModel.yAxisInterval,
                        ticktext: templateTabChartConfigurationPreviewViewModel.yAxisUnits,
                        title: {
                            text: templateTabChartConfigurationPreviewViewModel.yAxisLabel + '[' +
                                templateTabChartConfigurationPreviewViewModel.yAxisUnits + ']'
                        }
                    },
                    legend: {
                        y: 0.5,
                        traceorder: 'reversed',
                        font: {
                            size: 16
                        }
                    },
                    width: 600,
                    height: 500
                };
            }
            else if (isBarChart === true) {
                trace = {
                    type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                    x: barChartXAxis,
                    y: barChartYAxis,
                    name: barChartLegendNames,
                    showlegend: barChartShowLegends,
                    width: barChartThickness,
                    orientation: templateTabChartConfigurationPreviewViewModel.chartOrientation,
                    marker: { 'color': barChartColors },
                    offset: 0
                };

                let barChartPassengerTrace = {
                    type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                    x: barChartPassengerXAxis,
                    y: barChartPassengerYAxis,
                    name: 'Passenger',
                    showlegend: barChartShowLegends,
                    width: barChartThickness,
                    orientation: templateTabChartConfigurationPreviewViewModel.chartOrientation,
                    marker: { 'color': 'pink' },
                    offset: 0
                };

                let barChartDriverTrace = {
                    type: templateTabChartConfigurationPreviewViewModel.chartTypeValue,
                    x: barChartDriverXAxis,
                    y: barChartDriverYAxis,
                    name: 'Driver',
                    showlegend: barChartShowLegends,
                    width: barChartThickness,
                    orientation: templateTabChartConfigurationPreviewViewModel.chartOrientation,
                    marker: { 'color': 'blue' },
                    offset: 0
                };

                plotlyData.push(trace);
                plotlyData.push(barChartPassengerTrace);
                plotlyData.push(barChartDriverTrace);

                layout = {
                    title: templateTabChartConfigurationPreviewViewModel.templateTabChartConfigurationName,
                    legend: {
                        y: 0.5,
                        traceorder: 'reversed',
                        font: {
                            size: 16
                        }
                    },
                    barmode: 'group',
                    bargap: 0.5,
                    width: 500,
                    height: 500
                };
            }
            if (isStackBarChart === true) {

                let acceptableTrace = {
                    y: acceptableYAxis,
                    x: acceptableXAxis,
                    name: 'Acceptable',
                    type: 'bar',
                    orientation: 'h',
                    marker: {
                        color: 'yellow'
                    }
                };
                let goodTrace = {
                    y: goodYAxis,
                    x: goodXAxis,
                    name: 'Good',
                    type: 'bar',
                    orientation: 'h',
                    marker: {
                        color: 'green'
                    }
                };

                let poorTrace = {
                    y: poorYAxis,
                    x: poorXAxis,
                    name: 'Poor',
                    type: 'bar',
                    orientation: 'h',
                    marker: {
                        color: 'red'
                    }
                };
                let marginalTrace = {
                    y: marginalYAxis,
                    x: marginalXAxis,
                    name: 'Marginal',
                    type: 'bar',
                    orientation: 'h',
                    marker: {
                        color: 'orange'
                    }
                };

                let driverTrace = {
                    y: driverYAxis,
                    x: driverXAxis,
                    name: 'Driver',
                    type: 'bar',
                    orientation: 'h',
                    width: [0.2, 0.2, 0.2],
                    offset: 0,
                    marker: {
                        color: 'blue'
                    }
                };

                let passengerTrace = {
                    y: passengerYAxis,
                    x: passengerXAxis,
                    name: 'Passenger',
                    type: 'bar',
                    orientation: 'h',
                    width: [0.2, 0.2, 0.2],
                    offset: -0.2,
                    marker: {
                        color: 'pink'
                    }
                };

                plotlyData = [poorTrace, marginalTrace, acceptableTrace, goodTrace, passengerTrace, driverTrace];
                plotlyLayout = { barmode: 'overlay' };
                plotlyOptions = { displaylogo: false };
                plotlyChartId = index;
                plotlyChartData.push({ plotlyData, plotlyLayout, plotlyOptions, plotlyChartId });
            }
            else {
                plotlyLayout = layout;
                plotlyOptions = { displaylogo: false };
                plotlyChartId = index;
                plotlyChartData.push({ plotlyData, plotlyLayout, plotlyOptions, plotlyChartId });
            }
        }
        return plotlyChartData;
    }
}