import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { MessageService, ConfirmationService } from 'primeng/api';
import { environment } from '../../../../environments/environment';
import {
    ITabConfigurationTableRows,
    ITabConfigurationTableRowsColumns,
    TabConfigurationTableRows,
    TabConfigurationTableRowsColumns
} from '../../../models/tabConfigurationTable.model';
import { TemplateService } from '../../../../services/template.service';
import { ITemplateTabTableConfigurationModel, TemplateTabTableConfigurationModel } from '../../../models/templateTabTableConfiguration.model';
import {
    ITemplateTabTableConfigurationDetailsDbModel,
    ITemplateTabTableConfigurationDetailsRowsDbModel,
    ITemplateTabTableConfigurationDetailsColumnsDbModel,
    ITemplateTabTableConfigurationDetailsRowsModel,
    ITemplateTabTableConfigurationDetailsColumnsModel
} from '../../../models/templateTabTableConfigurationDetails.model';
import { ITemplateTabConfigurationModel } from '../../../models/templateTabConfiguration.model';
import { TemplateTabTableConfigurationService } from '../../../../services/templateTabTableConfiguration.service';
import { TemplateTabTableConfigurationDetailsService } from '../../../../services/templateTabTableConfigurationDetails.service';
import { FormulaOperatorService } from '../../../../services/formulaOperator.service';
import { TemplateTabConfigurationService } from '../../../../services/templateTabConfiguration.service';
import { ATDPositionService } from '../../../../services/atdPosition.service';
import { ColumnType } from 'src/app/enums/columnType.enum';
import { IATDPosition } from 'src/app/models/atdPosition.model';
import { ASAMService } from '../../../../services/asam.service';

var _self: any;

@Component({
    selector: 'add-tabTableConfiguration-modal',
    templateUrl: './add-tabTableConfiguration-modal.component.html',
    styleUrls: ['./add-tabTableConfiguration-modal.component.scss'],
    styles: [`
    :host ::ng-deep .ui-dropdown {
                        display: inline-flex;
                        position: relative;
                        cursor: pointer;
                        min-width: 15em;
                        float:left;
                        }
    :host ::ng-deep .ui-dialog {
                    padding: 0;
                    pointer-events: auto;
                    display: flex;
                    flex-direction: column;
                    position: relative;
                }
    `]
})

export class AddTabTableConfigurationModalComponent implements OnInit {
    @Input() templateId: number;
    @Input() testId: number;
    @Input() templateTabId: number;
    @Input() templateTabConfigIndexId: number;
    @Input() templateTabConfigId: number;
    @Input() templateTabTableConfigId: number;
    @Input() templateTabTableConfigName: string;
    @Input() isVisibleModal: boolean = false;
    @Input() displayFormatType: string;
    @Input() selectedCellArray: Array<any>;
    @Input() finalIndexArray: Array<any>;
    @Output() isVisibleModalChange = new EventEmitter();
    @Output() onLoadTemplatTabEditConfiguration = new EventEmitter();

    tabTableConfigRowData: ITabConfigurationTableRows = <ITabConfigurationTableRows>{};
    tabTableConfigColumnData: ITabConfigurationTableRowsColumns = <ITabConfigurationTableRowsColumns>{};
    tabConfigTableColumnsDataList: Array<ITabConfigurationTableRowsColumns> = [];
    tabTableConfigData: Array<ITemplateTabTableConfigurationDetailsRowsModel>;

    templateTabTableConfigurationModel: ITemplateTabTableConfigurationModel = <ITemplateTabTableConfigurationModel>{};
    templateTabTableConfigurationDetailsDbModel: ITemplateTabTableConfigurationDetailsDbModel = <ITemplateTabTableConfigurationDetailsDbModel>{};

    templateTabTableConfigurationDetailsListModel: Array<ITemplateTabTableConfigurationDetailsDbModel> = [];
    templateTabTableConfigurationDetailsRowsDbListModel: Array<ITemplateTabTableConfigurationDetailsRowsDbModel> = [];

    disableSaveConfigButton: boolean = false;
    showSave: boolean = false;
    showTemplateTabTableConfiguration: boolean = false;
    showConditionalDefinedValueTextBox: boolean = false;
    selectedValue: string;
    displayColumnTypeInPopup: boolean = false;
    atdPositionDropdownDataList: Array<IATDPosition> = [];
    calculationsDropdownDataList: any;
    dbDefinedValueWithUserDefinedValueDropdownDataList: any;
    formulaOperatorsDropdownDataList: any;
    initialColId: number;
    currentColumnIndex: number;
    selectedCell: Array<any> = []; selectedCells: Array<any> = [];
    conditionalColumnSource: Array<{ label: string, value: string }> = [];
    conditionalColumnTarget: Array<{ label: string, value: string }> = [];
    isVisiblePreviewModal: boolean = false;
    showPreviewTabConfigurationTableDialog: boolean = false;

    constructor(
        private notifyService: MessageService,
        private confirmationService: ConfirmationService,
        private templateService: TemplateService,
        private templateTabTableConfigurationService: TemplateTabTableConfigurationService,
        private templateTabTableConfigurationDetailsService: TemplateTabTableConfigurationDetailsService,
        private templateTabConfigurationService: TemplateTabConfigurationService,
        private formulaOperatorService: FormulaOperatorService,
        private atdPositionService: ATDPositionService,
        private asamService: ASAMService
    ) { }

    ngOnInit() {
        _self = this;
    }

    success(message, summaryMessage = 'Success Message') {
        this.notifyService.add({ severity: 'success', summary: summaryMessage, detail: message });
    }

    error(message, summaryMessage = 'Error Message') {
        this.notifyService.add({ severity: 'error', summary: summaryMessage, detail: message });
    }

    showModal() {
        this.templateTabTableConfigurationModel.Name = this.templateTabTableConfigName;
        this.loadCalculationsDropdown();
        this.loadFormulaOperatorsValue();
        this.loadATDPosition();
    }

    cancelModal() {
        //this.onSaveTabTableConfigurationDetails('save');
        this.tabTableConfigData = [];
        this.showSave = false;
        this.isVisibleModalChange.emit(this.isVisibleModal);
        this.onLoadTemplatTabEditConfiguration.emit(this.templateTabId + "_" + this.templateTabConfigIndexId);
    }

    makeitfalse(event) {
        this.showPreviewTabConfigurationTableDialog = false;
    }

    onColumnWidthChange(colId: number, tdWidth: number) {

        let sumOfAllWidth = 0;
        let rowsData = this.tabTableConfigData;
        for (let i = 1; i < rowsData.length; i++) {
            let columnsData = rowsData[i].Columns.filter(x => !x.IsDeleted);
            for (let j = 0; j < columnsData.length - 1; j++) {
                columnsData[colId].ColumnWidth = tdWidth;
                if (i === 1) {
                    sumOfAllWidth = parseInt(sumOfAllWidth.toString()) + parseInt(columnsData[j].ColumnWidth.toString());
                }
            }
        }
        if (sumOfAllWidth > 100) {
            this.error("Total columns width should not exceed 100%.");
        }
    }

    onCellSelection(rowId, columnId) {
        if (columnId < this.tabTableConfigData[rowId].Columns.length - 1) {
            let row_index = rowId
            let col_index = columnId;
            let isRowspan = false;

            let existingColumnsData = this.tabTableConfigData[row_index].Columns;
            let existingColumnData = existingColumnsData[col_index];
            let checkRowspan = existingColumnData.RowSpan;
            let checkColspan = existingColumnData.ColumnSpan;
            _self.selectedCell = { row_index, col_index, isRowspan }
            if (row_index > 1) {
                if (_self.selectedCellArray.length == 0) {
                    _self.selectedCellArray.push(_self.selectedCell);
                    _self.finalIndexArray.push(_self.selectedCell);
                    this.tabTableConfigData[row_index].Columns[col_index].TDSelectionCss = "highlited";
                }
                else {
                    let selectedCellArrayLength = _self.selectedCellArray.length;

                    let previousRowId = _self.selectedCellArray[selectedCellArrayLength - 1].row_index;
                    let previousColId = _self.selectedCellArray[selectedCellArrayLength - 1].col_index;
                    let previousColumnsData = this.tabTableConfigData[previousRowId].Columns;
                    let previousColumnData = previousColumnsData[previousColId];
                    let previousRowspan = previousColumnData.RowSpan;
                    let previousColspan = previousColumnData.ColumnSpan - 1;

                    let currentRowId = _self.selectedCell.row_index;
                    let currentColId = _self.selectedCell.col_index;
                    let currentColumnsData = this.tabTableConfigData[currentRowId].Columns;
                    let currentColumnData = currentColumnsData[currentColId];
                    let currentRowspan = currentColumnData.RowSpan;
                    let currentColspan = currentColumnData.ColumnSpan - 1;

                    let currentColumnIndex = 0;
                    let currentRowIndex = 0;
                    if (previousRowId === currentRowId) {
                        if (currentColId > previousColId) {
                            currentColumnIndex = currentColId;
                            for (let k = currentColId; k > previousColId + 1; k--) {
                                currentColumnIndex -= 1;
                            }
                            currentColId = currentColumnIndex;
                        }
                        else if (previousColId > currentColId) {
                            currentColumnIndex = previousColId;
                            for (let k = previousColId; k > currentColId + 1; k--) {
                                currentColumnIndex -= 1;
                            }
                            previousColId = currentColumnIndex;
                        }
                    }
                    else if (previousColId === currentColId) {
                        if (currentRowId > previousRowId) {
                            currentRowIndex = currentRowId;
                            for (let k = currentRowId; k > previousRowId + 1; k--) {
                                currentRowIndex -= 1;
                            }
                            currentRowId = currentRowIndex;
                        }
                        else if (previousRowId > currentRowId) {
                            currentRowIndex = previousRowId;
                            for (let k = previousRowId; k > currentRowId + 1; k--) {
                                currentRowIndex -= 1;
                            }
                            previousRowId = currentRowIndex;
                        }
                    }

                    let foundCellPosition = _self.findValueInSelectedCellArray(_self.selectedCell, _self.selectedCellArray);
                    if (foundCellPosition < 0) {
                        if (
                            ((currentRowId === previousRowId) && ((currentColId === previousColId + 1) || (currentColId === previousColId - 1)))
                            ||
                            ((currentColId === previousColId) && ((currentRowId === previousRowId + 1) || (currentRowId === previousRowId - 1)))
                        ) {
                            if (_self.selectedCellArray.length === 1) {
                                if (_self.selectedCellArray[0].row_index === _self.selectedCell.row_index) {
                                    if (this.tabTableConfigData[_self.selectedCellArray[0].row_index].Columns[_self.selectedCellArray[0].col_index].RowSpan ===
                                        this.tabTableConfigData[_self.selectedCell.row_index].Columns[_self.selectedCell.col_index].RowSpan) {
                                        _self.selectedCell.isRowspan = false;
                                        _self.selectedCellArray.push(_self.selectedCell);
                                        _self.selectedCellArray[0].isRowspan = false;
                                        _self.finalIndexArray.push(_self.selectedCellArray[_self.selectedCellArray.length - 1]);
                                        _self.finalIndexArray[0].isRowspan = false;

                                        //Order the selection
                                        if (_self.finalIndexArray.length > 1) {
                                            if (_self.finalIndexArray[0].col_index > _self.finalIndexArray[1].col_index) {
                                                _self.finalIndexArray.splice(0, 0, _self.finalIndexArray[1]);
                                                _self.finalIndexArray.splice(_self.finalIndexArray.length - 1, 1);
                                            }
                                        }
                                        this.tabTableConfigData[row_index].Columns[col_index].TDSelectionCss = "highlited";
                                    }
                                    else {
                                        this.tabTableConfigData[row_index].Columns[col_index].TDSelectionCss = "white";
                                    }
                                }
                                else if (_self.selectedCellArray[0].col_index === _self.selectedCell.col_index) {
                                    if (this.tabTableConfigData[_self.selectedCellArray[0].row_index].Columns[_self.selectedCellArray[0].col_index].ColumnSpan ===
                                        this.tabTableConfigData[_self.selectedCell.row_index].Columns[_self.selectedCell.col_index].ColumnSpan) {
                                        _self.selectedCell.isRowspan = true;
                                        _self.selectedCellArray.push(_self.selectedCell);
                                        _self.selectedCellArray[0].isRowspan = true;
                                        _self.finalIndexArray.push(_self.selectedCellArray[_self.selectedCellArray.length - 1]);
                                        _self.finalIndexArray[0].isRowspan = true;

                                        //Order the selection
                                        if (_self.finalIndexArray.length > 1) {
                                            if (_self.finalIndexArray[0].row_index > _self.selectedCellArray[1].row_index) {
                                                _self.finalIndexArray.splice(0, 0, _self.selectedCellArray[1]);
                                                _self.finalIndexArray.splice(_self.finalIndexArray.length - 1, 1);
                                            }
                                        }
                                        this.tabTableConfigData[row_index].Columns[col_index].TDSelectionCss = "highlited";
                                    }
                                    else {
                                        this.tabTableConfigData[row_index].Columns[col_index].TDSelectionCss = "white";
                                    }
                                }
                            }
                            else if (_self.selectedCellArray.length > 1) {
                                _self.selectedCellArray.push(_self.selectedCell);
                                for (let i = 2; i < _self.selectedCellArray.length; i++) {

                                    //Colspan
                                    if ((_self.selectedCellArray[i - 2].row_index === _self.selectedCellArray[i - 1].row_index)
                                        && ((_self.selectedCellArray[i - 1].row_index === _self.selectedCell.row_index))) {
                                        if (_self.finalIndexArray.length > 1) {
                                            if (_self.finalIndexArray[i - 2].col_index > _self.finalIndexArray[i - 1].col_index) {
                                                _self.finalIndexArray.splice(0, 0, _self.finalIndexArray[i - 1]);
                                                _self.finalIndexArray.splice(i, 1);
                                            }

                                            if (_self.finalIndexArray[i - 1].col_index > _self.selectedCellArray[i].col_index) {
                                                if (this.findValueInSelectedCellArray(_self.selectedCellArray[i], _self.finalIndexArray) < 0) {
                                                    _self.finalIndexArray.splice(0, 0, _self.selectedCellArray[i]);
                                                }
                                            }
                                            else {
                                                if (this.findValueInSelectedCellArray(_self.selectedCellArray[i], _self.finalIndexArray) < 0) {
                                                    _self.finalIndexArray.push(_self.selectedCellArray[_self.selectedCellArray.length - 1]);
                                                    _self.finalIndexArray[i].isRowspan = false;
                                                }
                                            }
                                        }

                                        this.tabTableConfigData[row_index].Columns[col_index].TDSelectionCss = "highlited";
                                    }
                                    //Rowspan with order
                                    else if ((_self.selectedCellArray[i - 2].col_index === _self.selectedCellArray[i - 1].col_index)
                                        && (_self.selectedCellArray[i - 1].col_index === _self.selectedCell.col_index)) {
                                        if (_self.finalIndexArray.length > 1) {
                                            if (_self.finalIndexArray[0].row_index > _self.selectedCellArray[_self.selectedCellArray.length - 1].row_index) {
                                                _self.finalIndexArray.splice(0, 0, _self.selectedCellArray[_self.selectedCellArray.length - 1]);
                                                _self.finalIndexArray[0].isRowspan = true;
                                            }
                                            else {
                                                if (this.findValueInSelectedCellArray(_self.selectedCellArray[i], _self.finalIndexArray) < 0) {
                                                    _self.finalIndexArray.push(_self.selectedCellArray[_self.selectedCellArray.length - 1]);
                                                    _self.finalIndexArray[i].isRowspan = true;
                                                }
                                            }
                                        }
                                        else if (_self.selectedCellArray[i - 1].row_index < _self.selectedCellArray[i - 2].row_index) {
                                            _self.finalIndexArray.splice(i - 2, 1);
                                            _self.finalIndexArray.push(_self.selectedCellArray[i - 1]);
                                            _self.finalIndexArray[i - 2].isRowspan = true;
                                            _self.finalIndexArray.push(_self.selectedCellArray[i - 2]);
                                            _self.finalIndexArray[i - 1].isRowspan = true;
                                        }
                                        else {
                                            _self.finalIndexArray.push(_self.selectedCellArray[i - 1]);
                                            _self.finalIndexArray[i - 2].isRowspan = true;
                                            _self.finalIndexArray[i - 1].isRowspan = true;
                                        }
                                        this.tabTableConfigData[row_index].Columns[col_index].TDSelectionCss = "highlited";
                                    }
                                    else {
                                        let foundUnorderCell = _self.findValueInSelectedCellArray(_self.selectedCell, _self.selectedCellArray);
                                        if (foundUnorderCell >= 0) {
                                            _self.selectedCellArray.splice(foundUnorderCell, 1);
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            if (foundCellPosition >= 0) {

                                if (_self.selectedCellArray[0].isRowspan) {
                                    if (_self.selectedCellArray[0].row_index === _self.selectedCell.row_index || _self.selectedCellArray[_self.selectedCellArray.length - 1].row_index === _self.selectedCell.row_index) {
                                        _self.selectedCellArray.splice(foundCellPosition, 1);
                                        let cellIndex = _self.findValueInSelectedCellArray(_self.selectedCell, _self.finalIndexArray);
                                        _self.finalIndexArray.splice(cellIndex, 1);
                                        this.tabTableConfigData[row_index].Columns[col_index].TDSelectionCss = "white";

                                    }

                                }
                                else {
                                    if (_self.selectedCellArray[0].col_index === _self.selectedCell.col_index || _self.selectedCellArray[_self.selectedCellArray.length - 1].col_index === _self.selectedCell.col_index) {
                                        _self.selectedCellArray.splice(foundCellPosition, 1);
                                        let cellIndex = _self.findValueInSelectedCellArray(_self.selectedCell, _self.finalIndexArray);
                                        _self.finalIndexArray.splice(cellIndex, 1);
                                        this.tabTableConfigData[row_index].Columns[col_index].TDSelectionCss = "white";

                                    }
                                }
                            }
                        }
                    }
                    else {
                        if (foundCellPosition >= 0) {

                            if (_self.selectedCellArray[0].isRowspan) {
                                if (_self.selectedCellArray[0].row_index === _self.selectedCell.row_index || _self.selectedCellArray[_self.selectedCellArray.length - 1].row_index === _self.selectedCell.row_index) {
                                    _self.selectedCellArray.splice(foundCellPosition, 1);
                                    let cellIndex = _self.findValueInSelectedCellArray(_self.selectedCell, _self.finalIndexArray);
                                    _self.finalIndexArray.splice(cellIndex, 1);
                                    this.tabTableConfigData[row_index].Columns[col_index].TDSelectionCss = "white";

                                }

                            }
                            else {
                                if (_self.selectedCellArray[0].col_index === _self.selectedCell.col_index || _self.selectedCellArray[_self.selectedCellArray.length - 1].col_index === _self.selectedCell.col_index) {
                                    _self.selectedCellArray.splice(foundCellPosition, 1);
                                    let cellIndex = _self.findValueInSelectedCellArray(_self.selectedCell, _self.finalIndexArray);
                                    _self.finalIndexArray.splice(cellIndex, 1);
                                    this.tabTableConfigData[row_index].Columns[col_index].TDSelectionCss = "white";

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    findValueInSelectedCellArray(cellValue, cellArray): number {
        let foundIndex = cellArray.findIndex(d => d.col_index === cellValue.col_index && d.row_index === cellValue.row_index);
        return foundIndex;
    }

    onMergeCell() {
        let mergeCellsArrayList = _self.finalIndexArray
        let firstCollIndex = mergeCellsArrayList[0].col_index;
        let firstRowlIndex = mergeCellsArrayList[0].row_index;
        let colSpan: number = this.tabTableConfigData[firstRowlIndex].Columns[firstCollIndex].ColumnSpan;

        for (let i = 1; i < mergeCellsArrayList.length; i++) {
            let rowIndex = mergeCellsArrayList[i].row_index;
            let colIndex = mergeCellsArrayList[i].col_index;
            let spanType = mergeCellsArrayList[i].isRowspan;
            let existingColumnsData = this.tabTableConfigData[rowIndex].Columns;
            if (spanType == false) {
                for (let j = 0; j < existingColumnsData.length; j++) {
                    if (j === mergeCellsArrayList[i].col_index) {
                        existingColumnsData[j].IsDeleted = true;
                        colSpan += existingColumnsData[j].ColumnSpan;
                        existingColumnsData[j].ColumnSpan = 0;
                    }
                }
                if (i === mergeCellsArrayList.length - 1) {
                    existingColumnsData[firstCollIndex].ColumnSpan = colSpan;
                }
            }
            else {
                let existingColumnData = existingColumnsData[mergeCellsArrayList[i].col_index];
                existingColumnData.RowSpan = 0
                existingColumnData.ColumnSpan = 0
                existingColumnData.IsDeleted = true;
                existingColumnsData[mergeCellsArrayList[i].col_index] = existingColumnData;
                this.tabTableConfigData[rowIndex].Columns = existingColumnsData;

                let firstSelectedColumnsData = this.tabTableConfigData[firstRowlIndex].Columns;
                let firstSelectedColumnData = firstSelectedColumnsData[firstCollIndex];
                firstSelectedColumnData.RowSpan = firstSelectedColumnData.RowSpan + (mergeCellsArrayList.length - 1);
                firstSelectedColumnData.IsDeleted = false;
                firstSelectedColumnsData[firstCollIndex] = firstSelectedColumnData;
                this.tabTableConfigData[firstRowlIndex].Columns = firstSelectedColumnsData;
            }
        }
        this.tabTableConfigData[firstRowlIndex].Columns[firstCollIndex].TDSelectionCss = "white";
        this.selectedCellArray = [];
        this.finalIndexArray = [];
    }

    onUndoMergeCell() {
        let row = _self.selectedCell.row_index;
        let col = _self.selectedCell.col_index;
        let existingColumnsData = this.tabTableConfigData[row].Columns;
        let existingColumnData = existingColumnsData[col];
        let columnSpan = existingColumnData.ColumnSpan;;
        if (existingColumnData.ColumnSpan > 1 && existingColumnData.RowSpan > 1) {

            //RowSpan
            let tabTableConfigRowData = <ITemplateTabTableConfigurationDetailsRowsModel>{};
            let rowIds: Array<any> = [];
            tabTableConfigRowData.RowId = row;
            rowIds.push(row);
            for (let j = 1; j < existingColumnData.RowSpan; j++) {
                let existingColumnData = this.tabTableConfigData[row + j].Columns[col];
                existingColumnData.IsDeleted = false;
                existingColumnData.RowSpan = 1;
                existingColumnData.TDSelectionCss = "white";
                this.tabTableConfigData[row + j].Columns[col] = existingColumnData;
                rowIds.push(row + j);
            }
            this.tabTableConfigData[row].Columns[col].RowSpan = 1;

            //ColumnSpan
            tabTableConfigRowData.RowId = row;
            for (let k = 0; k < rowIds.length; k++) {
                existingColumnsData = this.tabTableConfigData[rowIds[k]].Columns;
                existingColumnData = existingColumnsData[col];
                let columnType = 0;
                for (let i = 1; i < columnSpan; i++) {
                    columnType = this.tabTableConfigData[0].Columns[i + col].ColumnType;
                    let configTablecolumnData = <ITemplateTabTableConfigurationDetailsColumnsModel>{};
                    configTablecolumnData = this.tabTableConfigData[rowIds[k]].Columns[i + col];
                    configTablecolumnData.ColumnSpan = 1;
                    configTablecolumnData.ColumnType = columnType;
                    configTablecolumnData.IsDeleted = false;
                    configTablecolumnData.TDSelectionCss = "white";
                    this.tabTableConfigData[rowIds[k]].Columns[i + col] = configTablecolumnData;
                }
                columnType = this.tabTableConfigData[0].Columns[col].ColumnType;
                this.tabTableConfigData[rowIds[k]].Columns[col].IsDeleted = false;
                this.tabTableConfigData[rowIds[k]].Columns[col].ColumnSpan = 1;
                this.tabTableConfigData[rowIds[k]].Columns[col].TDSelectionCss = "white";
            }
        }
        else if (existingColumnData.ColumnSpan > 1) {
            let columnSpan = existingColumnData.ColumnSpan;
            let columnType = 0;
            for (let i = 1; i < columnSpan; i++) {
                columnType = this.tabTableConfigData[0].Columns[i + col].ColumnType;
                let configTablecolumnData = <ITemplateTabTableConfigurationDetailsColumnsModel>{};
                configTablecolumnData = this.tabTableConfigData[row].Columns[i + col];
                configTablecolumnData.ColumnSpan = 1;
                configTablecolumnData.ColumnType = columnType;
                configTablecolumnData.IsDeleted = false;
                configTablecolumnData.TDSelectionCss = "white";
                this.tabTableConfigData[row].Columns[i + col] = configTablecolumnData;
            }
            columnType = this.tabTableConfigData[0].Columns[col].ColumnType;
            this.tabTableConfigData[row].Columns[col].IsDeleted = false;
            this.tabTableConfigData[row].Columns[col].ColumnSpan = 1;
            this.tabTableConfigData[row].Columns[col].TDSelectionCss = "white";
        }
        else if (existingColumnData.RowSpan > 1) {
            let tabTableConfigRowData = <ITemplateTabTableConfigurationDetailsRowsModel>{};
            tabTableConfigRowData.RowId = row;
            for (let j = 1; j < existingColumnData.RowSpan; j++) {
                let existingColumnData = this.tabTableConfigData[row + j].Columns[col];
                existingColumnData.IsDeleted = false;
                existingColumnData.RowSpan = 1;
                existingColumnData.TDSelectionCss = "white";
                this.tabTableConfigData[row + j].Columns[col] = existingColumnData;
            }
            this.tabTableConfigData[row].Columns[col].RowSpan = 1;
            this.tabTableConfigData[row].Columns[col].ColumnSpan = 1;
            this.tabTableConfigData[row].Columns[col].TDSelectionCss = "white";
        }
        else {
            //can not undo merge for single cell
            let configTablecolumnData = <ITemplateTabTableConfigurationDetailsColumnsModel>{};
            configTablecolumnData = this.tabTableConfigData[row].Columns[col];
            configTablecolumnData.TDSelectionCss = "white";
            this.error("Can not undo merge for single cell.");
        }
        this.selectedCellArray = [];
        this.finalIndexArray = [];
    }

    onDisplayColumnType(initialColId, currentIndex) {
        this.displayColumnTypeInPopup = true;
        this.currentColumnIndex = currentIndex;
        this.initialColId = initialColId;
    }

    onDbColumnValueChange(rowId, columnId, selectedValue) {
        let selectedValueSplitLength = selectedValue.split('_').length;
        let valueType = selectedValue.split('_')[selectedValueSplitLength - 1];
        this.tabTableConfigData[rowId].Columns[columnId].ValueType = valueType;
    }

    onConditionalValueChange(rowId, columnId, selectedValue) {
        if (selectedValue === "UserDefinedValue") {
            this.tabTableConfigData[rowId].Columns[columnId].ShowConditionalDefinedValueTextBox = true;
        }
        else {
            this.tabTableConfigData[rowId].Columns[columnId].ShowConditionalDefinedValueTextBox = false;
        }
    }

    onHeaderNameChange(rowId, columnId, selectedValue) {
        if (selectedValue != "") {
            let conditionalColumn = { label: selectedValue, value: selectedValue };
            this.conditionalColumnSource.push(conditionalColumn);
            this.conditionalColumnTarget.push(conditionalColumn);
        }
    }

    loadATDPosition() {
        this.atdPositionService.getATDPositionData().subscribe(response => {
            this.atdPositionDropdownDataList = [];
            this.atdPositionDropdownDataList = response.map(item => {
                return { label: item.name, value: item.name };
            });
        });
    }

    loadCalculationsDropdown() {
        this.templateService.getById(this.templateId).subscribe(res => {
            this.asamService.getASAMCalculation(res.testType, res.testMode).subscribe(response => {
                this.calculationsDropdownDataList = [];
                this.calculationsDropdownDataList = response.map(item => {
                    return { label: item, value: item };
                });
            });
        });
    }

    loadFormulaOperatorsValue() {
        this.formulaOperatorService.getFormulaOperatorsData().subscribe(response => {
            this.formulaOperatorsDropdownDataList = [];
            this.formulaOperatorsDropdownDataList = response.map(item => {
                return { label: item.operator, value: item.id };
            });
        });
    }

    loadDefaultTabTableConfiguration() {
        this.tabTableConfigData = [];
        this.conditionalColumnTarget = [];
        let conditionalColumn = { label: "User Defined Value", value: "UserDefinedValue" };
        this.conditionalColumnTarget.push(conditionalColumn);
        for (let i = 0; i < 4; i++) {
            let tabTableConfigRowData = <ITemplateTabTableConfigurationDetailsRowsModel>{};
            tabTableConfigRowData.RowId = i;
            let tabTableConfigColumnsDataList = [];
            for (let j = 0; j < 5; j++) {
                let columnName = '';
                let rowSpan = 1;
                let columnSpan = 1;
                let columnType = ColumnType.UserDefinedColumn;
                let isDeleted = false;
                let showDeleteIcon = false;
                let isConfigHeader = false;
                let isHeader = false;

                if (i == 0) {
                    if (j == 2) {
                        columnType = ColumnType.DbTestResult;
                    }
                }
                else if (i == 1) {
                    isConfigHeader = true;
                    if (j == 0) {
                        columnName = 'User Defined Column';
                    }
                    if (j == 1) {
                        columnName = 'User Criteria';
                    }
                    else if (j == 2) {
                        columnName = 'Test Result';
                        columnType = ColumnType.DbTestResult;
                    }
                    else if (j == 3) {
                        columnName = 'Conditional Column';
                        columnType = ColumnType.ConditionalColumn;
                    }
                    else if (j == 4) {
                        columnName = 'Delete';
                        columnType = 0;
                    }
                }
                else if (i === 2) {
                    if (j < 4) {
                        isHeader = true;
                    }
                }
                else if (i == 3) {

                    if (j == 2) {
                        columnType = ColumnType.DbTestResult;
                    }
                    else if (j == 3) {
                        columnType = ColumnType.ConditionalColumn;
                    }
                }

                let tabTableConfigColumnData = <ITemplateTabTableConfigurationDetailsColumnsModel>{};
                tabTableConfigColumnData.Id = 0;
                tabTableConfigColumnData.ColumnId = j;
                tabTableConfigColumnData.UserDefinedColumn = columnName;
                tabTableConfigColumnData.ColumnType = (j == 4) ? 0 : columnType;
                tabTableConfigColumnData.RowSpan = rowSpan;
                tabTableConfigColumnData.ColumnSpan = columnSpan;
                tabTableConfigColumnData.ColumnWidth = 25;
                tabTableConfigColumnData.DbColumn = "";
                tabTableConfigColumnData.ValueType = "";
                tabTableConfigColumnData.AtdPosition = "";
                tabTableConfigColumnData.ResultColumn = "";
                tabTableConfigColumnData.ColourCode = "";
                tabTableConfigColumnData.IsConfigHeader = isConfigHeader;
                tabTableConfigColumnData.IsHeader = isHeader;
                tabTableConfigColumnData.IsDeleted = isDeleted;
                tabTableConfigColumnData.FormulaOperatorId = 0;
                tabTableConfigColumnData.TemplateTabTableConfigurationFormulaId = 0;
                tabTableConfigColumnData.ConditionalUserDefinedColumn = "";
                tabTableConfigColumnData.ShowConditionalDefinedValueTextBox = false;
                tabTableConfigColumnData.ShowDeleteIcon = (j == 4) ? true : showDeleteIcon;
                tabTableConfigColumnData.TDSelectionCss = 'white';
                tabTableConfigColumnsDataList.push(tabTableConfigColumnData);
            }
            tabTableConfigRowData.Columns = tabTableConfigColumnsDataList;
            tabTableConfigRowData.TotalRows = (i + 1);
            tabTableConfigRowData.TotalCoumns = (tabTableConfigRowData.Columns.length - 1);
            this.tabTableConfigData.push(tabTableConfigRowData);
        }
    }

    loadTabTableConfigurtationByDbCoulmnsLength(dbColumns) {
        let tabTableConfigRowData = <ITemplateTabTableConfigurationDetailsRowsModel>{};
        tabTableConfigRowData.RowId = 0;
        let tabTableConfigColumnsDataList = [];

        for (let i = 0; i <= dbColumns.length; i++) {
            let tabTableConfigColumnData = <ITemplateTabTableConfigurationDetailsColumnsModel>{};
            tabTableConfigColumnData.Id = 0;
            tabTableConfigColumnData.ColumnId = i;
            tabTableConfigColumnData.UserDefinedColumn = "";
            tabTableConfigColumnData.DbColumn = "";
            tabTableConfigColumnData.ValueType = "";
            tabTableConfigColumnData.AtdPosition = "";
            tabTableConfigColumnData.ResultColumn = "";
            tabTableConfigColumnData.ColumnType = (i < dbColumns.length) ? dbColumns[i].columnType : 0;
            tabTableConfigColumnData.RowSpan = 1;
            tabTableConfigColumnData.ColumnSpan = 1;
            tabTableConfigColumnData.ColumnWidth = 0;
            tabTableConfigColumnData.ColourCode = "";
            tabTableConfigColumnData.IsConfigHeader = false;
            tabTableConfigColumnData.IsHeader = false;
            tabTableConfigColumnData.IsDeleted = (i < dbColumns.length) ? dbColumns[i].isDeleted : false;
            tabTableConfigColumnData.ShowDeleteIcon = false;
            tabTableConfigColumnData.FormulaOperatorId = 0;
            tabTableConfigColumnData.TemplateTabTableConfigurationFormulaId = 0;
            tabTableConfigColumnData.ConditionalUserDefinedColumn = "";
            tabTableConfigColumnData.ShowConditionalDefinedValueTextBox = false;
            tabTableConfigColumnData.TDSelectionCss = 'white';
            tabTableConfigColumnsDataList.push(tabTableConfigColumnData);
        }
        tabTableConfigRowData.Columns = tabTableConfigColumnsDataList;
        this.tabTableConfigData.push(tabTableConfigRowData);
    }

    loadDbTabTableConfiguration(templateTabTableConfigId) {
        this.tabTableConfigData = [];
        this.conditionalColumnTarget = [];
        let conditionalColumn = { label: "User Defined Value", value: "UserDefinedValue" };
        this.conditionalColumnTarget.push(conditionalColumn);

        this.templateTabTableConfigurationDetailsService.getByTemplateTabTableConfigId(templateTabTableConfigId).subscribe(response => {
            this.templateTabTableConfigurationDetailsRowsDbListModel = response;

            if (this.templateTabTableConfigurationDetailsRowsDbListModel.length > 0) {
                this.loadTabTableConfigurtationByDbCoulmnsLength(this.templateTabTableConfigurationDetailsRowsDbListModel[0].columns);
                this.conditionalColumnSource = [];
                for (let k = 0; k < this.templateTabTableConfigurationDetailsRowsDbListModel.length; k++) {

                    let tabTableConfigRowData = <ITemplateTabTableConfigurationDetailsRowsModel>{};
                    let tabTableConfigColumnsDataList = [];
                    let tabTableConfigColumnsDbData = this.templateTabTableConfigurationDetailsRowsDbListModel[k].columns;

                    for (let l = 0; l <= tabTableConfigColumnsDbData.length; l++) {
                        let tabTableConfigColumnData = <ITemplateTabTableConfigurationDetailsColumnsModel>{};
                        let deleteCellLength = tabTableConfigColumnsDbData.filter(x => x.isDeleted).length;

                        if (l < tabTableConfigColumnsDbData.length && tabTableConfigColumnsDbData[l].isHeader) {
                            if (tabTableConfigColumnsDbData[l].userDefinedColumn != ""
                            ) {
                                conditionalColumn = { label: tabTableConfigColumnsDbData[l].userDefinedColumn, value: tabTableConfigColumnsDbData[l].userDefinedColumn };
                                this.conditionalColumnSource.push(conditionalColumn);
                                this.conditionalColumnTarget.push(conditionalColumn);
                            }
                        }

                        tabTableConfigRowData.RowId = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].rowId : k + 1;
                        tabTableConfigColumnData.RowId = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].rowId : k + 1;
                        tabTableConfigColumnData.Id = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].id : 0;
                        tabTableConfigColumnData.ColumnId = l;
                        tabTableConfigColumnData.UserDefinedColumn = (k === 0 && l === tabTableConfigColumnsDbData.length) ? "Delete" : (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].userDefinedColumn : "";
                        tabTableConfigColumnData.DbColumn = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].dbColumn : "";
                        tabTableConfigColumnData.ValueType = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].valueType : "";
                        tabTableConfigColumnData.AtdPosition = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].atdPosition : "";
                        tabTableConfigColumnData.ResultColumn = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].resultColumn : "";
                        tabTableConfigColumnData.ColumnType = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnType : 0;
                        tabTableConfigColumnData.RowSpan = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].rowSpan : 1;
                        tabTableConfigColumnData.ColumnSpan = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnSpan : 1;
                        tabTableConfigColumnData.ColumnWidth = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnWidth : 25;
                        tabTableConfigColumnData.ColourCode = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].colourCode : "";
                        tabTableConfigColumnData.IsConfigHeader = (k === 0 && l === tabTableConfigColumnsDbData.length) ? true : (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].isConfigHeader : false;
                        tabTableConfigColumnData.IsHeader = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].isHeader : false;
                        tabTableConfigColumnData.FormulaOperatorId = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].formulaOperatorId : 0;
                        tabTableConfigColumnData.TemplateTabTableConfigurationFormulaId = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].templateTabTableConfigurationFormulaId : 0;
                        tabTableConfigColumnData.ShowConditionalDefinedValueTextBox = (l < tabTableConfigColumnsDbData.length) ? (tabTableConfigColumnsDbData[l].conditionalUserDefinedColumn !== "") ? true : false : false;
                        tabTableConfigColumnData.ConditionalUserDefinedColumn = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].conditionalUserDefinedColumn : "";
                        tabTableConfigColumnData.TDSelectionCss = 'white';
                        if (deleteCellLength === tabTableConfigColumnsDbData.length) {
                            tabTableConfigColumnData.IsDeleted = true;
                            tabTableConfigColumnData.ShowDeleteIcon = false;
                        }
                        else {
                            tabTableConfigColumnData.IsDeleted = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].isDeleted : false;
                            tabTableConfigColumnData.ShowDeleteIcon = (k > 1 && l === tabTableConfigColumnsDbData.length) ? true : false;
                        }
                        tabTableConfigColumnsDataList.push(tabTableConfigColumnData);
                    }
                    tabTableConfigRowData.Columns = tabTableConfigColumnsDataList;
                    this.tabTableConfigData.push(tabTableConfigRowData);
                }
            }
            else {
                this.loadDefaultTabTableConfiguration();
            }
        });
    }

    getColspanValue(rowId, columnId): number {
        let colSpanValue: number = 0;
        if (this.tabTableConfigData[rowId].Columns[columnId - 1].ColumnSpan === 0) {
            for (let k = columnId; k >= 0; k--) {
                if (this.tabTableConfigData[rowId].Columns[k].ColumnSpan > 1) {
                    colSpanValue = this.tabTableConfigData[rowId].Columns[k].ColumnSpan;
                    return colSpanValue;
                }
            }
        }
    }

    getColspanIndex(rowId, columnId): number {
        let ColspanIndex: number = 0;
        if (this.tabTableConfigData[rowId].Columns[columnId - 1].ColumnSpan === 0) {
            for (let k = columnId; k >= 0; k--) {
                if (this.tabTableConfigData[rowId].Columns[k].ColumnSpan > 1) {
                    ColspanIndex = this.tabTableConfigData[rowId].Columns[k].ColumnId;
                    return ColspanIndex;
                }
            }
        }
    }

    onAddNewRow() {
        let lastRowId = this.tabTableConfigData.length;
        let tabTableConfigRowData = <ITemplateTabTableConfigurationDetailsRowsModel>{};
        tabTableConfigRowData.RowId = lastRowId;
        let tabConfigTableColumnsDataList = [];
        let columnsData = this.tabTableConfigData[1].Columns;
        for (let j = 0; j < columnsData.length; j++) {
            let tabTableConfigColumnData = <ITemplateTabTableConfigurationDetailsColumnsModel>{};
            tabTableConfigColumnData.Id = 0;
            tabTableConfigColumnData.ColumnId = j;
            tabTableConfigColumnData.UserDefinedColumn = "";
            tabTableConfigColumnData.DbColumn = "";
            tabTableConfigColumnData.ValueType = "";
            tabTableConfigColumnData.AtdPosition = "";
            tabTableConfigColumnData.ResultColumn = "";
            tabTableConfigColumnData.ColumnType = (j < columnsData.length - 1) ? columnsData[j].ColumnType : 0;
            tabTableConfigColumnData.RowSpan = 1;
            tabTableConfigColumnData.ColumnSpan = 1;
            tabTableConfigColumnData.ColumnWidth = (j < columnsData.length - 1) ? columnsData[j].ColumnWidth : 25;
            tabTableConfigColumnData.ColourCode = "";
            tabTableConfigColumnData.IsConfigHeader = false;
            tabTableConfigColumnData.IsHeader = false;
            tabTableConfigColumnData.IsDeleted = (j < columnsData.length - 1) ? columnsData[j].IsDeleted : false;
            tabTableConfigColumnData.ShowDeleteIcon = (j < columnsData.length - 1) ? false : true;
            tabTableConfigColumnData.FormulaOperatorId = 0;
            tabTableConfigColumnData.TemplateTabTableConfigurationFormulaId = 0;
            tabTableConfigColumnData.ConditionalUserDefinedColumn = "";
            tabTableConfigColumnData.ShowConditionalDefinedValueTextBox = false;
            tabTableConfigColumnData.TDSelectionCss = 'white';
            tabConfigTableColumnsDataList.push(tabTableConfigColumnData);
        }
        tabTableConfigRowData.Columns = tabConfigTableColumnsDataList;
        tabTableConfigRowData.TotalCoumns = (tabTableConfigRowData.Columns.length - 1);
        this.tabTableConfigData[lastRowId] = tabTableConfigRowData;
        this.success('Row Added Successfully');
    }

    onAddNewColumn() {
        let tabTableConfigRows = this.tabTableConfigData;
        for (let i = 0; i < tabTableConfigRows.length; i++) {
            let existingColumnsData = tabTableConfigRows[i].Columns;
            let tabTableConfigRowData = tabTableConfigRows[i];
            let existingColumnsDataLength = existingColumnsData.length;

            let deletedColumnsLength = existingColumnsData.filter(x => x.IsDeleted).length;
            for (let j = 0; j < existingColumnsDataLength; j++) {
                if (j == this.currentColumnIndex) {
                    let configTablecolumnData = <ITemplateTabTableConfigurationDetailsColumnsModel>{};
                    tabTableConfigRowData.RowId = tabTableConfigRowData.Columns[j].RowId;
                    configTablecolumnData.Id = 0;
                    configTablecolumnData.ColumnId = j;
                    if (this.selectedValue == "testResultColumn") {
                        configTablecolumnData.ColumnType = ColumnType.DbTestResult;
                        configTablecolumnData.UserDefinedColumn = (i === 1) ? "Test Result" : "";
                    }
                    else if (this.selectedValue == "conditionalColumn") {
                        configTablecolumnData.ColumnType = ColumnType.ConditionalColumn;
                        configTablecolumnData.UserDefinedColumn = (i === 1) ? "Conditional Defined Column" : "";
                    }
                    else {
                        configTablecolumnData.ColumnType = ColumnType.UserDefinedColumn;
                        configTablecolumnData.UserDefinedColumn = (i === 1) ? "User Defined Column" : "";
                    }

                    configTablecolumnData.DbColumn = "";
                    configTablecolumnData.ValueType = "";
                    configTablecolumnData.AtdPosition = "";
                    configTablecolumnData.ResultColumn = "";
                    configTablecolumnData.RowSpan = 1;
                    configTablecolumnData.ColumnSpan = (tabTableConfigRowData.Columns[j].ColumnSpan > 0) ? (tabTableConfigRowData.Columns[j].ColumnSpan < 2) ? tabTableConfigRowData.Columns[j].ColumnSpan : 1 : 1;
                    configTablecolumnData.ColumnWidth = 25;
                    configTablecolumnData.ColourCode = "";
                    configTablecolumnData.IsConfigHeader = (i === 1) ? true : false;
                    configTablecolumnData.IsHeader = (i === 2) ? true : false;

                    configTablecolumnData.ShowDeleteIcon = false;
                    configTablecolumnData.FormulaOperatorId = 0;
                    configTablecolumnData.TemplateTabTableConfigurationFormulaId = 0;
                    configTablecolumnData.ConditionalUserDefinedColumn = "";
                    configTablecolumnData.ShowConditionalDefinedValueTextBox = false;
                    configTablecolumnData.TDSelectionCss = "white";

                    if (this.getColspanValue(i, j) > 0) {
                        let colspanColumnIndex = this.getColspanIndex(i, j)
                        existingColumnsData[colspanColumnIndex].ColumnSpan += 1;
                        configTablecolumnData.IsDeleted = true;
                    }
                    else {
                        configTablecolumnData.IsDeleted = (deletedColumnsLength === existingColumnsDataLength) ? true : false;
                    }

                    if ((j - 1) < 0) {
                        existingColumnsData.splice(0, 0, configTablecolumnData);
                    }
                    else {
                        existingColumnsData.splice((j), 0, configTablecolumnData);
                    }
                }
            }
            this.tabTableConfigData[i].Columns = existingColumnsData;
        }
        this.reOrderTableDataIndex(this.tabTableConfigData);
        this.displayColumnTypeInPopup = false;
        this.success('Column Added Successfully');
    }

    onDeleteRow(rowId) {
        this.confirmationService.confirm({
            header: "Delete Confirmation",
            message: "Are you sure?",
            key: 'edit-deleteRow',
            accept: () => {
                let rowData = this.tabTableConfigData[rowId];
                let columnsData = rowData.Columns;
                let columnsDataList = [];
                for (let i = 0; i < columnsData.length; i++) {
                    columnsData[i].IsDeleted = true;
                    columnsData[i].ColumnSpan = 0;
                }
                this.tabTableConfigData[rowId].Columns = columnsData;
                this.success('Row Deleted Successfully');
            }
        });
    }

    onDeleteColumn(columnId) {
        this.confirmationService.confirm({
            header: "Delete Confirmation",
            message: "Are you sure?",
            key: 'edit-deleteColumn',
            accept: () => {
                let existingRowlength = this.tabTableConfigData.length;
                for (let i = 0; i < existingRowlength; i++) {
                    let columnsData = this.tabTableConfigData[i].Columns;
                    for (let j = 0; j < columnsData.length; j++) {
                        if (j == columnId) {
                            if (columnsData[j].ColumnSpan > 1) {
                                columnsData[j].ColumnSpan = columnsData[j].ColumnSpan - 1;
                            }
                            else if ((columnsData[j].ColumnSpan == 0) && (i > 1)) {
                                for (let k = j; k >= 0; k--) {
                                    if (columnsData[k].ColumnSpan !== undefined && columnsData[k].ColumnSpan > 1) {
                                        columnsData[k].IsDeleted = false;
                                        columnsData[k].ColumnSpan -= 1;
                                        this.tabTableConfigData[i].Columns[k] = columnsData[k];
                                        break;
                                    }
                                }
                            }
                            else {
                                columnsData[j].IsDeleted = true;
                                columnsData[j].ColumnSpan = 0;
                            }
                        }
                        this.tabTableConfigData[i].Columns[j] = columnsData[j];
                    }
                }
                this.success('Column Deleted Successfully');
            }
        });
    }

    reOrderTableDataIndex(tabTableConfigData) {
        let existingRows = tabTableConfigData;
        for (let i = 0; i < existingRows.length; i++) {
            let tabTableConfigRowData = <ITemplateTabTableConfigurationDetailsRowsModel>{};
            tabTableConfigRowData = tabTableConfigData[i];
            tabTableConfigRowData.RowId = i;
            for (let j = 0; j < existingRows[i].Columns.length; j++) {
                let tabTableConfigColumnData = <ITemplateTabTableConfigurationDetailsColumnsModel>{};
                tabTableConfigColumnData = existingRows[i].Columns[j];

                if (existingRows[i].Columns[j].IsHeader) {
                    if (existingRows[i].Columns[j].UserDefinedColumn != "") {
                        let conditionalColumn = { label: existingRows[i].Columns[j].UserDefinedColumn, value: existingRows[i].Columns[j].UserDefinedColumn };
                        this.conditionalColumnSource.push(conditionalColumn);
                        this.conditionalColumnTarget.push(conditionalColumn);
                    }
                }

                tabTableConfigColumnData.Id = existingRows[i].Columns[j].Id;
                tabTableConfigColumnData.ColumnId = j;
                tabTableConfigColumnData.UserDefinedColumn = (j === existingRows[i].Columns.length - 1 && i === 1) ? "Delete" : existingRows[i].Columns[j].UserDefinedColumn;
                tabTableConfigColumnData.ColumnType = (j === existingRows[i].Columns.length - 1) ? 0 : existingRows[i].Columns[j].ColumnType;
                tabTableConfigColumnData.RowSpan = existingRows[i].Columns[j].RowSpan;
                tabTableConfigColumnData.ColumnSpan = existingRows[i].Columns[j].ColumnSpan;
                tabTableConfigColumnData.ColumnWidth = existingRows[i].Columns[j].ColumnWidth;
                tabTableConfigColumnData.DbColumn = existingRows[i].Columns[j].DbColumn;
                tabTableConfigColumnData.ValueType = existingRows[i].Columns[j].ValueType;
                tabTableConfigColumnData.AtdPosition = existingRows[i].Columns[j].AtdPosition;
                tabTableConfigColumnData.ResultColumn = existingRows[i].Columns[j].ResultColumn;
                tabTableConfigColumnData.ColourCode = existingRows[i].Columns[j].ColourCode;
                tabTableConfigColumnData.IsConfigHeader = (i === 1) ? true : false;
                tabTableConfigColumnData.IsHeader = (i === 2) ? true : false;
                tabTableConfigColumnData.IsDeleted = existingRows[i].Columns[j].IsDeleted;
                tabTableConfigColumnData.ShowDeleteIcon = (j === existingRows[i].Columns.length - 1 && i > 1) ? true : false;
                tabTableConfigColumnData.FormulaOperatorId = existingRows[i].Columns[j].FormulaOperatorId;
                tabTableConfigColumnData.TemplateTabTableConfigurationFormulaId = existingRows[i].Columns[j].TemplateTabTableConfigurationFormulaId;
                tabTableConfigColumnData.ConditionalUserDefinedColumn = "";
                tabTableConfigColumnData.ShowConditionalDefinedValueTextBox = false;
                tabTableConfigColumnData.TDSelectionCss = 'white';
                this.tabTableConfigData[i].Columns[j] = tabTableConfigColumnData;
            }

            let existingConditionTargetData = this.conditionalColumnTarget.some(function (el) { return el.value === 'UserDefinedValue' });
            if (!existingConditionTargetData) {
                let conditionalColumn = { label: "User Defined Value", value: "UserDefinedValue" };
                this.conditionalColumnTarget.push(conditionalColumn);
            }
        }
    }

    onSaveTabTableConfiguration() {
        this.templateTabTableConfigurationModel.TemplateTabConfigId = this.templateTabConfigId;
        if (this.templateTabTableConfigurationModel.Name != undefined || this.templateTabTableConfigurationModel.Name != null) {
            this.templateTabTableConfigurationService.checkTemplateTabTableConfigName(this.templateTabTableConfigurationModel.TemplateTabConfigId, this.templateTabTableConfigurationModel.Name).subscribe(result => {
                if (!result) {
                    this.templateTabTableConfigurationService.postData(this.templateTabTableConfigurationModel).subscribe(response => {
                        if (response.isSuccess) {
                            this.templateTabTableConfigId = response.id;
                            this.templateTabConfigurationService.getById(this.templateTabConfigId).subscribe(response => {
                                let templateTabConfigurationData = <ITemplateTabConfigurationModel>{};
                                templateTabConfigurationData = response;
                                templateTabConfigurationData.TemplateTabId = response.templateTabId;
                                templateTabConfigurationData.Id = response.id;
                                templateTabConfigurationData.RowId = response.rowId;
                                templateTabConfigurationData.ColumnId = response.columnId;
                                templateTabConfigurationData.ColumnSpan = response.columnSpan;
                                templateTabConfigurationData.DisplayFormatTypeId = (this.displayFormatType.split('_')[0].toString() == "Table") ? 1 : (this.displayFormatType.split('_')[0].toString() == "Chart") ? 2 : 3;
                                templateTabConfigurationData.IsDeleted = response.isDeleted;
                                this.templateTabConfigurationService.putDataById(templateTabConfigurationData).subscribe(response => {
                                    if (response.isSuccess) {
                                        this.showTemplateTabTableConfiguration = true;
                                        this.disableSaveConfigButton = true;
                                        this.loadDefaultTabTableConfiguration();
                                    }
                                });

                            });
                            this.success('Tab Table Configuration Successfully Saved');
                        }
                        else {
                            this.error("Failed To Save");
                        }
                    });
                }
                else {
                    this.error("Config Name Is Already Exists");
                }
            });
        } else {
            this.error("Please Enter Configuration Name");
        }
    }

    onSaveTabTableConfigurationDetails(saveOrPreview) {
        this.templateTabTableConfigurationDetailsListModel = [];
        let existingTableRowsData = this.tabTableConfigData;

        for (let i = 1; i < existingTableRowsData.length; i++) {
            let rowData = existingTableRowsData[i];
            for (let j = 0; j < rowData.Columns.length - 1; j++) {
                let templateTabTableConfigurationDetailsModel = <ITemplateTabTableConfigurationDetailsDbModel>{};
                let columnData = rowData.Columns[j];
                templateTabTableConfigurationDetailsModel.id = columnData.Id;
                templateTabTableConfigurationDetailsModel.templateTabTableConfigurationId = this.templateTabTableConfigId;
                templateTabTableConfigurationDetailsModel.templateTabTableConfigurationName = this.templateTabTableConfigurationModel.Name;
                templateTabTableConfigurationDetailsModel.rowId = (i);
                templateTabTableConfigurationDetailsModel.rowSpan = columnData.RowSpan;
                templateTabTableConfigurationDetailsModel.columnId = j + 1;
                templateTabTableConfigurationDetailsModel.columnSpan = columnData.ColumnSpan;
                templateTabTableConfigurationDetailsModel.columnWidth = columnData.ColumnWidth;
                templateTabTableConfigurationDetailsModel.columnType = columnData.ColumnType;
                templateTabTableConfigurationDetailsModel.userDefinedColumn = columnData.UserDefinedColumn;
                templateTabTableConfigurationDetailsModel.dbColumn = columnData.DbColumn;
                templateTabTableConfigurationDetailsModel.valueType = columnData.ValueType;
                templateTabTableConfigurationDetailsModel.atdPosition = columnData.AtdPosition;
                templateTabTableConfigurationDetailsModel.conditionalUserDefinedColumn = columnData.ConditionalUserDefinedColumn;
                templateTabTableConfigurationDetailsModel.resultColumn = columnData.ResultColumn;
                templateTabTableConfigurationDetailsModel.colourCode = columnData.ColourCode;
                templateTabTableConfigurationDetailsModel.isConfigHeader = columnData.IsConfigHeader;
                templateTabTableConfigurationDetailsModel.isHeader = columnData.IsHeader;
                templateTabTableConfigurationDetailsModel.isDbColumn = (columnData.ColumnType == ColumnType.DbTestResult) ? true : false;
                templateTabTableConfigurationDetailsModel.isUserDefined = (columnData.ColumnType == ColumnType.UserDefinedColumn) ? true : false;
                templateTabTableConfigurationDetailsModel.isConditional = (columnData.ColumnType == ColumnType.ConditionalColumn) ? true : false;
                templateTabTableConfigurationDetailsModel.isReadOnly = false;
                templateTabTableConfigurationDetailsModel.isDeleted = columnData.IsDeleted;
                templateTabTableConfigurationDetailsModel.formulaOperatorId = columnData.FormulaOperatorId;
                templateTabTableConfigurationDetailsModel.templateTabTableConfigurationFormulaId = columnData.TemplateTabTableConfigurationFormulaId;
                this.templateTabTableConfigurationDetailsListModel.push(templateTabTableConfigurationDetailsModel);
            }
        }

        this.templateTabTableConfigurationDetailsService.putData(this.templateTabTableConfigurationDetailsListModel).subscribe(response => {
            if (response.isSuccess) {
                this.loadDbTabTableConfiguration(this.templateTabTableConfigId);
                this.success('Tab Configuration Details Successfully Saved');
                if (saveOrPreview == 'preview') {
                    this.showPreviewTabConfigurationTableDialog = true;
                }
            }
            else if (response.Message === "Configuration Name Alsready Exists") {
                this.error("Configuration Name Alsready Exists");
            }
            else {
                this.error("Tab Configuration Details Failed To Save");
            }
        });
    }
}
