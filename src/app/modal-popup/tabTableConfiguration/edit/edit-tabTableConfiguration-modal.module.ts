import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route } from '@angular/compiler/src/core';
import { Routes, RouterModule } from '@angular/router';
import { TabViewModule } from 'primeng/tabview';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { RadioButtonModule } from 'primeng/radiobutton';
import { Component, ViewChild, ViewContainerRef, Input, EventEmitter, Output } from '@angular/core';
import { TabView, TabPanel } from 'primeng/primeng';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { ToastModule } from 'primeng/toast';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { EditTabTableConfigurationModalComponent } from '../edit/edit-tabTableConfiguration-modal.component';
import { PreviewTabTableConfigurationModalModule } from '../preview/preview-tabTableConfiguration-modal.module';
import { from } from 'rxjs';

@NgModule({
    declarations: [EditTabTableConfigurationModalComponent],
    imports: [
        PreviewTabTableConfigurationModalModule,
        DialogModule,
        DropdownModule,
        FormsModule,
        ButtonModule,
        TableModule,
        TabViewModule,
        CommonModule,
        ToastModule,
        MessagesModule,
        MessageModule,
        ConfirmDialogModule,
        RadioButtonModule
    ],
    exports: [EditTabTableConfigurationModalComponent]
})
export class EditTabTableConfigurationModalModule { }
