import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { ITemplateTabTableConfigurationDetailsPreviewRowsDbModel, ITemplateTabTableConfigurationDetailsPreviewColumnsDbModel } from '../../../models/templateTabTableConfigurationDetails.model';
import { TemplateTabTableConfigurationDetailsService } from '../../../../services/templateTabTableConfigurationDetails.service';
import { flatMap } from 'rxjs/operators';
import { NgxSpinnerService } from "ngx-spinner";

var _self: any;

@Component({
    selector: 'preview-tabTableConfiguration-modal',
    templateUrl: './preview-tabTableConfiguration-modal.component.html',
    styleUrls: ['./preview-tabTableConfiguration-modal.component.scss'],
    styles: [`
    :host ::ng-deep .ui-dialog {
                    padding: 0;
                    pointer-events: auto;
                    display: flex;
                    flex-direction: column;
                    position: relative;
                }
    `]
})

export class PreviewTabTableConfigurationModalComponent implements OnInit {
    @Input() templateTabTableConfigId: number;
    @Input() testId: number;
    @Input() isVisiblePreviewModal: boolean = false;
    @Output() isVisibleModalChange = new EventEmitter();

    tabTableConfigDataPreview: Array<ITemplateTabTableConfigurationDetailsPreviewRowsDbModel>;
    templateTabTableConfigurationDetailsPreviewRowsDbModel: ITemplateTabTableConfigurationDetailsPreviewRowsDbModel = <ITemplateTabTableConfigurationDetailsPreviewRowsDbModel>{};
    templateTabTableConfigurationDetailsPreviewColumnsDbModel: ITemplateTabTableConfigurationDetailsPreviewColumnsDbModel = <ITemplateTabTableConfigurationDetailsPreviewColumnsDbModel>{};
    templateTabTableConfigurationDetailsPreviewRowsDbModelList: Array<ITemplateTabTableConfigurationDetailsPreviewRowsDbModel> = [];
    templateTabTableConfigurationDetailsPreviewColumnsDbModelList: Array<ITemplateTabTableConfigurationDetailsPreviewColumnsDbModel> = [];

    constructor(
        private templateTabTableConfigurationDetailsService: TemplateTabTableConfigurationDetailsService,
        private SpinnerService: NgxSpinnerService
    ) { }

    ngOnInit() {
    }

    showModal() {
        this.loadDbTabTableConfigurationPreview(this.templateTabTableConfigId);
    }

    cancelModal() {
        this.tabTableConfigDataPreview = [];
        this.isVisibleModalChange.emit(this.isVisiblePreviewModal);
    }

    loadDbTabTableConfigurationPreview(configId: number) {
        this.tabTableConfigDataPreview = [];
        let tabTableConfigurationRowsData: Array<ITemplateTabTableConfigurationDetailsPreviewRowsDbModel> = [];
        this.SpinnerService.show();
        this.templateTabTableConfigurationDetailsService.previewByTemplateTabTableConfigId(this.templateTabTableConfigId, this.testId).subscribe(response => {
            tabTableConfigurationRowsData = response;

            if (tabTableConfigurationRowsData.length > 0) {
                for (let k = 0; k < tabTableConfigurationRowsData.length; k++) {

                    let tabTableConfigRowData = <ITemplateTabTableConfigurationDetailsPreviewRowsDbModel>{};
                    tabTableConfigRowData = tabTableConfigurationRowsData[k];
                    tabTableConfigRowData.rowId = k + 1;
                    let tabTableConfigColumnsDataList = [];
                    let tabTableConfigColumnsDbData = tabTableConfigRowData.columns;

                    for (let l = 0; l <= tabTableConfigColumnsDbData.length; l++) {
                        let tabTableConfigColumnData = <ITemplateTabTableConfigurationDetailsPreviewColumnsDbModel>{};
                        if (l < tabTableConfigColumnsDbData.length) {
                            tabTableConfigColumnData.id = tabTableConfigColumnsDbData[l].id;
                            tabTableConfigColumnData.columnId = l;
                            tabTableConfigColumnData.dbColumn = tabTableConfigColumnsDbData[l].dbColumn;
                            tabTableConfigColumnData.columnType = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnType : 0;
                            tabTableConfigColumnData.columnHeader = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnHeader : "";
                            tabTableConfigColumnData.columnData = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnData : "";
                            tabTableConfigColumnData.rowSpan = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].rowSpan : 1;
                            tabTableConfigColumnData.columnSpan = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnSpan : 1;
                            tabTableConfigColumnData.columnWidth = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].columnWidth : 10;
                            tabTableConfigColumnData.colourCode = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].colourCode : "";
                            tabTableConfigColumnData.isHeader = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].isHeader : false;
                            tabTableConfigColumnData.isDeleted = (l < tabTableConfigColumnsDbData.length) ? tabTableConfigColumnsDbData[l].isDeleted : false;
                            tabTableConfigColumnsDataList.push(tabTableConfigColumnData);
                        }
                    }
                    tabTableConfigRowData.columns = tabTableConfigColumnsDataList;
                    this.tabTableConfigDataPreview.push(tabTableConfigRowData);
                }
            }
            this.SpinnerService.hide();
        });
    }
}
