import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route } from '@angular/compiler/src/core';
import { Routes, RouterModule } from '@angular/router';
import { TabViewModule } from 'primeng/tabview';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { RadioButtonModule } from 'primeng/radiobutton';
import { Component, ViewChild, ViewContainerRef, Input, EventEmitter, Output } from '@angular/core';
import { TabView, TabPanel } from 'primeng/primeng';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { ToastModule } from 'primeng/toast';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { PreviewTabTableConfigurationModalComponent } from '../preview/preview-tabTableConfiguration-modal.component';
import { from } from 'rxjs';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
    declarations: [PreviewTabTableConfigurationModalComponent],
    imports: [
        DialogModule,
        DropdownModule,
        FormsModule,
        ButtonModule,
        TableModule,
        TabViewModule,
        CommonModule,
        ToastModule,
        MessagesModule,
        MessageModule,
        ConfirmDialogModule,
        RadioButtonModule,
        NgxSpinnerModule
    ],
    exports: [PreviewTabTableConfigurationModalComponent]
})
export class PreviewTabTableConfigurationModalModule { }
