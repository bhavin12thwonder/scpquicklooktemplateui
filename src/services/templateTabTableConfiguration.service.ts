import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { ITemplateTabTableConfigurationModel } from '../app/models/templateTabTableConfiguration.model';

@Injectable({
  providedIn: 'root'
})
export class TemplateTabTableConfigurationService extends BaseService {
  templateTabTableConfigurationModel: ITemplateTabTableConfigurationModel;

  constructor(
    protected http: HttpClient
  ) {
    super(http);
  }

  getAll(): Observable<any> {
    return this.http
      .get(`${this.baseApiUrl}TemplateTabTableConfiguration/GetAll`, { headers: this.headers })
      .pipe(map(this.getJson));
  }

  getById(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabTableConfiguration/GetById/${id}`,
      { headers: this.headers }
    );
  }

  getByTemplateTabConfigId(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabTableConfiguration/GetByTemplateTabConfigId/${id}`,
      { headers: this.headers }
    );
  }

  getGridData(): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabTableConfiguration/GetAll`,
      { headers: this.headers }
    );
  }

  checkTemplateTabTableConfigName(tabConfigId: number, name: string): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabTableConfiguration/CheckTabTableConfigName/${tabConfigId}/${name}`,
      { headers: this.headers }
    );
  }

  postData(body: ITemplateTabTableConfigurationModel): Observable<any> {
    return this.http.post<any>(
      `${this.baseApiUrl}TemplateTabTableConfiguration`,
      body,
      { headers: this.headers }
    );
  }

  putData(body: ITemplateTabTableConfigurationModel): Observable<any> {
    return this.http.put<any>(
      `${this.baseApiUrl}TemplateTabTableConfiguration`,
      body,
      { headers: this.headers }
    );
  }

  deleteData(id: number): Observable<any> {
    return this.http.delete(
      `${this.baseApiUrl}TemplateTabTableConfiguration/${id}`,
      { headers: this.headers }
    );
  }
}
