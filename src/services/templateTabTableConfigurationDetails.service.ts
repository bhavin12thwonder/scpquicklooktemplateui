import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { ITemplateTabTableConfigurationDetailsDbModel } from '../app/models/templateTabTableConfigurationDetails.model';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class TemplateTabTableConfigurationDetailsService extends BaseService {
  templateTabTableConfigurationDetailsDbModel: ITemplateTabTableConfigurationDetailsDbModel;

  constructor(
    protected http: HttpClient
  ) {
    super(http);
  }

  getAll(): Observable<any> {
    return this.http
      .get(`${this.baseApiUrl}TemplateTabTableConfigurationDetails/GetAll`, { headers: this.headers })
      .pipe(map(this.getJson));
  }

  getById(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabTableConfigurationDetails/GetById/${id}`,
      { headers: this.headers }
    );
  }

  getByTemplateTabTableConfigId(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabTableConfigurationDetails/GetByTemplateTabTableConfigId/${id}`,
      { headers: this.headers }
    );
  }

  getGridData(): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabTableConfigurationDetails/GetAll`,
      { headers: this.headers }
    );
  }

  postData(body: Array<ITemplateTabTableConfigurationDetailsDbModel>): Observable<any> {
    return this.http.post<any>(
      `${this.baseApiUrl}TemplateTabTableConfigurationDetails`,
      body,
      { headers: this.headers }
    );
  }

  putData(body: Array<ITemplateTabTableConfigurationDetailsDbModel>): Observable<any> {
    return this.http.put<any>(
      `${this.baseApiUrl}TemplateTabTableConfigurationDetails`,
      body,
      { headers: this.headers }
    );
  }

  deleteData(id: number): Observable<any> {
    return this.http.delete(
      `${this.baseApiUrl}TemplateTabTableConfigurationDetails/${id}`,
      { headers: this.headers }
    );
  }
  previewByTemplateTabTableConfigId(id: number, testId: number): Observable<any> {
    //testId = 24150;
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabTableConfigurationDetails/PreviewByTemplateTabTableConfigId/${id}/${testId}`,
      { headers: this.headers }
    );
  }
}
