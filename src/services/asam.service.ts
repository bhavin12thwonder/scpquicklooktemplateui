import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { environment } from '../environments/environment';
import { IChannelConvertData } from 'src/app/models/asam.model';

@Injectable({
    providedIn: 'root'
})

export class ASAMService extends BaseService {
    constructor(
        protected http: HttpClient
    ) {
        super(http);
    }

    getASAMCalculation(testType: any, testMode: any): Observable<any> {
        return this.http.get<any>(
            `${environment.asamApiUrl}ASAM/GetCalculation/${testType}/${testMode}`,
            { headers: this.headers }
        );
    }

    getASAMChannels(testType: any, testMode: any): Observable<any> {
        var url = `${environment.asamApiUrl}ASAM/GetAllChannels?testType=${testType}&`;
        var testModeUrl = `testMode=`;
        for (var i = 0; i < testMode.length; i++) {
            testModeUrl = testModeUrl + testMode[i] + '&testMode=';
        }
        testModeUrl = testModeUrl.substring(0, testModeUrl.lastIndexOf("&testMode="));
        url = url + testModeUrl;
        return this.http.get<any>(
            url,
            { headers: this.headers }
        );
    }

    getASAMTestData(testType: any, testMode: any[]): Observable<any> {
        var url = `${environment.asamApiUrl}ASAM/GetAllTest?testType=${testType}&`;
        var testModeUrl = `testMode=`;
        for (var i = 0; i < testMode.length; i++) {
            testModeUrl = testModeUrl + testMode[i] + '&testMode=';
        }
        testModeUrl = testModeUrl.substring(0, testModeUrl.lastIndexOf("&testMode="));
        url = url + testModeUrl;
        return this.http.get<any>(
            url,
            { headers: this.headers }
        );
    }

    getASAMChannelData(testRequestId: any, channels: any[]): Observable<any> {
        return this.http.get<any>(
            `${environment.asamApiUrl}ASAM/GetChannelData?testId=${testRequestId}&channels=${channels}`,
            { headers: this.headers }
        );
    }

    getASAMCalculationValue(testRequestId: any, calculations: any[]): Observable<any> {
        return this.http.get<any>(
            `${environment.asamApiUrl}ASAM/GetCalculationValue?testRequestId=${testRequestId}&calculations${calculations}`,
            { headers: this.headers }
        );
    }

    getASAMCalculationChannelYAxis(channel: any): Observable<any> {
        return this.http.get<any>(
            `${environment.asamApiUrl}ASAM/GetCalculationChannelYAxis/${channel}`,
            { headers: this.headers }
        );
    }

    getASAMCalculationChannelNameYAxis(channelName: any): Observable<any> {
        return this.http.get<any>(
            `${environment.asamApiUrl}ASAM/GetCalculationChannelNameYAxis/${channelName}`,
            { headers: this.headers }
        );
    }

    getChannelUnit(channelName: any): Observable<any> {
        return this.http.get<any>(
            `${environment.asamApiUrl}ASAM/GetChannelUnit/${channelName}`,
            { headers: this.headers }
        );
    }

    getPhysicalUnit(channelName: any): Observable<any> {
        return this.http.get<any>(
            `${environment.asamApiUrl}ASAM/GetPhysicalUnit/${channelName}`,
            { headers: this.headers }
        );
    }

    getConvertDataUnit(body: IChannelConvertData): Observable<any> {
        return this.http.post<any>(
            `${environment.asamApiUrl}ASAM/ConvertData_Unit`,
            body,
            { headers: this.headers }
        );
    }
}