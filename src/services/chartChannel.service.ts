import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { BaseService } from './base.service';
import { IChartChannelModel } from 'src/app/models/chartChannel.model';

@Injectable({
    providedIn: 'root'
})
export class ChartChannelService extends BaseService {
    constructor(
        protected http: HttpClient
    ) {
        super(http);
    }

    getById(id: number): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartChannel/GetById/${id}`,
            { headers: this.headers }
        );
    }

    getByChartConfigId(id: number): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartChannel/GetByChartConfigId/${id}`,
            { headers: this.headers }
        );
    }

    getAll(): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartChannel/GetAll`,
            { headers: this.headers }
        );
    }

    postData(body: IChartChannelModel): Observable<any> {
        return this.http.post<any>(
            `${this.baseApiUrl}ChartChannel`,
            body,
            { headers: this.headers }
        );
    }

    putData(body: IChartChannelModel): Observable<any> {
        return this.http.put<any>(
            `${this.baseApiUrl}ChartChannel`,
            body,
            { headers: this.headers }
        );
    }

    deleteData(id: number): Observable<any> {
        return this.http.delete(
            `${this.baseApiUrl}ChartChannel/${id}`,
            { headers: this.headers }
        );
    }

    updateXAxisChannel(body: IChartChannelModel): Observable<any> {
        return this.http.post<any>(
            `${this.baseApiUrl}ChartChannel/UpdateXAxisChannel`,
            body,
            { headers: this.headers }
        );
    }

    getXaxisChannel(id: number): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartChannel/GetXaxisChannel/${id}`,
            { headers: this.headers }
        );
    }
}