import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class TestModeService extends BaseService {
  constructor(
    protected http: HttpClient
  ) {
    super(http);
  }

  getAll(): Observable<any> {
    return this.http
      .get(`${this.baseApiUrl}TestMode/GetAll`, { headers: this.headers })
      .pipe(map(this.getJson));
  }

  getByTestType(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TestMode/GetByTestType/${id}`,
      { headers: this.headers }
    );
  }
}
