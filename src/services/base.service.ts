import { baseApiUrl } from '../app/apiUrlConst/base.ApiUrl';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import { IApiResult } from './apiResultInterface';

export class BaseService {
    headers: HttpHeaders = new HttpHeaders({
        "Content-Type": "application/json; charset=utf-8"
    });
    baseApiUrl: string = '';
    constructor(
        protected http?: HttpClient
    ) {
        if (this.http) {
            this.headers = this.headers,
                this.baseApiUrl = baseApiUrl;
        }
    }

    public Headers(): HttpHeaders {
        return this.headers;
    }

    public getJson(response: HttpResponse<any>) {
        return response || {};
    }

    public checkErrors(response: HttpResponse<any>): HttpResponse<any> {
        if (response.status >= 200 && response.status <= 300) {
            return response;
        } else {
            var error = new Error(response.statusText);
            error['response'] = response;
            console.error(error);
            throw error;
        }
    }

    callAPI(type, apiUrl, postBody?) {
        if (type === 'get') {
            return this.http.get<IApiResult>(`${this.baseApiUrl}${apiUrl}`, {
                headers: this.headers
            });
        }
        if (type === 'post') {
            return this.http.post<IApiResult>(
                `${this.baseApiUrl}${apiUrl}`,
                postBody,
                { headers: this.headers }
            );
        }
        if (type === 'put') {
            return this.http.post<IApiResult>(
                `${this.baseApiUrl}${apiUrl}`,
                postBody,
                { headers: this.headers }
            );
        }
        if (type === 'delete') {
            return this.http.delete<IApiResult>(
                `${this.baseApiUrl}${apiUrl}`,
                { headers: this.headers }
            );
        }
    }
}

