import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { IChartLineThicknessModel } from 'src/app/models/chartLineThickness.model';

@Injectable({
    providedIn: 'root'
})
export class ChartLineThicknessService extends BaseService {
    constructor(
        protected http: HttpClient
    ) {
        super(http);
    }

    getById(id: number): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartLineThickness/GetById/${id}`,
            { headers: this.headers }
        );
    }

    getAll(): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartLineThickness/GetAll`,
            { headers: this.headers }
        );
    }

    postData(body: IChartLineThicknessModel): Observable<any> {
        return this.http.post<any>(
            `${this.baseApiUrl}ChartLineThickness`,
            body,
            { headers: this.headers }
        );
    }

    putData(body: IChartLineThicknessModel): Observable<any> {
        return this.http.put<any>(
            `${this.baseApiUrl}ChartLineThickness`,
            body,
            { headers: this.headers }
        );
    }

    deleteData(id: number): Observable<any> {
        return this.http.delete(
            `${this.baseApiUrl}ChartLineThickness/${id}`,
            { headers: this.headers }
        );
    }
}