import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { ITemplateModel } from '../app/models/template.model';


@Injectable({
  providedIn: 'root'
})
export class TemplateService extends BaseService {
  templateModel: ITemplateModel;

  constructor(
    protected http: HttpClient
  ) {
    super(http);
  }

  getTemplate(): Observable<any> {
    return this.http.get(`${this.baseApiUrl}Template/GetAll`,
      { headers: this.headers })
      .pipe(map(this.getJson));
  }

  getById(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}Template/GetById/${id}`,
      { headers: this.headers }
    );
  }

  getGridData(): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}Template/GetAll`,
      { headers: this.headers }
    );
  }

  previewByTemplateId(id: number, testId: number, submatrixName: string): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}Template/PreviewByTemplateId/${id}/${testId}/${submatrixName}`,
      { headers: this.headers }
    );
  }


  checkTemplateName(body: ITemplateModel): Observable<any> {
    return this.http.post<any>(
      `${this.baseApiUrl}Template/CheckTemplateName`,
      body,
      { headers: this.headers }
    );
  }

  postData(body: ITemplateModel): Observable<any> {
    return this.http.post<any>(
      `${this.baseApiUrl}Template`,
      body,
      { headers: this.headers }
    );
  }

  putData(body: ITemplateModel): Observable<any> {
    return this.http.put<any>(
      `${this.baseApiUrl}Template`,
      body,
      { headers: this.headers }
    );
  }

  deleteData(id: number): Observable<any> {
    return this.http.delete(
      `${this.baseApiUrl}Template/${id}`,
      { headers: this.headers }
    );
  }

  enableData(id: number): Observable<any> {
    return this.http.put(
      `${this.baseApiUrl}Template/Enable/${id}`,
      { headers: this.headers }
    );
  }
}