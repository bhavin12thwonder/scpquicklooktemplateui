import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { IChartAnnotationModel } from 'src/app/models/chartAnnotation.model';

@Injectable({
    providedIn: 'root'
})
export class ChartAnnotationService extends BaseService {
    constructor(
        protected http: HttpClient
    ) {
        super(http);
    }

    getById(id: number): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartAnnotation/GetById/${id}`,
            { headers: this.headers }
        );
    }

    getByChartConfigId(id: number): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartAnnotation/GetByChartConfigId/${id}`,
            { headers: this.headers }
        );
    }

    getAll(): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartAnnotation/GetAll`,
            { headers: this.headers }
        );
    }

    postData(body: IChartAnnotationModel): Observable<any> {
        return this.http.post<any>(
            `${this.baseApiUrl}ChartAnnotation`,
            body,
            { headers: this.headers }
        );
    }

    putData(body: IChartAnnotationModel): Observable<any> {
        return this.http.put<any>(
            `${this.baseApiUrl}ChartAnnotation`,
            body,
            { headers: this.headers }
        );
    }

    deleteData(id: number): Observable<any> {
        return this.http.delete(
            `${this.baseApiUrl}ChartAnnotation/${id}`,
            { headers: this.headers }
        );
    }
}