import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { ITemplateTabModel } from '../app/models/templateTab.model';

@Injectable({
  providedIn: 'root'
})
export class TemplateTabService extends BaseService {
  templateTabModel: ITemplateTabModel;

  constructor(
    protected http: HttpClient
  ) {
    super(http);
  }

  getAll(): Observable<any> {
    return this.http
      .get(`${this.baseApiUrl}TemplateTab/GetAll`, { headers: this.headers })
      .pipe(map(this.getJson));
  }

  getById(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTab/GetById/${id}`,
      { headers: this.headers }
    );
  }

  getByTemplateId(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTab/GetByTemplateId/${id}`,
      { headers: this.headers }
    );
  }

  getGridData(): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTab/GetAll`,
      { headers: this.headers }
    );
  }

  postData(body: ITemplateTabModel): Observable<any> {
    return this.http.post<any>(
      `${this.baseApiUrl}TemplateTab`,
      body,
      { headers: this.headers }
    );
  }

  putData(body: ITemplateTabModel): Observable<any> {
    return this.http.put<any>(
      `${this.baseApiUrl}TemplateTab`,
      body,
      { headers: this.headers }
    );
  }

  deleteData(templateId: number, tabIndex: number): Observable<any> {
    return this.http.delete(
      `${this.baseApiUrl}TemplateTab/${templateId}/${tabIndex}`,
      { headers: this.headers }
    );
  }
}
