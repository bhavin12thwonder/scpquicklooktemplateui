import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { IChartLineStyleModel } from 'src/app/models/chartLineStyle.model';

@Injectable({
    providedIn: 'root'
})
export class ChartLineStyleService extends BaseService {
    constructor(
        protected http: HttpClient
    ) {
        super(http);
    }

    getById(id: number): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartLineStyle/GetById/${id}`,
            { headers: this.headers }
        );
    }

    getAll(): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartLineStyle/GetAll`,
            { headers: this.headers }
        );
    }

    postData(body: IChartLineStyleModel): Observable<any> {
        return this.http.post<any>(
            `${this.baseApiUrl}ChartLineStyle`,
            body,
            { headers: this.headers }
        );
    }

    putData(body: IChartLineStyleModel): Observable<any> {
        return this.http.put<any>(
            `${this.baseApiUrl}ChartLineStyle`,
            body,
            { headers: this.headers }
        );
    }

    deleteData(id: number): Observable<any> {
        return this.http.delete(
            `${this.baseApiUrl}ChartLineStyle/${id}`,
            { headers: this.headers }
        );
    }
}