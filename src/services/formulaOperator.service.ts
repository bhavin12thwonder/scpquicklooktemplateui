import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { ITemplateTabTableConfigurationDetailsDbModel } from '../app/models/templateTabTableConfigurationDetails.model';

@Injectable({
    providedIn: 'root'
})
export class FormulaOperatorService extends BaseService {
    constructor(
        protected http: HttpClient
    ) {
        super(http);
    }


    getFormulaOperatorsData(): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}FormulaOperator/GetAll`,
            { headers: this.headers }
        );
    }
}