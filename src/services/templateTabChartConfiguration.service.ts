import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { ITemplateTabChartConfigurationModel } from '../app/models/templateTabChartConfiguration.model';

@Injectable({
  providedIn: 'root'
})
export class TemplateTabChartConfigurationService extends BaseService {

  constructor(
    protected http: HttpClient
  ) {
    super(http);
  }

  getAll(): Observable<any> {
    return this.http
      .get(`${this.baseApiUrl}TemplateTabChartConfiguration/GetAll`, { headers: this.headers })
      .pipe(map(this.getJson));
  }

  getById(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabChartConfiguration/GetById/${id}`,
      { headers: this.headers }
    );
  }

  getByTemplateTabConfigId(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabChartConfiguration/GetByTemplateTabConfigId/${id}`,
      { headers: this.headers }
    );
  }

  getGridData(): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabChartConfiguration/GetAll`,
      { headers: this.headers }
    );
  }

  checkTemplateTabChartConfigName(tabConfigId: number, chartTypeId: number, name: string): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabChartConfiguration/CheckTabChartConfigName/${tabConfigId}/${chartTypeId}/${name}`,
      { headers: this.headers }
    );
  }

  postData(body: ITemplateTabChartConfigurationModel): Observable<any> {
    return this.http.post<any>(
      `${this.baseApiUrl}TemplateTabChartConfiguration`,
      body,
      { headers: this.headers }
    );
  }

  putData(body: ITemplateTabChartConfigurationModel): Observable<any> {
    return this.http.put<any>(
      `${this.baseApiUrl}TemplateTabChartConfiguration`,
      body,
      { headers: this.headers }
    );
  }

  deleteData(id: number): Observable<any> {
    return this.http.delete(
      `${this.baseApiUrl}TemplateTabChartConfiguration/${id}`,
      { headers: this.headers }
    );
  }
}
