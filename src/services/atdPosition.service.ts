import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { IATDPosition } from '../app/models/atdPosition.model';

@Injectable({
    providedIn: 'root'
})
export class ATDPositionService extends BaseService {
    constructor(
        protected http: HttpClient
    ) {
        super(http);
    }

    getATDPositionData(): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ATDPosition/GetAll`,
            { headers: this.headers }
        );
    }
}