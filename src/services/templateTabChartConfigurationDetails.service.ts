import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { ITemplateTabChartConfigurationDetailsUpsertModel } from '../app/models/templateTabChartConfigurationDetails.model';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TemplateTabChartConfigurationDetailsService extends BaseService {

  constructor(
    protected http: HttpClient
  ) {
    super(http);
  }

  getAll(): Observable<any> {
    return this.http
      .get(`${this.baseApiUrl}TemplateTabChartConfigurationDetails/GetAll`, { headers: this.headers })
      .pipe(map(this.getJson));
  }

  getById(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabChartConfigurationDetails/GetById/${id}`,
      { headers: this.headers }
    );
  }

  getByTemplateTabChartConfigId(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabChartConfigurationDetails/GetByTemplateTabChartConfigId/${id}`,
      { headers: this.headers }
    );
  }

  postData(body: ITemplateTabChartConfigurationDetailsUpsertModel): Observable<any> {
    return this.http.post<any>(
      `${this.baseApiUrl}TemplateTabChartConfigurationDetails`,
      body,
      { headers: this.headers }
    );
  }

  putData(body: ITemplateTabChartConfigurationDetailsUpsertModel): Observable<any> {
    return this.http.put<any>(
      `${this.baseApiUrl}TemplateTabChartConfigurationDetails`,
      body,
      { headers: this.headers }
    );
  }

  deleteData(id: number): Observable<any> {
    return this.http.delete(
      `${this.baseApiUrl}TemplateTabChartConfigurationDetails/${id}`,
      { headers: this.headers }
    );
  }

  previewByTemplateTabChartConfigId(id: number, testId: number, submatrixName: string): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabChartConfigurationDetails/PreviewByTemplateTabChartConfigId/${id}/${testId}/${submatrixName}`,
      { headers: this.headers }
    );
  }
}
