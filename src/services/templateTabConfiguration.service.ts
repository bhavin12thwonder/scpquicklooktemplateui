import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { ITemplateTabConfigurationModel } from '../app/models/templateTabConfiguration.model';

@Injectable({
  providedIn: 'root'
})
export class TemplateTabConfigurationService extends BaseService {
  templateTabConfigurationModel: ITemplateTabConfigurationModel;

  constructor(
    protected http: HttpClient
  ) {
    super(http);
  }

  getAll(): Observable<any> {
    return this.http
      .get(`${this.baseApiUrl}TemplateTabConfiguration/GetAll`, { headers: this.headers })
      .pipe(map(this.getJson));
  }

  getById(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabConfiguration/GetById/${id}`,
      { headers: this.headers }
    );
  }

  getByTemplateTabId(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabConfiguration/GetByTemplateTabId/${id}`,
      { headers: this.headers }
    );
  }

  previewByTemplateTabId(id: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabConfiguration/PreviewByTemplateTabId/${id}`,
      { headers: this.headers }
    );
  }

  getTabConfigTypeByTabId(id: number, configTypeId: number): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabConfiguration/GetTemplateTabConfigurationTypeByTemplateTabId/${configTypeId}/${id}`,
      { headers: this.headers }
    );
  }

  getGridData(): Observable<any> {
    return this.http.get<any>(
      `${this.baseApiUrl}TemplateTabConfiguration/GetAll`,
      { headers: this.headers }
    );
  }

  postData(body: Array<ITemplateTabConfigurationModel>): Observable<any> {
    return this.http.post<any>(
      `${this.baseApiUrl}TemplateTabConfiguration`,
      body,
      { headers: this.headers }
    );
  }

  putData(body: Array<ITemplateTabConfigurationModel>): Observable<any> {
    return this.http.put<any>(
      `${this.baseApiUrl}TemplateTabConfiguration`,
      body,
      { headers: this.headers }
    );
  }

  putDataById(body: ITemplateTabConfigurationModel): Observable<any> {
    return this.http.put<any>(
      `${this.baseApiUrl}TemplateTabConfiguration/UpdateById`,
      body,
      { headers: this.headers }
    );
  }

  putTabConfigTypeByTabId(id: number, configTypeId: number): Observable<any> {
    return this.http.put<any>(
      `${this.baseApiUrl}TemplateTabConfiguration/UpdateTemplateTabConfigurationTypeByTemplateTabId/${configTypeId}/${id}`,
      { headers: this.headers }
    );
  }

  deleteData(id: number): Observable<any> {
    return this.http.delete(
      `${this.baseApiUrl}TemplateTabConfiguration/${id}`,
      { headers: this.headers }
    );
  }
}
