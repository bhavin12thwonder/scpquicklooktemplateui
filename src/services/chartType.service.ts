import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { IChartTypeModel } from 'src/app/models/chartType.model';

@Injectable({
    providedIn: 'root'
})
export class ChartTypeService extends BaseService {
    constructor(
        protected http: HttpClient
    ) {
        super(http);
    }



    getById(id: number): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartType/GetById/${id}`,
            { headers: this.headers }
        );
    }

    getAll(): Observable<any> {
        return this.http.get<any>(
            `${this.baseApiUrl}ChartType/GetAll`,
            { headers: this.headers }
        );
    }

    postData(body: IChartTypeModel): Observable<any> {
        return this.http.post<any>(
            `${this.baseApiUrl}ChartType`,
            body,
            { headers: this.headers }
        );
    }

    putData(body: IChartTypeModel): Observable<any> {
        return this.http.put<any>(
            `${this.baseApiUrl}ChartType`,
            body,
            { headers: this.headers }
        );
    }

    deleteData(id: number): Observable<any> {
        return this.http.delete(
            `${this.baseApiUrl}ChartType/${id}`,
            { headers: this.headers }
        );
    }
}