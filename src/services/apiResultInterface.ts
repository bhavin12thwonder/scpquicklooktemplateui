export interface IApiResult {
    result: any[] | any;
    errorMessage: string;
    isSuccess: boolean;
    timeGenerated: string;
    $values?: any[];
}